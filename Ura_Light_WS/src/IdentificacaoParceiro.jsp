<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {

	 JSONObject result = new JSONObject();
	    
	    String clientId = additionalParams.get("clientId"); 
	    String clientSecret = additionalParams.get("clientSecret");
		String opcao = additionalParams.get("opcao");
		String partner = additionalParams.get("partner"); 
	    String anlage = additionalParams.get("anlage");
		String telefone = additionalParams.get("telefone");
		String cpf = additionalParams.get("cpf"); 
	    String cnpj = additionalParams.get("cnpj");
		String dt_notas = additionalParams.get("dt_notas");
		String max_instals = additionalParams.get("max_instals"); 
	    String urlUra = additionalParams.get("urlUra");
		String timeoutUra = additionalParams.get("timeoutUra");
	       
		IdentificacaoParceiro identificacaoParceiro = new IdentificacaoParceiro();
	    result = identificacaoParceiro.executeIdentificacaoParceiro(clientId, clientSecret, opcao, partner, anlage, telefone, cpf, cnpj, dt_notas, max_instals, urlUra, timeoutUra);
	    result.put ("objRetornoIdentificacaoParceiro",result);
	    
	    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="sap.*"%>
<%@include file="../include/backend.jspf" %>