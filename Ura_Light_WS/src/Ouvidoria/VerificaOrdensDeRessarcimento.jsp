<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {

	JSONObject result = new JSONObject();
	 
	JSONObject dados = state.getJSONObject("io_dados");
		
	//String clientId = dados.getString("clientId"); 
	//String clientSecret = dados.getString("clientSecret");
    //String urlUra = dados.getString("urlUra");
  //String urlUra = "http://ssqplci.light.com.br:50200/XISOAPAdapter/MessageServlet?channel=:BS_URO_QUA:Z_CC_URO_BUSCA_ZRAR_SOAP_SENDER&version=3.0&Sender.Service=BS_URO_QUA&Interface=http://atc/agv/crm/020^BuscaZrarSyncOut_SI";

  	String clientId = "WS_USER_URVV"; 
    String clientSecret = "P@ss21182222";
    String urlUra = "http://ssppldb1.light.com.br:50200/XISOAPAdapter/MessageServlet?channel=:BS_URO_PRD:Z_CC_URO_BUSCA_ZRAR_SOAP_SENDER&version=3.0&Sender.Service=BS_URO_PRD&Interface=http://atc/agv/crm/020^BuscaZrarSyncOut_SI";
    
    if ("d".equals(dados.getString("ambiente"))){
    	clientId = "WS_USER_URO";
    	clientSecret = "P@ssw0rd";
    	urlUra = "http://ssqplci.light.com.br:50200/XISOAPAdapter/MessageServlet?channel=:BS_URO_QUA:Z_CC_URO_BUSCA_ZRAR_SOAP_SENDER&version=3.0&Sender.Service=BS_URO_QUA&Interface=http://atc/agv/crm/020^BuscaZrarSyncOut_SI";	
    }
    
    //String timeoutUra = dados.getString("timeoutUra");
	String timeoutUra = "5000";
	
	String partner = dados.getJSONObject("servicoIdentificacao").getString("Y_PARTNER"); 
    String dataAtual = "";
    String dataLimite = "";
	
	
    Calendar dataAuxiliar = Calendar.getInstance();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	dataAtual = sdf.format(dataAuxiliar.getTime());
	dataAuxiliar.add(Calendar.DAY_OF_MONTH, -125);
	dataLimite = sdf.format(dataAuxiliar.getTime());
       
	OrdemDeRessarcimento servico = new OrdemDeRessarcimento();
    result.put ("retornoServico", servico.verificarOrdemRessarcimento(urlUra, clientId, clientSecret, timeoutUra, partner, dataAtual, dataLimite));
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="br.com.light.ordemressarcimento.OrdemDeRessarcimento"%>
<%@include file="../../include/backend.jspf" %>