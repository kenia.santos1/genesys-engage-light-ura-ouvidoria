<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {

	JSONObject result = new JSONObject();
	JSONObject dados = state.getJSONObject("io_dados");
	
	Map<String, String> parametros = new HashMap<String, String>();
	
	parametros.put("ani", dados.getString("ani").replace("+55", ""));
	//parametros.put("DBUID", dados.getJSONObject("parametros").getString("dbuid"));
	parametros.put("DBUID", "light_ivr");
	//parametros.put("DBPWD", dados.getJSONObject("parametros").getString("dbpwd"));
	parametros.put("DBPWD", "light_ivr");
    
	Rechamada rechamada = new Rechamada();
	int retorno = rechamada.buscarQtdeRechamada(parametros);
	
	result.put ("retornoServico", retorno);
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="br.com.light.rechamada.Rechamada"%>
<%@include file="../../include/backend.jspf" %>