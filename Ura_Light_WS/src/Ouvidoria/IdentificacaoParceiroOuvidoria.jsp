<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {

	JSONObject result = new JSONObject();
	 
	JSONObject dados = state.getJSONObject("io_dados");
	
	Calendar dataAuxiliar = Calendar.getInstance();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	dataAuxiliar.add(Calendar.DAY_OF_MONTH, -60);
	
	//String clientId = dados.getString("clientId"); 
	//String clientSecret = dados.getString("clientSecret");
    //String urlUra = dados.getString("urlUra");
    //String urlUra = "http://ssppldb1.light.com.br:50200/XISOAPAdapter/MessageServlet?channel=:BS_URO_QUA:Z_CC_URO_VERIF_PN_INSTAL_SOAP_SENDER&version=3.0&Sender.Service=BS_URO_QUA&Interface=http://www.light.com.br/ura/ouvid_verif_pn_in^Z_SI_OUVID_VERIF_PN_IN_OUTBD_SYNC";
    //String timeoutUra = dados.getString("timeoutUra");
	
    String clientId = "WS_USER_URVV"; 
    String clientSecret = "P@ss21182222";
    String urlUra = "http://ssppldb1.light.com.br:50200/XISOAPAdapter/MessageServlet?channel=:BS_URO_PRD:Z_CC_URO_VERIF_PN_INSTAL_SOAP_SENDER&version=3.0&Sender.Service=BS_URO_PRD&Interface=http://www.light.com.br/ura/ouvid_verif_pn_in^Z_SI_OUVID_VERIF_PN_IN_OUTBD_SYNC";
    
    if ("d".equals(dados.getString("ambiente"))){
    	clientId = "WS_USER_URO";
    	clientSecret = "P@ssw0rd";
        urlUra = "http://ssppldb1.light.com.br:50200/XISOAPAdapter/MessageServlet?channel=:BS_URO_QUA:Z_CC_URO_VERIF_PN_INSTAL_SOAP_SENDER&version=3.0&Sender.Service=BS_URO_QUA&Interface=http://www.light.com.br/ura/ouvid_verif_pn_in^Z_SI_OUVID_VERIF_PN_IN_OUTBD_SYNC";	
    }
    
	String timeoutUra = "5000";
	String opcao = "1";
	String partner = ""; 
    String anlage = "";
	String telefone = "";
	String cpf = ""; 
    String cnpj = "";
    String protocolo = "";
	String dt_notas = sdf.format(dataAuxiliar.getTime());
	String max_instals = ""; 
	
	if("telefone".equals(dados.getString("tipo_identificacao"))){
		telefone = dados.getString("ani");
	} else if("cpf".equals(dados.getString("tipo_identificacao"))){
		cpf = dados.getString("doc_identificacao");
	} else if("cnpj".equals(dados.getString("tipo_identificacao"))){
		cnpj = dados.getString("doc_identificacao");
	} else if("protocolo".equals(dados.getString("tipo_identificacao"))){
		opcao = "4";
		protocolo = dados.getString("doc_identificacao");
	} else {
		//identificacao por numero de instalacao
		anlage = dados.getString("doc_identificacao");
		
		if (anlage.length() == 9) {
        	anlage = "0" + anlage;
		}
	}
       
	IdentificacaoParceiro identificacaoParceiro = new IdentificacaoParceiro();
	
	JSONObject retorno = identificacaoParceiro.executeIdentificacaoParceiro(clientId, clientSecret, opcao, partner, anlage, telefone, cpf, cnpj, dt_notas, max_instals, protocolo, urlUra, timeoutUra);
    
	if("protocolo".equals(dados.getString("tipo_identificacao")) && (!"0".equals(retorno.getString("Y_RETURN")))){
		opcao = "1";
		protocolo = "";
		anlage = dados.getString("doc_identificacao");
		retorno = identificacaoParceiro.executeIdentificacaoParceiro(clientId, clientSecret, opcao, partner, anlage, telefone, cpf, cnpj, dt_notas, max_instals, protocolo, urlUra, timeoutUra);
	}
	
	result.put ("retornoServico", retorno);
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="br.com.light.ouvidoria.identificacaoparceiro.IdentificacaoParceiro"%>
<%@include file="../../include/backend.jspf" %>