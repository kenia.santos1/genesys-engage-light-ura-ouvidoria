<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {

	JSONObject result = new JSONObject();
	 
	JSONObject dados = state.getJSONObject("io_dados");
	
	//String clientId = dados.getString("clientId"); 
	//String clientSecret = dados.getString("clientSecret");
    //String urlUra = dados.getString("urlUra");
    //String urlUra = "http://ssqplci.light.com.br:50200/XISOAPAdapter/MessageServlet?channel=:BS_URO_QUA:Z_CC_URO_CRM_ACTIVITY_CREATE_SOAP_SENDER&version=3.0&Sender.Service=BS_URO_QUA&Interface=http://www.light.com.br/ura/crm_activity_create^Z_MI_CRM_ACTIVITY_CREATE_OUTBD_SYNC";

    String clientId = "WS_USER_URVV"; 
    String clientSecret = "P@ss21182222";
    String urlUra = "http://ssppldb1.light.com.br:50200/XISOAPAdapter/MessageServlet?channel=:BS_URO_PRD:Z_CC_URO_CRM_ACTIVITY_CREATE_SOAP_SENDER&version=3.0&Sender.Service=BS_URO_PRD&Interface=http://www.light.com.br/ura/crm_activity_create^Z_MI_CRM_ACTIVITY_CREATE_OUTBD_SYNC";
    
    if ("d".equals(dados.getString("ambiente"))){
    	clientId = "WS_USER_URO";
    	clientSecret = "P@ssw0rd";
    	urlUra = "http://ssqplci.light.com.br:50200/XISOAPAdapter/MessageServlet?channel=:BS_URO_QUA:Z_CC_URO_CRM_ACTIVITY_CREATE_SOAP_SENDER&version=3.0&Sender.Service=BS_URO_QUA&Interface=http://www.light.com.br/ura/crm_activity_create^Z_MI_CRM_ACTIVITY_CREATE_OUTBD_SYNC";	
    }
    
    //String timeoutUra = dados.getString("timeoutUra");
	String timeoutUra = "5000";
	String partner = dados.getJSONObject("servicoIdentificacao").getString("Y_PARTNER");
	String protocolo = dados.getJSONObject("servicoProtocoloInicial").getString("Y_PROTOCOLO");
    String install = dados.getJSONObject("instalacao_selecionada").getString("ANLAGE");
	
    GerarContatoAssociado servico = new GerarContatoAssociado();
    result.put ("retornoServico", servico.gerarContatoAoProtocolo(urlUra, clientId, clientSecret, timeoutUra, partner, install, protocolo));
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="br.com.light.gerarcontatoassociado.GerarContatoAssociado"%>
<%@include file="../../include/backend.jspf" %>