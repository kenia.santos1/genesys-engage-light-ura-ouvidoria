<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {

	 JSONObject result = new JSONObject();
	    
	    String clientId = additionalParams.get("client");
		String clientSecret = additionalParams.get("clientSec");
	    String xpartner = additionalParams.get("partner"); 
	    String xcategory = additionalParams.get("category");
		String urlUra = additionalParams.get("url");
		String timeoutUra = additionalParams.get("timeout");
		CriacaoProtocoloInicial criacaoProtocoloInicial = new CriacaoProtocoloInicial();
	    result = criacaoProtocoloInicial.executeCriacaoProtocoloInicial (clientId, clientSecret , xpartner,  xcategory, urlUra, timeoutUra);
	    
	    result.put ("objRetornoProtocolo",result);
	    
	    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="sap.*"%>
<%@include file="../include/backend.jspf" %>