<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
    JSONObject result = new JSONObject();
    
    String clientId = additionalParams.get("clientId");
	String clientSecret = additionalParams.get("clientSecret");
	String xopbel = additionalParams.get("xopbel"); 
    String xpartner = additionalParams.get("xpartner"); 
    String xprotocolo = additionalParams.get("xprotocolo");
    String xcategory = additionalParams.get("xcategory");
	String urlUra = additionalParams.get("url");
	String timeoutUra = additionalParams.get("timeout");
	
	SegundaViaCorreio segundaViaCorreio = new SegundaViaCorreio();
    result = segundaViaCorreio.executeSegundaViaCorreio( clientId,  clientSecret,  xopbel,  xpartner,  xprotocolo,  xcategory,  urlUra,  timeoutUra);
   
    result.put ("objRetornoSegundaViaCorreio",result);
    
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="sap.*"%>
<%@include file="../include/backend.jspf" %>