<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
    
    JSONObject result = new JSONObject();
    
    String clientId = additionalParams.get("clientId"); 
    String clientSecret = additionalParams.get("clientSecret");
	String opcao = additionalParams.get("opcao");
	String partner = additionalParams.get("partner"); 
    String anlage = additionalParams.get("anlage");
	String celular = additionalParams.get("celular");
	String invoice = additionalParams.get("invoice"); 
	String billing_periodo = additionalParams.get("billing_periodo"); 
	String dtvenc = additionalParams.get("dtvenc"); 
	String betrw = additionalParams.get("betrw"); 
	String printdoc = additionalParams.get("printdoc"); 
	String codbarras = additionalParams.get("codbarras"); 
    String urlUra = additionalParams.get("urlUra");
	String timeoutUra = additionalParams.get("timeoutUra");
       
	CodigoBarras codigoBarras = new CodigoBarras();
    result = codigoBarras.executeCodigoBarras(clientId, clientSecret, partner, anlage, celular,  invoice, billing_periodo, dtvenc, betrw, printdoc, codbarras, protocolo, category, url, timeoutUra);				
    result.put ("objRetornoCodigoBarras",result);
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="sap.*"%>
<%@include file="../include/backend.jspf" %>