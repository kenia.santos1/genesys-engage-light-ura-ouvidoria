<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {

	 JSONObject result = new JSONObject();
	    
	    String clientId = additionalParams.get("clientId"); 
	    String clientSecret = additionalParams.get("clientSecret");
	    String anlage = additionalParams.get("anlage");
	    String vertrag = additionalParams.get("vertrag");
	    String vkont = additionalParams.get("vkont");
	    String partner = additionalParams.get("partner"); 
	    String flag_consumos = additionalParams.get("flag_consumos");	
	    String urlUra = additionalParams.get("urlUra");
	    String timeoutUra = additionalParams.get("timeoutUra");
	  
	    ObterDadosInstalacao obterDadosInstalacao = new ObterDadosInstalacao();
	    result = obterDadosInstalacao.executeObterDadosInstalacao(clientId, clientSecret,  anlage, vertrag, vkont, partner, flag_consumos, urlUra, timeoutUra);
	    result.put ("objRetornoObterDadosInstalacao",result);
	    
	    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="sap.*"%>
<%@include file="../include/backend.jspf" %>