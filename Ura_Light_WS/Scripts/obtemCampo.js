/*********************************************************************************************************
AUTORES: DSF/BARS 2014 - v0.0.1
FUNCAO GENERICA PARA OBTER UM VALOR OU UM ARRAY DE VALORES DENTRO DE UM JSON PELO NOME DO CAMPO
EXEMPLO DE USO:
	Estrutura
		var teste = {
				"a" : {
					"b" : [
			       		{"b1" : "valor_1"},
			       		{"b1" : "valor_2"}			       
					],
					"c" : "valor_3"
				}
		};
	Comando:
		Caso 1: "[" + jsonLog(obtemCampo("b1", teste)) + "]"
		Caso 2: "[" + jsonLog(obtemCampo("c", teste)) + "]"
	Saida:
		Caso 1: ["0": "valor_1" "1": "valor_2"]
		Caso 2: ["0": "valor_3"]
**********************************************************************************************************/
var retCampoNaoExistenteObtemCampo = undefined;

function obtemCampo(nomeCampo, retornoTransacao){
	var ret = obtemCampoTransacao(nomeCampo, retornoTransacao);
	if (ret.length === 0){
		ret[0] = retCampoNaoExistenteObtemCampo;
	}
	return ret;
}

function jsonLog(json){
	
	return JSON.stringify(json);
	
}

function obtemCampoTransacao(nomeCampo, retornoTransacao){
	var retornoCampo = [];
	var temp = [];
	
	for (var tag in retornoTransacao){
		switch (typeof retornoTransacao[tag]){
			case "object":
				if (Object.prototype.toString.call(retornoTransacao[tag]) === '[object Array]'){
					if (tag === nomeCampo){
						temp = retornoTransacao[tag];
					}else{
						temp = obtemCampoTrataArray(nomeCampo, retornoTransacao[tag]);
					}
				}else{
					if (tag === nomeCampo){
						temp[0] = retornoTransacao[tag];					
					}else{
						temp = obtemCampoTransacao(nomeCampo, retornoTransacao[tag]);
					}
				}
				if (temp.length > 0){
					retornoCampo = temp;
				}	
				break;
			default:
				if (tag === nomeCampo){
					retornoCampo.push(retornoTransacao[tag]);
				}
		}
	}
	
	return retornoCampo;
}

function obtemCampoTrataArray(nomeCampo, retornoTransacao){	
	var retornoCampo = [];
	var temp = [];
	
	for (var i=0; i < retornoTransacao.length; i++){
		if (Object.prototype.toString.call(retornoTransacao[i]) === '[object Array]'){
			temp = obtemCampoTrataArray(nomeCampo, retornoTransacao[i]);
		}else{
			temp = obtemCampoTransacao(nomeCampo, retornoTransacao[i]);
		}
		if (temp.length == 1){
			retornoCampo.push(temp[0]);
		}
	}
	
	return retornoCampo;
}
/*********************************************************************************************************
*********************************************************************************************************/
