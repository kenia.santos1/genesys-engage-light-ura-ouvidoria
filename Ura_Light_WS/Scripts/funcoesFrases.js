function getFrasesXXX(dados) {
	var ret = {};
	var i = 0;
	var frases = [];
	
	frases[i++] = {"frase" : ".vox"};
	frases[i++] = {"data" : formataData( "","ddMMyyyy")};
	frases[i++] = {"frase" : ".vox"};
	frases[i++] = {"valor" : trataValor("")};
	frases[i++] = {"frase" : ".vox"};
	frases[i++] = {"numero" : "3"};
	
	return frases;
}

function getFrasesProtocolo(dados) {
	var ret = {};
	var i = 0;
	var frases = [];
	
	var protocolo = dados['protocolo'];
	
	if (protocolo) {
		frases[i++] = {"frase" : "MSG011.vox"};
		frases[i++] = {"string" : protocolo};
	}
	
	return frases;
}

function getFrasesFHAPrompt() {
	var ret = {};
	var i = 0;
	var frases = [];
	
	frases[i++] = {"frase" : "FHA0001.vox"};
	frases[i++] = {"frase" : "deslig_0001.vox"};
	
	return frases;
}