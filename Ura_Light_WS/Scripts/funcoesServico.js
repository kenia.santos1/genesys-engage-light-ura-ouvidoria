
function preparaParametrosServicoAutenticarConsumidorSolicitacao(dadosCliente) {
	var ret = {};
	if (dadosCliente['ambiente'] == 'desenvolvimento') {
		ret['codigoHierarquia'] = '2';
		ret['dominio'] = '';
		ret['usuario'] = 'ura_central';
		ret['senha'] = 'ura_central';
	} else if (dadosCliente['ambiente'] == 'homologacao') {
		ret['codigoHierarquia'] = '2';
		ret['dominio'] = '';
		ret['usuario'] = 'ura_central';
		ret['senha'] = 'ura_central';
	} else {
		ret['codigoHierarquia'] = '1';
		ret['dominio'] = 'CSFCPV';
		ret['usuario'] = 'apl_uracentral';
		ret['senha'] = 'ur@centr@l123';
	}
	return ret;
}

function trataRetornoServicoAutenticarConsumidorSolicitacao(retorno) {
	var ret = {};
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['sucesso'] = obtemCampo('b:Sucesso', retorno)[0];
	ret['token'] = (ret['codRetorno'] == 0 ? obtemCampo('a:Token', retorno)[0] : '');
	return ret;
}

function trataRetornoServicoValidarToken(retorno) {
	var ret = {};
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['sucesso'] = obtemCampo('b:Sucesso', retorno)[0];
	return ret;
}

function preparaParametrosIdentificarClienteSolicitacao(dadosCliente, ani) {
	var ret = {};
	
	ret['numeroCartao'] = '';
	ret['numeroCPF'] = '';
	if (ehCPFouCartao(dadosCliente['documentoDigitado']) == 'cpf') {
		ret['numeroCPF'] = dadosCliente['documentoDigitado'];
	} else {
		ret['numeroCartao'] = dadosCliente['documentoDigitado'];
	}
	if (ani != '') {
		ani = getANITratado(ani);
		ret['DDD'] = ani.substr(0, 2);
		ret['telefone'] = ani.substr(2);
	} else {
		ret['DDD'] = '';
		ret['telefone'] = '';
	}

	return ret;
}

function preparaParametrosIdentificarClienteBI(dadosCliente, ani) {
	var ret = {};
	
	ret['numeroCartao'] = '';
	ret['numeroCPF'] = '';
	if (ehCPFouCartao(dadosCliente['documentoDigitado']) == 'cpf') {
		ret['numeroCPF'] = dadosCliente['documentoDigitado'];
	} else {
		ret['numeroCartao'] = dadosCliente['documentoDigitado'];
	}
	if (ani != '') {
		ani = getANITratado(ani);
		ret['DDD'] = ani.substr(0, 2);
		ret['telefone'] = ani.substr(2);
	} else {
		ret['DDD'] = '';
		ret['telefone'] = '';
	}

	return ret;
}

/* Exemplo de retorno do Servico Identificar Cliente Solicitacao
 * Estrutura : dadosCliente['identificarCliente'] e a estrutura abaixo
{	
	"codRetorno": "0",
	"sucesso": "true",
	"cartoes": {
		"titular" : [{
			"dataValidade": "12/2023",
			"isPrincipal": "true",
			"isTitular": "true",
			"numeroCartao": "5300342883010169",
			"numeroProduto": "6",
			"produto": "MC HÍBRIDO LOCAL",
			"situacao": "I",
			"status": "NORM",
			"bin": "5300",
			"cardId": "0169"
		}],
		"adicional" : [{
			"dataValidade": "12/2023",
			"isPrincipal": "true",
			"isTitular": "false",
			"numeroCartao": "5300342883010169",
			"numeroProduto": "6",
			"produto": "MC HÍBRIDO LOCAL",
			"situacao": "I",
			"status": "NORM",
			"bin": "5300",
			"cardId": "0169"
			}]
	},
	"melhorDiaCompra": "20",
	"statusConta": "NORM",
	"isFuncionario": "false",
	"limiteCredito": "900",
	"diaVencimentoFatura": "1",
	"numeroConta": "37076536657",
	"produtoConta": "CONTA DE MC LOCAL",
	"telefoneBlackList": "false",
	"telefoneEncontrado": "false",
	"telefonePublico": "false",
	"cpfTitular": "14525663723"
}
*/
function trataRetornoServicoIdentificarClienteSolicitacao(retorno, dadosCliente) {
	//retorno : dadoscliente['identificarCliente'][...]
	var ret = {};
	
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['codigoErro'] = '0';
	if (ret['codRetorno'] == '99'){
		ret['codigoErro'] = obtemCampo('c:Codigo', retorno)[0];
	} 
	
	ret['sucesso'] = obtemCampo('b:Sucesso', retorno)[0];
	if (ret['sucesso'] == 'true' && ret['codRetorno'] == '0') {
		var contas = obtemCampo('b:DadosContaDto', retorno);
		ret['qtdContas'] = contas.length;
		var posDataMaior = 0;
		var contaComCartaoDigitado = 0;
		var flag = false;
		for (var y=0; y < contas.length; y++) {
			if (ehCPFouCartao(dadosCliente['documentoDigitado']) == 'cartao') {
				var cards = obtemCampo('b:DadosCartaoDto', contas[y]['b:Cartoes']);
				for (var b=0; b < cards.length; b++) {
					if (dadosCliente['documentoDigitado'] == cards[b]['b:NumeroCartao']) {
						contaComCartaoDigitado = y;
						flag = true;
						break;
					}
				}
				if (flag) {
					break;	
				}
			} else {
				for (var z=y+1; z < contas.length; z++){
					var data1 = removeCaracter(contas[y]["b:DataCadastro"], '/');
					var data2 = removeCaracter(contas[z]["b:DataCadastro"], '/');
					if (diferencaEntreDatas(data1, data2) < 0){ //verifica se a primeira data é menor que a segunda
						posDataMaior = z;
					}
				}
			}
		}
		var cartoes = '';
		if (ehCPFouCartao(dadosCliente['documentoDigitado']) == 'cartao') {
			cartoes = obtemCampo('b:DadosCartaoDto', contas[contaComCartaoDigitado]['b:Cartoes']);
		} else {
			cartoes = obtemCampo('b:DadosCartaoDto', contas[posDataMaior]['b:Cartoes']);	
		}		
		var j = 0;
		var k = 0;
		ret['cartoes'] = {};
		ret['cartoes']['titular'] = [];
		ret['cartoes']['adicional'] = [];
		for (var i = 0; i < cartoes.length; i++){
			if (cartoes[i]['b:IsTitular'] == "true"){
				ret['cartoes']['titular'][j] = {};
				ret['cartoes']['titular'][j]['dataValidade'] = cartoes[i]['b:DataValidade'];
				ret['cartoes']['titular'][j]['isTitular'] = cartoes[i]['b:IsTitular'] == "true";
				ret['cartoes']['titular'][j]['numeroCartao'] = cartoes[i]['b:NumeroCartao'];
				ret['cartoes']['titular'][j]['numeroProduto'] = cartoes[i]['b:NumeroProduto'];
				ret['cartoes']['titular'][j]['produto'] = cartoes[i]['b:Produto'];
				ret['cartoes']['titular'][j]['situacao'] = cartoes[i]['b:Situacao'];
				ret['cartoes']['titular'][j]['status'] = cartoes[i]['b:Status'].toLowerCase();
				ret['cartoes']['titular'][j]['bin'] = cartoes[i]['b:Bin'];
				ret['cartoes']['titular'][j]['cardId'] = cartoes[i]['b:CardID'];
				j++;
			}else{
				ret['cartoes']['adicional'][k] = {};
				ret['cartoes']['adicional'][k]['dataValidade'] = cartoes[i]['b:DataValidade'];
				ret['cartoes']['adicional'][k]['isTitular'] = cartoes[i]['b:IsTitular'] == "true";
				ret['cartoes']['adicional'][k]['numeroCartao'] = cartoes[i]['b:NumeroCartao'];
				ret['cartoes']['adicional'][k]['numeroProduto'] = cartoes[i]['b:NumeroProduto'];
				ret['cartoes']['adicional'][k]['produto'] = cartoes[i]['b:Produto'];
				ret['cartoes']['adicional'][k]['situacao'] = cartoes[i]['b:Situacao'];
				ret['cartoes']['adicional'][k]['status'] = cartoes[i]['b:Status'].toLowerCase();
				ret['cartoes']['adicional'][k]['bin'] = cartoes[i]['b:Bin'];
				ret['cartoes']['adicional'][k]['cardId'] = cartoes[i]['b:CardID'];
				k++;
			}
		}
		
		//TODO
		ret['dataCadastro'] = removeCaracter(obtemCampo('b:DataCadastro', contas[posDataMaior])[0],"/");
		ret['melhorDiaCompra'] = obtemCampo('b:MelhorDiaCompra', contas[posDataMaior])[0];
		ret['statusConta'] = obtemCampo('b:StatusConta', contas[posDataMaior])[0];
		ret['isFuncionario'] =  obtemCampo('b:IsFuncionario', contas[posDataMaior])[0];
		ret['limiteCredito'] = trataPontuacaoValor(obtemCampo('b:LimiteCredito', contas[posDataMaior])[0]);
		ret['diaVencimentoFatura'] = obtemCampo('b:DiaVencimentoFatura', contas[posDataMaior])[0];
		ret['numeroConta'] = obtemCampo('b:NumeroConta', contas[posDataMaior])[0];
		ret['produtoConta'] = obtemCampo('b:ProdutoConta', contas[posDataMaior])[0];
		ret['telefoneBlackList'] = obtemCampo('b:TelefoneBlackList', retorno)[0];
		ret['telefoneEncontrado'] = obtemCampo('b:TelefoneEncontrado', retorno)[0];
		ret['telefonePublico'] = obtemCampo('b:TelefonePublico', retorno)[0];
		ret['cpfTitular'] = obtemCampo('b:CpfTitular', retorno)[0];
		ret['qtdCartaoTitular'] = ret['cartoes']['titular'].length;
		ret['qtdCartaoAdicional'] = ret['cartoes']['adicional'].length;
		
		//caso nao venha string com valor, virah : {i:nil="true"}
		if (typeof ret['melhorDiaCompra'] === 'object') {
			ret['melhorDiaCompra'] = "";
		}
		if (typeof ret['diaVencimentoFatura'] === 'object') {
			ret['diaVencimentoFatura'] = "";
		}
	}
	
	return ret;
}

function trataRetornoServicoIdentificarClienteBI(retorno, dadosCliente) {
	//retorno : dadoscliente['identificarCliente'][...]
	var ret = {};
	
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['codigoErro'] = '0';
	if (ret['codRetorno'] == '99'){
		ret['codigoErro'] = obtemCampo('c:Codigo', retorno)[0];
	} 
	
	ret['sucesso'] = obtemCampo('b:Sucesso', retorno)[0];
	if (ret['sucesso'] == 'true' && ret['codRetorno'] == '0') {
		ret['dataCadastro'] = removeCaracter(obtemCampo('a:DataCadastroConta', retorno)[0], "/");
		ret['numeroConta'] = obtemCampo('a:NumeroConta', retorno)[0];
		ret['statusConta'] = obtemCampo('a:StatusConta', retorno)[0];

		ret['telefoneBlackList'] = obtemCampo('b:TelefoneBlackList', retorno)[0];
		ret['telefoneEncontrado'] = obtemCampo('b:TelefoneEncontrado', retorno)[0];
		ret['telefonePublico'] = obtemCampo('b:TelefonePublico', retorno)[0];
		
		dadosCliente['cartao'] = {};
		dadosCliente['cartao']['status'] = ret['statusConta'];
	}
	
	return ret;
}

function insereCartaoSelecionadoDadosCliente(dadosCliente, cartao){
	
	var cartoesTitulares = dadosCliente['identificarCliente']['cartoes']['titular'];
	var cartoesAdicionais = dadosCliente['identificarCliente']['cartoes']['adicional'];
	var documentoDigitado = cartao;

	if (documentoDigitado.length > 11){
		dadosCliente['cartao'] = {};
		for (var i = 0; i < cartoesTitulares.length; i++){
			if (cartoesTitulares[i]['numeroCartao'].indexOf(documentoDigitado) != -1){
				dadosCliente['cartao'] = cartoesTitulares[i];
				return dadosCliente;
			}
		}
		
		for (var j = 0; j < cartoesAdicionais.length; j++){
			if (cartoesAdicionais[j]['numeroCartao'].indexOf(documentoDigitado) != -1){
				dadosCliente['cartao'] = cartoesAdicionais[j];
				return dadosCliente;
			}
		}
	}
	return dadosCliente;
}

function getCartoesNorm(dadosCliente, cartoes){
	var statusCartao = '';
	var cartoesNorm = [];
	for (var i=0; i < cartoes.length; i++) {
		statusCartao = cartoes[i]['status'];
		if (isRecupera(dadosCliente, statusCartao)) {
		} else if (isFraude(dadosCliente, statusCartao)) {
		} else if (isCobranca(dadosCliente, statusCartao)) {
		} else if (isBloqueio(dadosCliente, statusCartao)) {
		} else { // norm
			cartoesNorm.push(cartoes[i]);
		}
	}
	return cartoesNorm;
}

function getCartaoNorm(dadosCliente, cartoes) {
	var statusCartao = '';
	for (var i=0; i < cartoes.length; i++) {
		statusCartao = cartoes[i]['status'];
		if (isRecupera(dadosCliente, statusCartao)) {
		} else if (isFraude(dadosCliente, statusCartao)) {
		} else if (isCobranca(dadosCliente, statusCartao)) {
		} else if (isBloqueio(dadosCliente, statusCartao)) {
		} else { // norm
			return cartoes[i];
		}
	}
	return {};
}

function trataRetornoValidaSenha(retorno) {
	var ret = {};
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['sucesso'] = obtemCampo('b:Sucesso', retorno)[0];
	ret['codigoErro'] = obtemCampo('c:Codigo', retorno)[0];
	return ret;
}

function trataRetornoServicoConsultarExtratoCompleto(retorno) {
	var ret = {};
	
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['sucesso'] = obtemCampo('b:Sucesso', retorno)[0];

	ret['vencimentoFatura'] = removeCaracter(obtemCampo('b:VencimentoFatura', retorno)[0], '/');
	ret['saldoDevedorNestaData'] = trataPontuacaoValor(obtemCampo('b:SaldoDevedorNestaData', retorno)[0]);

	var fatura = obtemCampo('b:DetalheDto', retorno);	
	ret['fatura'] = {};
	ret['fatura']['aberta'] = {};
	ret['fatura']['fechada'] = {};
	for (var i = 0; i < fatura.length; i++){
		if (fatura[i]['b:Identificacao'] == "aberta"){
			ret['fatura']['aberta']['faturaPagamentoMinimo'] = trataPontuacaoValor(fatura[i]['b:FaturaPagamentoMinimo']);
			ret['fatura']['aberta']['faturaSaldo'] = trataPontuacaoValor(fatura[i]['b:FaturaSaldo']);
			ret['fatura']['aberta']['faturaSaldoAberto'] = trataPontuacaoValor(fatura[i]['b:FaturaSaldoAberto']);
			ret['fatura']['aberta']['identificacao'] = fatura[i]['b:Identificacao'];
			ret['fatura']['aberta']['totalCreditos'] = trataPontuacaoValor(fatura[i]['b:TotalCreditos']);
			ret['fatura']['aberta']['transacoes'] = [];
			if (fatura[i]['b:Transacoes'] != undefined) {
				
				var lancamentos = obtemCampo('b:TransacaoDto', fatura[i]);
				
				if (lancamentos[0] != 404) {
					for (var j = 0; j < lancamentos.length; j++){
						ret['fatura']['aberta']['transacoes'][j] = {};
						ret['fatura']['aberta']['transacoes'][j]['dataLancamento'] = removeCaracter(lancamentos[j]['b:Data'], '/');
						ret['fatura']['aberta']['transacoes'][j]['indicadorDebCred'] = (lancamentos[j]['b:Valor'].substr(0,1) == "-") ? "-" : "+";
						ret['fatura']['aberta']['transacoes'][j]['valorLancamento'] = trataPontuacaoValor((lancamentos[j]['b:Valor'].substr(0,1) == "-") ? lancamentos[j]['b:Valor'].substr(1) : lancamentos[j]['b:Valor']);
					}
					ret['fatura']['aberta']['qtdeTransacoes'] = ret['fatura']['aberta']['transacoes'].length;
				} else {
					ret['fatura']['aberta']['qtdeTransacoes'] = 0;
				}				
			}		
		}
	}
	
	for (var i = 0; i < fatura.length; i++){
		if (fatura[i]['b:Identificacao'] == "fechadaUltima"){
			ret['fatura']['fechada']['faturaPagamentoMinimo'] = trataPontuacaoValor(fatura[i]['b:FaturaPagamentoMinimo']);
			ret['fatura']['fechada']['faturaSaldo'] = trataPontuacaoValor(fatura[i]['b:FaturaSaldo']);
			ret['fatura']['fechada']['faturaSaldoAberto'] = trataPontuacaoValor(fatura[i]['b:FaturaSaldoAberto']);
			ret['fatura']['fechada']['identificacao'] = fatura[i]['b:Identificacao'];
			ret['fatura']['fechada']['totalCreditos'] = trataPontuacaoValor(fatura[i]['b:TotalCreditos']);
			ret['fatura']['fechada']['transacoes'] = [];
			if (fatura[i]['b:Transacoes'] != undefined) {
				
				var lancamentos = obtemCampo('b:TransacaoDto', fatura[i]);
				
				if (lancamentos[0] != 404) {
					for (var j = 0; j < lancamentos.length; j++){
						ret['fatura']['fechada']['transacoes'][j] = {};
						ret['fatura']['fechada']['transacoes'][j]['dataLancamento'] = removeCaracter(lancamentos[j]['b:Data'], '/');
						ret['fatura']['fechada']['transacoes'][j]['indicadorDebCred'] = (lancamentos[j]['b:Valor'].substr(0,1) == "-") ? "-" : "+";
						ret['fatura']['fechada']['transacoes'][j]['valorLancamento'] = trataPontuacaoValor((lancamentos[j]['b:Valor'].substr(0,1) == "-") ? lancamentos[j]['b:Valor'].substr(1) : lancamentos[j]['b:Valor']);
					}
					ret['fatura']['fechada']['qtdeTransacoes'] = ret['fatura']['fechada']['transacoes'].length;
				} else {
					ret['fatura']['fechada']['qtdeTransacoes'] = 0;
				}
			}
		}
	}
	
	return ret;
 }

function trataRetornoExtratoCompacto(retorno) {
	var ret = {};
	
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['sucesso'] = obtemCampo('b:Sucesso', retorno)[0];
	ret['limiteComprasCarrefour'] = trataPontuacaoValor(obtemCampo('b:LimiteComprasCarrefour', retorno)[0]);
	ret['saldoDevedorNestaData'] = trataPontuacaoValor(obtemCampo('b:SaldoDevedorNestaData', retorno)[0]);
	ret['saldoDisponivelCompras'] = trataPontuacaoValor(obtemCampo('b:SaldoDisponivelCompras', retorno)[0]);
	ret['ultimoPagamentoData'] = obtemCampo('b:UltimoPagamentoData', retorno)[0];
	if (typeof ret['ultimoPagamentoData'] === 'object') {
		ret['ultimoPagamentoData'] = "";
	}
	ret['ultimoPagamentoValor'] = trataPontuacaoValor(obtemCampo('b:UltimoPagamentoValor', retorno)[0]);
	if (typeof ret['ultimoPagamentoValor'] === 'object') {
		ret['ultimoPagamentoValor'] = "";
	}
	ret['vencimento'] = removeCaracter(obtemCampo('b:Vencimento', retorno)[0],'/');
	
	return ret;
}

function trataRetornoConsultarCodigoDeBarras(retorno) {
	var ret = {};
	
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['descricao'] = obtemCampo('b:Descricao', retorno)[0];
	ret['dataVencimento'] = removeCaracter(obtemCampo('b:DataVencimento', retorno)[0],'/');
	ret['faturaPagamentoMinimo'] = trataPontuacaoValor(obtemCampo('b:FaturaPagamentoMinimo', retorno)[0]);
	ret['codigoDeBarras'] = obtemCampo('b:LinhaDigitavelBanco', retorno)[0];
	ret['valorFatura'] = trataPontuacaoValor(obtemCampo('b:ValorFatura', retorno)[0]);
	ret['isFechada'] = obtemCampo('b:isFechada', retorno)[0];
	
	//caso nao venha string com valor, virah : {i:nil="true"}
	if (typeof ret['dataVencimento'] === 'object') {
		ret['dataVencimento'] = "";
	}

	return ret;
}

/*
 * 	"codRetorno": "0",
	"descricao": "Operação realizada com sucesso",
	"dataVencimento": "10112020",
	"faturaPagamentoMinimo": "2158307",
	"codigoDeBarras": "23792.37205 61000.000085 92000.429008 5 00000000000000",
	"valorFatura": "13489421",
	"isFechada": "true"
 * 
 */

function trataRetornoDesbloqueio(retorno) {
	var ret = {};
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['sucesso'] = obtemCampo('b:Sucesso', retorno)[0];
	//ret['token'] = obtemCampo('a:Token', retorno)[0];
	return ret;
}

function trataRetornoBloqueio(retorno) {
	var ret = {};
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['sucesso'] = obtemCampo('b:Sucesso', retorno)[0];
	return ret;
}

function trataRetornoCreditoPessoal(retorno) {
	var ret = {};
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['sucesso'] = obtemCampo('b:Sucesso', retorno)[0];
	ret['emprestimoDisponivel'] = obtemCampo('a:EmprestimosDisponiveis', retorno)[0];
	ret['emprestimosPermitidos'] = obtemCampo('a:EmprestimosPermitidos', retorno)[0];
	ret['limiteTotal'] = trataPontuacaoValor(obtemCampo('a:LimiteTotal', retorno)[0]);
	ret['saldoDisponivel'] = trataPontuacaoValor(obtemCampo('a:SaldoDisponivel', retorno)[0]);
	return ret;
}

function preparaParametrosValidaSenha(dadosCliente, pinblock){
	var ret = {};
	
	ret['numeroCartao'] = dadosCliente['cartao']['numeroCartao'];
	ret['numeroSerial'] = 'FIXED-URA-WKGKEY';
	ret['senhaCartao'] = pinblock;

	return ret;
}

function preparaParametrosConsultarExtratoCompactoFatura(dadosCliente){
	var ret = {};
	
	ret['numeroCartao'] = dadosCliente['cartao']['numeroCartao'];
	
	return ret;
}

function preparaParametrosCreditoPessoal(dadosCliente){
	var ret = {};
	
	ret['numeroCartao'] = dadosCliente['cartao']['numeroCartao'];
	
	return ret;
}

function preparaParametrosConsultarCodigoDeBarras(dadosCliente){
	var ret = {};	
	ret['numeroCartao'] = dadosCliente['cartao']['numeroCartao'];	
	return ret;
}

function preparaParametrosServicoBloqueio(dadosCliente){
	var ret = {};
	ret['numeroCartao'] = dadosCliente['cartao']['numeroCartao'];
	ret['tipoBloqueio'] = dadosCliente['tipoBloqueio'];
	return ret;
}

function trataRetornoServicoBloqueio(retorno) {
	var ret = {};
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['sucesso'] = obtemCampo('b:Sucesso', retorno)[0];
	return ret;
}

function preparaParametrosServicoEmprestimoCreditoPessoal(dadosCliente){
	var ret = {};
	
	ret['numeroCartao'] = dadosCliente['cartao']['numeroCartao'];
	
	return ret;
}

function preparaParametrosServicoConsultarFaturasFechadas(dadosCliente){
	var ret = {};
	
	ret['numeroCartao'] = dadosCliente['cartao']['numeroCartao'];
	ret['numeroConta'] = dadosCliente['identificarCliente']['numeroConta'];
	ret['cpf'] = dadosCliente['identificarCliente']['cpfTitular'];
	
	return ret;
}

function trataRetornoServicoConsultarFaturasFechadas(retorno) {
	var ret = {};
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['sucesso'] = obtemCampo('b:Sucesso', retorno)[0];
	ret['id'] = obtemCampo('b:Id', retorno)[0];
	return ret;
}

function preparaParametrosObterTipoEnvioFatura(dadosCliente){
	var ret = {};
	
	ret['numeroCartao'] = dadosCliente['cartao']['numeroCartao'];
	ret['numeroConta'] = dadosCliente['identificarCliente']['numeroConta'];
	
	return ret;
}

function trataRetornoObterTipoEnvioFatura(retorno) {
	var ret = {};
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['sucesso'] = obtemCampo('b:Sucesso', retorno)[0];
	ret['email'] = obtemCampo('a:Email', retorno)[0];
	return ret;
}

function preparaParametrosEnviarFaturaPorEmail(dadosCliente){
	var ret = {};
	
	ret['numeroConta'] = dadosCliente['identificarCliente']['numeroConta'];
	ret['email'] = dadosCliente['servicoObterTipoEnvioFatura']['email'];
	ret['id'] = dadosCliente['servicoConsultarFaturasFechadas']['id'];
	
	return ret;
}

function trataRetornoEnviarFaturaPorEmail(retorno) {
	var ret = {};
	ret['codRetorno'] = obtemCampo('b:Codigo', retorno)[0];
	ret['sucesso'] = obtemCampo('b:Sucesso', retorno)[0];
	return ret;
}

function preparaParametrosEnviarOperacaoURA(dadosCliente, ani) {
	var ret = {};
	ret['cpf'] = '';
	ret['numeroConta'] = '0';
	ret['numeroCartao'] = '';
	if (dadosCliente['identificarCliente']) {
		ret['cpf'] = dadosCliente['identificarCliente']['cpfTitular'] != undefined ? dadosCliente['identificarCliente']['cpfTitular'] : '';
	  	ret['numeroConta'] = dadosCliente['identificarCliente']['numeroConta'] != undefined ? dadosCliente['identificarCliente']['numeroConta'] : '';
	}
	if (dadosCliente['cartao']) {
		ret['numeroCartao'] = dadosCliente['cartao']['numeroCartao'] != undefined ? dadosCliente['cartao']['numeroCartao'] : '';
	}
	if (ani != '' && ani != undefined) {
		ret['ddd'] = ani.substr(0, 2);
		ret['telefone'] = ani.substr(2);
	}
  	ret['dataLigacao'] = formataDataAtual("yyyyMMdd");
  	ret['horaLigacao'] = formataDataAtual("hhmmss");  	
  	ret['payTrue'] = dadosCliente['paytrue'];
  	
  	return ret;
}

//### INICIO SERVICOS LIGHT

function preparaParametrosConsultarProtocolo(dadosCliente){
	var ret = {};	
	ret['x_partner'] = dadosCliente['identificacao']['numeroCliente']; //Numero do cliente (parceiro de negocios SAP)
	ret['x_category'] = 'Z02'; //Canal de atendimento (para a URA preencher com Z02)
	return ret;
}

function trataRetornoConsultarProtocolo(retorno) {
	var ret = {};
	
	ret['codRetorno'] = obtemCampo('b:y_return', retorno)[0]; // confirmar o nome da tag que retorna no servico
	ret['descricao'] = obtemCampo('b:y_message', retorno)[0];
	ret['protocolo'] = obtemCampo('b:y_protocolo', retorno)[0];
	ret['guid'] = obtemCampo('b:y_guid', retorno)[0];
	
	return ret;
}


function preparaParametrosConsultarIdentificacao(dadosCliente){
	var ret = {};	
	
	ret['x_opcao'] = dadosCliente['opcaoPesquisa']; //1 para pesquisar cliente, ou 3 para verificar se instalação é do cliente
	ret['x_partner'] = ''; //Número do cliente
	ret['x_anlage'] = ''; //Número da instalação
	
	if (ehCPFCNPJouInstalacao(dadosCliente['documentoDigitado']) == 'cpf') {
		ret['x_partner'] = dadosCliente['documentoDigitado'];
	} else if (ehCPFCNPJouInstalacao(dadosCliente['documentoDigitado']) == 'instalacao') {
		ret['x_anlage'] = dadosCliente['documentoDigitado'];
	}
	
	ret['x_telefone'] = AppState.ANI; //Número do telefone no formato DDNNNNNNNNN (DDD+número)
	ret['x_cpf'] = ''; //Número do CPF
	ret['x_cnpj'] = ''; //Número do CNPJ
	
	if (ehCPFCNPJouInstalacao(dadosCliente['documentoDigitado']) == 'cpf') {
		ret['x_cpf'] = dadosCliente['documentoDigitado'];
	} else if (ehCPFCNPJouInstalacao(dadosCliente['documentoDigitado']) == 'cnpj') {
		ret['x_cnpj'] = dadosCliente['documentoDigitado'];
	}
	
	ret['x_dt_notas'] = formataDataAtual("yyyyMMdd-hhmmss"); //Data de Referencia
	
	if (dadosCliente['ambiente'] == 'producao') {
		ret['x_dt_notas'] = '';		
	} 
	ret['x_max_instals'] = ''; // Numero max de instalacoes
	
	return ret;
}


function trataRetornoConsultarIdentificacao(retorno) {
	var ret = {};
		
	ret['codRetorno'] = obtemCampo('b:y_return', retorno)[0]; // confirmar o nome da tag que retorna no servico
	
	if (ret['codRetorno'] == '0') {
		
		ret['numClienteParceiro'] = obtemCampo('b:y_partner', retorno)[0]; // confirmar o nome da tag que retorna no servico
		ret['nomeCliente'] = obtemCampo('b:y_nome', retorno)[0]; // confirmar o nome da tag que retorna no servico
		ret['descricao'] = obtemCampo('b:y_message', retorno)[0];
		ret['cpfOucnpj'] = obtemCampo('b:y_cpf_cnpj', retorno)[0];
		ret['tipoCliente'] = obtemCampo('b:y_tp_cliente', retorno)[0];	
		ret['tipoCliente'] = obtemCampo('b:y_tel_ger', retorno)[0];
		ret['tipoCliente'] = obtemCampo('b:y_tp_cliente', retorno)[0];
		ret['tipoCliente'] = obtemCampo('b:y_tp_cliente', retorno)[0];
		ret['tipoCliente'] = obtemCampo('b:y_tp_cliente', retorno)[0];
		ret['telefoneGerenteConta'] = obtemCampo('b:y_tp_cliente', retorno)[0]; //Se Y_TP_CLIENTE for diferente de Z001, telefone do gerente de contas responsável pelo atendimento, no formato DDNNNNNNNNN
		
		var qtdInstalacoes = obtemCampo('b:y_qtinst', retorno);
		ret['qtdInstalacoes'] = qtdInstalacoes.length;
		
		for (var y=0; y < qtdInstalacoes.length; y++) {
			var j = 0;
			ret['instalacoes'] = []; //yt_instals		
			ret['instalacoes'][j] = {};
			ret['instalacoes'][j]['anlage'] = qtdInstalacoes[y]['b:anlage'];
			ret['instalacoes'][j]['vertrag'] = qtdInstalacoes[y]['b:vertrag'];
			ret['instalacoes'][j]['dt_ligacao'] = qtdInstalacoes[y]['b:dt_ligacao'];
			ret['instalacoes'][j]['vkont'] = qtdInstalacoes[y]['b:vkont'];
			ret['instalacoes'][j]['tp_instal'] = qtdInstalacoes[y]['b:tp_instal'];
			ret['instalacoes'][j]['cod_rua'] = qtdInstalacoes[y]['b:cod_rua'];
			ret['instalacoes'][j]['rua'] = qtdInstalacoes[y]['b:rua'].toLowerCase();
			ret['instalacoes'][j]['cod_bairro'] = qtdInstalacoes[y]['b:cod_bairro'];
			ret['instalacoes'][j]['bairro'] = qtdInstalacoes[y]['b:bairro'];
			ret['instalacoes'][j]['cod_cidade'] = qtdInstalacoes[y]['b:cod_cidade'];
			ret['instalacoes'][j]['cidade'] = qtdInstalacoes[y]['b:cidade'];
			ret['instalacoes'][j]['cep'] = qtdInstalacoes[y]['b:cep'];
			ret['instalacoes'][j]['endereco'] = qtdInstalacoes[y]['b:endereco'];
			j++;		
		}

		
		var qtdNotas = obtemCampo('b:yt_notas', retorno);
		ret['qtdNotas'] = qtdNotas.length;

		for (var a=0; a < qtdNotas.length; a++) {
			var b = 0;
			ret['notas'] = []; //yt_instals		
			ret['notas'][b] = {};
			ret['notas'][b]['qmnum'] = qtdNotas[a]['b:qmnum'];
			ret['notas'][b]['qmart'] = qtdNotas[a]['b:qmart'];
			ret['notas'][b]['anlage'] = qtdNotas[a]['b:anlage'];
			ret['notas'][b]['fegrp'] = qtdNotas[a]['b:fegrp'];
			ret['notas'][b]['fecod'] = qtdNotas[a]['b:fecod'];
			ret['notas'][b]['qmdat'] = qtdNotas[a]['b:qmdat'];
			ret['notas'][b]['status'] = qtdNotas[a]['b:status'];
			ret['notas'][b]['ltrmn'] = qtdNotas[a]['b:ltrmn'];
			ret['notas'][b]['ltrur'] = qtdNotas[a]['b:ltrur'];
			ret['notas'][b]['ausbs'] = qtdNotas[a]['b:ausbs'];
			ret['notas'][b]['austb'] = qtdNotas[a]['b:austb'];
			ret['notas'][b]['mngrp'] = qtdNotas[a]['b:mngrp'];
			ret['notas'][b]['mncod'] = qtdNotas[a]['b:mncod'];
			b++;		
		}
		
		ret['y_partner_guid'] = obtemCampo('b:y_partner_guid', retorno)[0];	
		
	}	
			
	return ret;

}


function preparaParametrosConsultarModificarProtocolo(dadosCliente){
	var ret = {};	
	ret['x_partner'] = dadosCliente['identificacao']['numeroCliente']; //Numero do cliente (parceiro de negocios SAP)
	ret['x_protocolo'] = dadosCliente['protocolo']; //Número do protocolo a modificar
	return ret;
}


function trataRetornoConsultarModificarProtocolo(retorno) {
	var ret = {};
	
	ret['codRetorno'] = obtemCampo('b:y_return', retorno)[0]; // confirmar o nome da tag que retorna no servico
	ret['descricao'] = obtemCampo('b:y_message', retorno)[0];
	//ret['protocolo'] = obtemCampo('b:y_protocolo', retorno)[0];
	return ret;
}


function preparaParametrosConsultarObterDadosInstalacao(dadosCliente){
	var ret = {};	
	
	ret['x_anlage'] = dadosCliente['numeroInstalacao'];//Número da instalação
	ret['x_vertrag'] = dadosCliente['numeroContrato'];//Número do contrato
	ret['x_vkont'] = dadosCliente['numeroContrato']; //Número da conta contrato
	ret['x_partner'] = dadosCliente['numeroParceiro'];
	ret['x_partner'] = ''; //Opcional, preencha com ‘X’ se quiser que o web service devolva informações de consumo na tabela YT_CONSUMOS
	
	return ret;
}


function trataRetornoConsultarObterDadosInstalacao(retorno) {
	var ret = {};
		
	ret['codRetorno'] = obtemCampo('b:y_return', retorno)[0]; // Código de retorno (diferente de zero é erro)
	
	if (ret['codRetorno'] == '0') {
		
		ret['descricao'] = obtemCampo('b:y_message', retorno)[0]; // Mensagem de Erro
		ret['y_fraude'] = obtemCampo('b:y_fraude', retorno)[0]; // Preenchido com ‘X’ se a instalação tem fraude constatada
		ret['y_liminar'] = obtemCampo('b:y_liminar', retorno)[0]; // Preenchido com ‘X’ se a instalação tem liminar		
		ret['y_tipo_lig'] = obtemCampo('b:y_tipo_lig', retorno)[0]; // 1 = Monofásico, 2 = Bifásico, 3 = Trifásico
		ret['y_telemed'] = obtemCampo('b:y_telemed', retorno)[0]; //Preenchido com ‘X’ se instalação é telemedida	
		ret['y_deb_aut'] = obtemCampo('b:y_deb_aut', retorno)[0]; //  Preenchido com ‘X’ se cadastrada para débito automático
		ret['y_dt_prox_leit'] = obtemCampo('b:y_dt_prox_leit', retorno)[0]; // Data da próxima leitura prevista para a instalação		
		ret['ye_suspensao'] = obtemCampo('b:ye_suspensao', retorno)[0]; //Informações de instalação cortada:		
		ret['discno'] = obtemCampo('b:discno', retorno)[0]; //Número do documento de suspensão
		ret['mot_corte'] = obtemCampo('b:mot_corte', retorno)[0]; // Motivo do corte:
		ret['status'] = obtemCampo('b:status', retorno)[0];  //Status do documento de suspensão
		
		var qtdFaturas = obtemCampo('b:y_qtfat', retorno); //Quantidade de faturas em aberto (vencidas e a vencer) da instalação
		ret['qtdFaturas'] = qtdFaturas.length;
		
		for (var y=0; y < qtdFaturas.length; y++) {
			var j = 0;
			ret['faturas'] = []; //		
			ret['faturas'][j] = {};
			ret['faturas'][j]['invoice'] = obtemCampo(qtdFaturas[y]['b:invoice']); //testar com e sem obtemCampo
			ret['faturas'][j]['billing_period'] = obtemCampo(qtdFaturas[y]['b:billing_period']);
			ret['faturas'][j]['dtvenc'] = obtemCampo(qtdFaturas[y]['b:dtvenc']);
			ret['faturas'][j]['betrw'] = obtemCampo(qtdFaturas[y]['b:betrw']);
			ret['faturas'][j]['printdoc'] = obtemCampo(qtdFaturas[y]['b:printdoc']);
			ret['faturas'][j]['codbarras'] = obtemCampo(qtdFaturas[y]['b:codbarras']);
			
			j++;		
		}
	
	ret['yt_consumos'] = obtemCampo('b:yt_consumos', retorno)[0];	
		
	}	
			
	return ret;

}


function preparaParametrosConsultarHistoricoConsumos(dadosCliente){
	var ret = {};	
	ret['x_vertrag'] = dadosCliente['numeroContrato'];//Número do contrato
	ret['_data_ini'] = dadosCliente['dataInicioConsumo']; //Opcional, data início do período para o qual se deseja os consumos
	ret['x_data_fim'] = dadosCliente['dataFimConsumo']; //Opcional, data fim do período para o qual se deseja os consumos

	return ret;
}


function trataRetornoConsultarHistoricoConsumos(retorno) {
	var ret = {};
	
	ret['codRetorno'] = obtemCampo('b:y_return', retorno)[0]; // Código de retorno (diferente de zero é erro)
	
	if (ret['codRetorno'] == '0') {
		
		ret['y_message'] = obtemCampo('b:y_message', retorno)[0]; // Mensagem de erro, caso ocorra
		ret['nomeCliente'] = obtemCampo('b:y_nome', retorno)[0]; // confirmar o nome da tag que retorna no servico
		var qtdConsumos = obtemCampo('b:yt_consumos', retorno);
		ret['qtdConsumos'] = qtdConsumos.length;
		
		for (var y=0; y < qtdConsumos.length; y++) {  // reavaliar a estrutura do json quando tiver o response em maoes e testar a funcao
			var j = 0;
			ret['consumos'] = []; //yt_instals		
			ret['consumos'][j] = {};
			ret['consumos'][j]['vertrag'] = qtdInstalacoes[y]['b:vertrag'];
			ret['consumos'][j]['mes_ref'] = qtdInstalacoes[y]['b:mes_ref'];
			ret['consumos'][j]['x_data_ini'] = qtdInstalacoes[y]['b:x_data_ini'];
			ret['consumos'][j]['x_data_fim'] = qtdInstalacoes[y]['b:x_data_fim'];
			ret['consumos'][j]['consumo'] = qtdInstalacoes[y]['b:consumo'];
			ret['consumos'][j]['dias'] = qtdInstalacoes[y]['b:dias'];
			ret['consumos'][j]['media_dia'] = qtdInstalacoes[y]['b:media_dia'].toLowerCase();
			ret['consumos'][j]['flag_media'] = qtdInstalacoes[y]['b:flag_media'];
			ret['consumos'][j]['nota_leitur'] = qtdInstalacoes[y]['b:nota_leitur'];
			ret['consumos'][j]['ctg_tarifa'] = qtdInstalacoes[y]['b:ctg_tarifa'];
			ret['consumos'][j]['flag_fat_'] = qtdInstalacoes[y]['b:flag_fat_'];
			ret['consumos'][j]['ctg_tarifa'] = qtdInstalacoes[y]['b:ctg_tarifa'];
			ret['consumos'][j]['val_fatura'] = qtdInstalacoes[y]['b:val_fatura'];
			ret['consumos'][j]['val_consumo'] = qtdInstalacoes[y]['b:val_consumo'];
			ret['consumos'][j]['aliq_icms'] = qtdInstalacoes[y]['b:aliq_icms'];
			ret['consumos'][j]['doc_calc'] = qtdInstalacoes[y]['b:doc_calc'];
			ret['consumos'][j]['doc_impr'] = qtdInstalacoes[y]['b:doc_impr'];
			ret['consumos'][j]['bandeira'] = qtdInstalacoes[y]['b:bandeira'];
			ret['consumos'][j]['valtar'] = qtdInstalacoes[y]['b:valtar'];
			ret['consumos'][j]['classe'] = qtdInstalacoes[y]['b:classe'];
			ret['consumos'][j]['media_consumo'] = qtdInstalacoes[y]['b:media_consumo'];
			ret['consumos'][j]['tipo_fat'] = qtdInstalacoes[y]['b:tipo_fat'];	
			j++;		
		}
	}	
			
	return ret;

}


function preparaParametrosConsultarValidarPossibilidadeReligacao(dadosCliente){
	var ret = {};
	
	ret['x_partner'] = dadosCliente['identificarCliente']['numeroParceiro']; //Número do cliente (parceiro de negócios SAP)
	ret['x_anlage'] = dadosCliente['identificarCliente']['numeroInstalacao']; //Número da instalação
	ret['x_vkont'] = dadosCliente['identificarCliente']['numeroContrato']; //Número da conta contrato
	ret['x_limite'] = '2'; // Número máximo de débitos (default=2, não precisa preencher)
	ret['x_dias_venc'] = '3'; //Número máximo de dias após o vencimento (default=3 dias úteis, não precisa preencher
	ret['x_dias_apos_'] = '9' ; //Número máximo de dias após o corte (default=9, não precisa preencher)
	ret['x_total_deb'] = ''; // Valor máximo do débito (default=5000,00, não precisa preencher)
	ret['x_protocolo'] = dadosCliente['protocolo']; //Número do protocolo inicial informado ao cliente
    ret['x_category'] = 'Z02'; //Canal de atendimento (URA = Z02)
		
	return ret;
}


function trataRetornoConsultarValidarPossibilidadeReligacao(retorno) {
	var ret = {};
		
	ret['codRetorno'] = obtemCampo('b:y_return', retorno)[0]; // Código de retorno (diferente de zero é erro)
	
	if (ret['codRetorno'] == '0') {
		
		ret['y_discno'] = obtemCampo('b:y_discno', retorno)[0]; // Número do documento de suspensão
		ret['y_message'] = obtemCampo('b:y_message', retorno)[0]; // confirmar o nome da tag que retorna no servico
		var qtdRetorno = obtemCampo('b:ti_retorno', retorno);
		ret['qtdRetorno'] = qtdRetorno.length;
		
		for (var y=0; y < qtdRetorno.length; y++) {  // reavaliar a estrutura do json quando tiver o response em maoes e testar a funcao
			var j = 0;
			ret['retorno'] = []; //yt_instals		
			ret['retorno'][j] = {};
			ret['retorno'][j]['invoice'] = qtdInstalacoes[y]['b:invoice'];
			ret['retorno'][j]['billing_period'] = qtdInstalacoes[y]['b:billing_period'];
			ret['retorno'][j]['dtemissao'] = qtdInstalacoes[y]['b:dtemissao'];
			ret['retorno'][j]['dtvenc'] = qtdInstalacoes[y]['b:dtvenc'];
			ret['retorno'][j]['dtpagto'] = qtdInstalacoes[y]['b:dtpagto'];
			ret['retorno'][j]['betrw'] = qtdInstalacoes[y]['b:betrw'];
			j++;		
		}
	}	
			
	return ret;

}



function preparaParametrosConsultarSolicitarReligacao(dadosCliente){
	var ret = {};
	
	var faturaDigitada = dadosCliente['faturaDigitada'];
		
	ret['x_discno'] = dadosCliente['identificarCliente']['x_discno']; //Número do documento de suspensão
	ret['x_partner'] = dadosCliente['identificarCliente']['x_partner']; // Número do cliente (parceiro de negócios SAP)
	ret['x_anlage'] = dadosCliente['documentoDigitado']; // Número da instalação
	ret['x_texto'] = ''; //Não usar
	ret['x_uname'] = ''; //Não usar
	ret['x_vkont'] = dadosCliente['numeroContaContrato'];  //Número da conta contrato
	
	var qtdFaturas = dadosCliente['qtdFaturasDigitado'].length; // Quantidade de Faturas alegado pelo cliente
		
	for (var y=0; y < qtdFaturas.length; y++) {  // reavaliar a estrutura do json quando tiver o response em maos e testar a funcao
		var j = 0;
		ret['xt_faturas'] = [];  //Uma linha para cada fatura com pagamento alegado pelo cliente:
		ret['xt_faturas'][j] = {};
		ret['xt_faturas'][j]['invoice'] = faturaDigitada[y]['numeroFatura']; //Número da fatura
		ret['xt_faturas'][j]['billing_period'] = faturaDigitada[y]['mesReferencia']; //Mês de referência no formato AAAA/MM
		ret['xt_faturas'][j]['dtvenc'] = faturaDigitada[y]['dataVencimento']; //Data de vencimento no formato AAAA-MM-DD
		ret['xt_faturas'][j]['betrw'] = faturaDigitada[y]['valorFatura']; //Valor da fatura
		ret['xt_faturas'][j]['dtpagto_aleg'] = faturaDigitada[y]['dataPagamento']; //Data de pagamento alegada pelo cliente no formato AAAA-MM-DD
		ret['xt_faturas'][j]['autentic_aleg'] = faturaDigitada[y]['numeroAutenticacaoDigitado']; //Autenticação digitada pelo cliente
		j++;		
	}
	
	ret['x_protocolo'] = dadosCliente['protocolo']; // Número do protocolo inicial fornecido ao cliente
	ret['x_category'] = 'Z02'; // Canal de atendimento (URA = Z02)
		
	return ret;
}


function trataRetornoConsultarSolicitarReligacao(retorno) {
	var ret = {};
	
	ret['codRetorno'] = obtemCampo('b:y_return', retorno)[0]; // Código de retorno (diferente de zero é erro)
	
	if (ret['codRetorno'] == '0') {
		ret['y_qmnum'] = obtemCampo('b:y_qmnum', retorno)[0];
		ret['y_message'] = obtemCampo('b:y_message', retorno)[0];
		ret['y_data_prevista'] = obtemCampo('b:y_data_prevista', retorno)[0];
		ret['y_hora_prevista'] = obtemCampo('b:y_hora_prevista', retorno)[0];
		ret['y_contato'] = obtemCampo('b:y_contato', retorno)[0];
	}
	
	return ret;
}


function preparaParametrosConsultarCadastrarTelefoneParceiro(dadosCliente){
	var ret = {};	
	ret['x_partner'] = dadosCliente['numeroParceiro'];//Número do parceiro
	ret['x_tipo_tel'] = '1'; //criar logica //Preencher com: 1 se telefone residencial, 2 se celular, 3 se telefone comercial
	ret['x_telefone'] = dadosCliente['dataFimConsumo']; //Número do telefone no formato DDNNNNNNNN se telefone residencial ou comercial. Se for celular, o número pode ter 8 ou 9 dígitos: DDNNNNNNNN ou DDNNNNNNNNN.
	ret['x_protocolo'] = dadosCliente['protocolo']; //Número do protocolo inicial fornecido ao cliente
	ret['x_category'] = 'Z02'; //Canal de atendimento (URA = Z02)
	return ret;
}


function trataRetornoConsultarCadastrarTelefoneParceiro(retorno) {
	var ret = {};
	
	ret['codRetorno'] = obtemCampo('b:y_return', retorno)[0]; // Código de retorno (diferente de zero é erro)
	
	if (ret['codRetorno'] == '0') {
		ret['y_message'] = obtemCampo('b:y_message', retorno)[0];
		ret['y_contato'] = obtemCampo('b:y_contato', retorno)[0];
				
	}
	
	return ret;
}



function preparaParametrosConsultarCodigoDeBarras(dadosCliente, faturaDigitada){
	var ret = {};	
	var faturaEscolhida = faturaDigitada;
	var fatura = dadosCliente['obterDadosInstalacao']['yt_faturas'];
	
	ret['x_partner'] = dadosCliente['numeroParceiro'];//Número do parceiro
	ret['x_anlage'] = dadosCliente['numeroInstalacao']; //Número da instalação
	ret['x_fatura'] = dadosCliente['obterDadosInstalacao']['yt_faturas']; // criar logica para pegar a fatura escolhida //Fatura escolhida pelo cliente. Copiar dados fornecidos pelo web service DADOS_INSTALACAO na tabela YT_FATURAS
	ret['x_celular'] =  AppState.ANI; //Número do Celular para enviar SMS, com DDD + 8 ou 9 dígitos. Deixar este campo em branco caso o código de barras tenha sido vocalizado pela URA.
	ret['x_protocolo'] = dadosCliente['protocolo']; //Número do protocolo inicial fornecido ao cliente
	ret['x_category'] = 'Z02'; //Canal de atendimento (URA = Z02)
	return ret;
	
}


function trataRetornoConsultarCodigoDeBarras(retorno) {
	var ret = {};
	
	ret['codRetorno'] = obtemCampo('b:y_return', retorno)[0]; // Código de retorno (diferente de zero é erro)
	
	if (ret['codRetorno'] == '0') {
		ret['y_message'] = obtemCampo('b:y_message', retorno)[0];
		ret['y_contato'] = obtemCampo('b:y_contato', retorno)[0];
				
	}
	
	return ret;
}



function preparaParametrosConsultarCadastrarDebitoAutomatico(dadosCliente){
	var ret = {};	
		
	ret['x_partner'] = dadosCliente['numeroParceiro'];//Número do parceiro
	ret['x_anlage'] = dadosCliente['numeroInstalacao']; //Número da instalação
	ret['x_opcao'] = '1'; // Tipo de processamento requerido 1 - Esta opção é usada para obter as informações de débito automático da instalação, 2- Esta opção faz o cadastramento do débito automático da instalação na conta bancária informada e 3 - Esta opção deve ser usada quando o cliente solicita o descadastramento do débito automático
	ret['x_banco'] =  dadosCliente['codBanco']; //Código do banco (3 dígitos
	ret['x_agencia'] = dadosCliente['codAgenciaBancaria']; //Código da agência bancária (4 dígitos)
	ret['x_conta'] = dadosCliente['numeroContaCorrente']; //Número da conta corrente sem o dígito verificador
	ret['x_dv'] = dadosCliente['digitoVerificadorConta']; //Dígito verificador da conta corrente
	ret['x_protocolo'] = dadosCliente['protocolo']; //Número do protocolo inicial fornecido ao cliente
	ret['x_category'] = 'Z02'; //Canal de atendimento (URA = Z02)
	
	return ret;
	
}


function trataRetornoConsultarCadastrarDebitoAutomatico(retorno) {
	var ret = {};
	
	ret['codRetorno'] = obtemCampo('b:y_return', retorno)[0]; // Código de retorno (diferente de zero é erro)
	
	if (ret['codRetorno'] == '0') {
		ret['y_banco'] = obtemCampo('b:y_banco', retorno)[0];
		ret['y_agencia'] = obtemCampo('b:y_agencia', retorno)[0];
		ret['y_conta'] = obtemCampo('b:y_conta', retorno)[0];
		ret['y_dv'] = obtemCampo('b:y_dv', retorno)[0];
		ret['y_contato'] = obtemCampo('b:y_contato', retorno)[0];
		ret['message'] = obtemCampo('b:message', retorno)[0];
	}
	
	return ret;
}


function preparaParametrosConsultarSegundaViaCorreio(dadosCliente){
	var ret = {};	
		
	ret['x_opbel'] = dadosCliente['x_opbel'];//Número da fatura
	ret['x_partner'] = dadosCliente['x_opbel']; // Número de identificação do parceiro
	ret['x_protocolo'] = dadosCliente['protocolo']; // Número do protocolo inicial fornecido ao cliente
	ret['x_category'] = 'Z02'; //Canal de atendimento (URA = Z02)
	
	return ret;
	
}


function trataRetornoConsultarSegundaViaCorreio(retorno) {
	var ret = {};
	
	ret['codRetorno'] = obtemCampo('b:y_return', retorno)[0]; // Código de retorno (diferente de zero é erro)
	
	if (ret['codRetorno'] == '0') {
		ret['y_contato'] = obtemCampo('b:y_contato', retorno)[0];
		ret['message'] = obtemCampo('b:message', retorno)[0];
	}
	
	return ret;
}

function preparaParametrosConsultarGerarContato(dadosCliente){
	var ret = {};	
		
	ret['x_partner'] = dadosCliente['x_partner'];//Número do parceiro ou 7777777
	ret['x_process_type'] = 'ZI22'; //Tipo de contato
	ret['x_category'] = 'Z02'; // Não usar
	ret['x_description'] = dadosCliente['x_description']; // Descrição do contato
	ret['x_code_group'] = 'ZINFO022' ; // Grupo de códigos do contato
	ret['x_code'] = 'Z001' ; // Código do motivo do contato //Z001 = Informações Autoatendimento 	Z002 = Cliente identificado pelo telefone 	Z003 = Cliente identificado pelo CPF 	Z004 = Cliente identificado pelo CNPJ
	//Z005 = Cliente identificado pela instalação
	ret['x_resp'] = ''; // Não usar
	ret['x_instal'] = dadosCliente['x_instal']; // Número da instalação
	ret['x_solic'] = ''; // Não usar
	ret['x_nota'] = dadosCliente['x_nota']; // Número da nota de serviço (se for o caso)
	ret['x_protocolo'] = dadosCliente['x_protocolo']; // Número do protocolo inicial fornecido ao cliente
	ret['xt_texto'] = [{"tdline" : dadosCliente['dadosNavegacao']}]; // Um registro para cada linha de texto do contato. Preencher apenas o campo abaixo:
	   
		  
	return ret;
	
}
	
	function trataRetornoConsultarGerarContato(retorno) {
		var ret = {};
		
		ret['codRetorno'] = obtemCampo('b:y_return', retorno)[0]; // Código de retorno (diferente de zero é erro)
		
		if (ret['codRetorno'] == '0') {
			ret['y_contato'] = obtemCampo('b:y_contato', retorno)[0];
			ret['message'] = obtemCampo('b:message', retorno)[0];
		}
		
		return ret;
	}


