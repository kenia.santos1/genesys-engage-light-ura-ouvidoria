function setParametrosDerivacao(dadosCliente, destino) {
	var ret = {};
	ret['ptderivacao'] = '';	
	ret['rp'] = '';
	
	var destino = destino;
	
	switch (destino) {
		case 'pesquisaEmpresarial_R1':
			ret['rp'] = '75731';
			break;
		case 'pesquisaEmpresarial_R2':
			ret['rp'] = '75732';
			break;
		case 'pesquisaEmpresarialMedias_R1':
			ret['rp'] = '75151';
			break;
		case 'pesquisaEmpresarialMedias_R2':
			ret['rp'] = '75890';
			break;
		case 'pesquisaCorpOnline_R1':
			ret['rp'] = '75890';
			break;
		case 'pesquisaCorpOnline_R2':
			ret['rp'] = '75890';
			break;

	}
		
	return ret;
}

function getAttachData(dadosCliente, ani, dadosDerivacao) {
	var ret = {};

    if (dadosDerivacao == undefined) {
		dadosDerivacao = {};
		dadosDerivacao['rp'] = "";
	}
	
	ret['telefoneOrigem'] = ani;
	ret['app'] = dadosCliente['app'];
	ret['startts'] = dadosCliente['startts'];
	ret['endts'] = formataDataAtual("yyyyMMdd-hhmmss");
	ret['hourType'] = dadosCliente['hourType'];
	ret['vdn'] = dadosDerivacao['rp'];
	
	return ret;
}

