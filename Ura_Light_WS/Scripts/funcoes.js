
// CONSTANTES
var silencio = "silence1ms.vox";
var appName = 'Ura_Emergencia';
// FIM CONSTANTES

function criaDadosCliente_OLD(opm, veioOPM) {
	var ret = {};
	
	ret['profile'] = getIVRProfile();
	ret['ambiente'] = getAmbiente();
	ret['ani'] = AppState.ANI;	
	ret['dnis'] = AppState.DNIS.substr(-4);	
	ret['startts'] = formataDataAtual("yyyyMMdd-hhmmss");
	ret['hourType'] = 'Horario normal';
	ret['app'] = appName;
	ret['vdn'] = '';
	ret['parametros'] = getParametros(opm, veioOPM); // verificar se transferencia de parametros por OPM ou Attachs	
	return ret;
}

function criaDadosCliente() {
	var ret = {};

	ret['ani'] = AppState.ANI;	
	ret['dnis'] = AppState.DNIS.substr(-4);	
	ret['startts'] = formataDataAtual("yyyyMMdd-hhmmss");
	ret['hourType'] = 'Horario normal';
	ret['app'] = appName;
	ret['vdn'] = '';
	//return jsonLog(ret);
}

function criaObjeto(nome, valor) {
	var objeto = {};
	objeto[nome] = valor;
	return objeto;
}

function criaObjetoVazio(){
	var objeto = {};
	return objeto;
}

function adicionaDados(objeto, nome, valor) {
	objeto[nome] = valor;
	return objeto;
}

function adicionaObjetoDados(objeto, parametros, nome, valor) {
	objeto[parametros][nome] = valor;
	return objeto;
}

function getDiretorioFrases() {
	return "../Resources/Prompts/Frases/";
}

function getSilencio() {
	return "Silencio500.vox";
}

function getDDD(ani){
	return ani.substring(0,2);
}

function verificaJsonVazio(obj) {
	for (var prop in obj) {
		if (obj.hasOwnProperty(prop)) {
			return false;
		}
	}
	return true;
}

function jsonValido(obj) {
	if (obj == null || obj == undefined) return false;
	return true;
}

function completaComZeros(variavel, tamanho) {
	if (variavel == null) {
		variavel = "";
	}
	while (variavel.length < tamanho) {
		variavel = "0" + variavel;
	}
	return variavel;
}

function completaComZerosDireita(variavel, tamanho) {
	if (variavel == null) {
		variavel = "";
	}
	while (variavel.length < tamanho) {
		variavel = variavel + "0";
	}
	return variavel;
}


function jsonLog(json){
	
	return JSON.stringify(json);
	
}

function ehCPFCNPJouInstalacao(coleta) {
	var ret = '';
	if (coleta.length == 11){
		ret = 'cpf';
	}else if (coleta.length == 14){ // confirmar tamanho do numero de instalacao
		ret = 'cnpj';
	}else if (coleta.length == 10){ // confirmar tamanho do numero de instalacao
		ret = 'instalacao';
	}
	
	return ret;
}

function concatenaPerguntaNota(pergunta, nota, dados){
	var ret = '';
	if(dados['notaDigitada']){
		ret += dados['notaDigitada'] + ";" + pergunta + '=' + nota;
	}else{
		ret += pergunta + '=' + nota;
	}
	return ret; 
	
function validanota(notas_validas,nota){
	var ret = 'NOK';
	 if(notas_validas.indexOf(nota) != -1){
		ret = 'OK';
		    }
		return ret;
}

function armazenarResposta(dadosCliente){
	
	var uraQualidade = "URA_QUALID_RESP_" ;
	var numPergunta =  parseInt(dadosCliente['numPergunta']);
	var valor =  parseInt(dadosCliente['valor']);
	
	if (numPergunta == 4){
		var aux = dadosCliente['Pesquisa3'];
		return aux + ";"+ uraQualidade + numPergunta+":"+valor;
	}
	return uraQualidade + numPergunta+":"+valor; 
	
}

function getPesquisaDestino(dadosCliente){
	var ret = ''; 
	var skill = dadosCliente['TLB_SKILL'];
	
	switch(skill){	
		
		case '962 – Sindicancia GO':
		case '646 - BO_UTC_CONTAS_PAGAS':
		case '328 - BO_BABY_CARE_MOVEL':
		case '274 - BO_OI_TV_ANATEL':
		case '486 - BO_ANATEL_SCM_RI':
		case '979 - BACK_OFFICE_PROCON':
		case '534 - OUVIDORIA_GNA':
		case '535 - CS_RESGATE_GRAVACAO':
		case '698 - VIP_BO497 - AUXILIO_UC_BO':
		case '602 - BO_UTC_FLAG_SEG':
		case '994 - BO_UTC':
		case '417 - BO_SERVICOS_MOVEL':
		case '415 - BO_OFERTA_VELOX_BA':
		case '903 - SAC_FILA_TR':
		case '416 - BO_FILA_TR_VELOX':
		case '720 - BACK_OFFICE_ANATEL':
		case '238 - AUXILIO_CAMPAN_OI_TV':
		case '213 - SMP_RI_E_SMP_RII':
		case '204 - BO_ANATEL_RECEPTIVO':
		case '408 - BO_CONSUMIDOR_GOV':
		case '979 - BACKOFFICE_PROCON':
		case '542 - CONTUMAZES_BO_R2':
			ret = 'pilotoBackOffice';
			break;
		case 'SAC_CONV_PJ_ESP_R1':
		case 'SAC_CONV_PJ_ESP_R2':
		case 'SAC_CONV_PJ_ESP_R1_CC':
		case 'SAC_CONV_PJ_ESP_R2_CC':
		case 'RETENCAO_EMP_CTX_R1':
		case 'RETENCAO_EMP_CTX_R2':
		case 'EMP_BLEND_PJ_PS_R1':
		case 'EMP_BLEND_PJ_PS_R2':
		case 'EMP_BLEND_PJ_R1':
		case 'EMP_BLEND_PJ_R2':
		case 'SAC_CSE_DADOS':
		case 'CSE_DADOS_R1':
		case 'REP_MOV_PJ':
		case 'REP_MOV_PJ_R1':
		case 'RET_EMP_ESPECIALIZADA':
		case 'RET_EMP':
		case 'RETENCAO_EMPRESARIAL':
		case 'RET_EMP_NAO_PROP':
		case 'RETEMP_ESPECIALIZADA':
		case 'MIGR_FIXA_VELOX_EMP':
		case 'MIGR_FIXA_VELOX_APP':
		case 'CONTINGENCIA_EMP_UNI':
		case 'SAC_MADRUGADA_PJ_R1':
		case 'SAC_EMP_MADRU_GNA':
		case 'FALHA_AT_EMPRESARIAL':
		case 'ESCOLAS_CONECTADAS':
		case 'ESCOLA_CONECTADA_R2':
		case 'FALHA_AT_ESCOLAS':
		case 'OI_WIFI_PJ':
		case 'ESCOLA_OIWIFI_R1':
		case 'ESCOLA_OIWIFI_R2':
		case 'RET_EMP_R1RETENCAO_LD':
		case 'RET_VELOX_NID':
		case 'RET_FIXA_UNI':
		case 'RET_VELOX_UNI':
		case 'RET_FIXO_OITOTAL':
		case 'RET_VELOX_OITOTAL':
		case 'ILHA_MIG_ATO_UNI_R2':
		case 'RET_CTXCALLDESK':
		case 'RETENCAO_OCT_R1':
		case 'TH_RETENCAO_OCT_R1':
		case 'RETENCAO_OCT_R2':
		case 'RET_OCT_ATO':
		case 'ILHA_MIGR_NAO_PROP':
		case 'ILHA_MIGR_FIXA_VELOX':
		case 'REPAROS_R2_CORP':
		case 'POSICIONAMENTO_ESP':
		case 'POSICIONAMENTO_GER':
		case 'BERCARIO_VELOX_RECP':
		case 'ACP_VELOXBERCARIO_FIXO_RECEP':
		case 'OFF_EMPRESARIAL':
		case 'BO_APOIO_VEND_EMP':
		case 'OFF_GEST_MED_EMP':
		case 'OFF_GEST_PEQ_EMP':
		case 'TRATATIVA_TRAAS':
		case 'BO_GESTAO_MEDIAS':
		case 'CORP_ATACADO':
		case 'RET_EMP_UNI_01':
		case 'RET_EMP_UNI_02':
		case 'RET_EMP_UNI_MED_01':
		case 'RET_EMP_UNI_MED_02':
		case 'RET_EMP_ESP_01':
		case 'RET_EMP_ESP_02':
		case 'RET_EMP_ESP_OCT':
		case 'RET_EMP_ESP_02_R1':
		case 'RET_EMP_ESP_01_R1':
		case 'RET_EMP_OCT':
		case 'RET_EMP_ESP_R1':
		case 'RET_EMP_ESP_MED_01':
		case 'RET_EMP_ESP_MED_02':
		case 'RET_EMP_R1_01':
		case 'RET_EMP_R1_02':
		case 'PORT_EMP_FI_R1_WCA':
		case 'PORT_EMP_MOV_R1_WCA':
		case 'RET_EMP_R1_MED_01':
		case 'RET_EMP_R1_MED_02':
		case 'RET_EMP_MOVEL':
		case 'RETENCAO_EMP_MOVEL':
		case 'SAC_R1_EMP_CONECT':
		case 'SAC_R2_EMP_CONECT':
		case 'SAC_R1_EMP_NAO_CONEC':
		case 'SAC_R2_EMP_NAO_CONEC':
		case 'ST_OITOTAL_MOVEL_EMP':
		case 'BERCARIO_R1_FIXO':
		case 'BERCARIO_R1_VELOX':
		case 'SAC_CONV_PJ_MIG_FTT':
		case 'SAC_CONV_PJ_S2_VIAR1':
		case 'SAC_CONV_PJ_S2_VIAR2':
		case 'SAC_TV_EMP':
		case 'RETENCAO_EMP_CTX_R1':
		case 'RETENCAO_EMP_CTX_R2':
		case 'RET_EMP_ESPECIALIZADA':
		case 'RET_EMPRETENCAO_EMPRESARIAL':
		case 'RET_EMP_NAO_PROP':
		case 'RETEMP_ESPECIALIZADA':
		case 'MIGR_FIXA_VELOX_EMP':
		case 'MIGR_FIXA_VELOX_APP':
		case 'SAC_MADRUGADA_PJ_R1':
		case 'SAC_EMP_MADRU_GNA':
		case 'FALHA_AT_EMPRESARIAL':
		case 'ESCOLAS_CONECTADAS':
		case 'ESCOLA_CONECTADA_R2':
		case 'FALHA_AT_ESCOLAS':
		case 'OI_WIFI_PJ':
		case 'ESCOLA_OIWIFI_R1':
		case 'ESCOLA_OIWIFI_R2':
		case 'RET_EMP_R1':
		case 'RETENCAO_LD':
		case 'RET_VELOX_NID':
		case 'RET_FIXA_UNI':
		case 'RET_VELOX_UNI':
		case 'RET_FIXO_OITOTAL':
		case 'RET_VELOX_OITOTAL':
		case 'ILHA_MIG_ATO_UNI_R2':
		case 'RET_CTXCALLDESK':
		case 'RETENCAO_OCT_R1':
		case 'TH_RETENCAO_OCT_R1':
		case 'RETENCAO_OCT_R2':
		case 'RET_OCT_ATO':
		case 'ILHA_MIGR_NAO_PROP':
		case 'ILHA_MIGR_FIXA_VELOX':
		case 'REPAROS_R2_CORP':
		case 'POSICIONAMENTO_ESP':
		case 'POSICIONAMENTO_GER':
		case 'BERCARIO_VELOX_RECP':
		case 'ACP_VELOX':
		case 'BERCARIO_FIXO_RECEP':
		case 'OFF_EMPRESARIAL':
		case 'BO_APOIO_VEND_EMP':
		case 'OFF_GEST_MED_EMP':
		case 'OFF_GEST_PEQ_EMP':
		case 'BO_GESTAO_MEDIAS':
		case 'RET_EMP_UNI_01':
		case 'RET_EMP_UNI_02':
		case 'RET_EMP_UNI_MED_01':
		case 'RET_EMP_UNI_MED_02':
		case 'RET_EMP_ESP_01':
		case 'RET_EMP_ESP_02':
		case 'RET_EMP_ESP_OCT':
		case 'RET_EMP_ESP_02_R1':
		case 'RET_EMP_ESP_01_R1':
		case 'RET_EMP_OCT':
		case 'RET_EMP_ESP_R1':
		case 'RET_EMP_ESP_MED_01':
		case 'RET_EMP_ESP_MED_02':
		case 'RET_EMP_R1_01':
		case 'RET_EMP_R1_02':
		case 'PORT_EMP_FI_R1_WCA':
		case 'PORT_EMP_MOV_R1_WCA':
		case 'RET_EMP_R1_MED_01':
		case 'RET_EMP_R1_MED_02':
		case 'RET_EMP_MOVEL':
		case 'RETENCAO_EMP_MOVEL':
		case 'BERCARIO_R1_FIXO':
		case 'BERCARIO_R1_VELOX':
		case 'CSE_FTTH_EMP':
		case 'S2_VIA_WEB_PJ_FIX_R1':
		case 'S2_VIA_WEB_PJ_MVL_R1':
		case 'TV_EMP_R1':
		case 'TV_EMP_R2':
		case 'SAC_CONV_PJ_ESP_R1':
		case 'SAC_CONV_PJ_ESP_R2':
		case 'EMP_BLEND_PJ_PS_R1':
		case 'EMP_BLEND_PJ_PS_R2':
		case 'EMP_BLEND_PJ_R1':
		case 'EMP_BLEND_PJ_R1':
		case 'SAC_CSE_DADOS':
		case 'CSE_DADOS_R1':
		case 'REP_MOV_PJ':
		case 'REP_MOV_PJ_R1':
		case 'CONTINGENCIA_EMP_UNI':
		case 'SAC_MADRUGADA_PJ_R1':
		case 'SAC_EMP_MADRU_GNA':
		case 'TRATATIVA_TRAAS':
		case 'SAC_R1_EMP_CONECT':
		case 'SAC_R1_EMP_NAO_CONEC':
		case 'SAC_R2_EMP_CONECT':
		case 'SAC_R2_EMP_NAO_CONEC':
		case 'SAC_CONV_PJ_MIG_FTT':
		case 'ST_OITOTAL_MOVEL_EMP':
		case 'SAC_CONV_PJ_S2_VIAR1':
		case 'SAC_CONV_PJ_S2_VIAR2':
		case 'SAC_TV_EMP':
		case 'SAC_CONV_PJ_FIXO_R1':
		case 'SAC_CONV_PJ_MOVEL_R1':
		case 'SAC_CONV_PJ_FIXO_R2':
		case 'SAC_CONV_PJ_MOVEL_R2':
		case 'S2_VIA_WEB_PJ_FIX_R2':
		case 'S2_VIA_WEB_PJ_MVL_R2':
		case 'SAC_EMP_IRC_R2':
		case 'SAC_EMP_IRC_R1':
		case 'EMP_CONTUMAZES_R1':
		case 'EMP_CONTUMAZES_R2':
		case 'FONE_OI_EMP':
		case 'FONE_OI_FIBRA_EMP':
		case 'SAC_PJ_ROAMING':
		case 'SAC_PJ_ROAMING_MADRU': 
			ret = 'pesquisaEmpresarial'
			break;
		case 'MEDIAS_2VIA_PJ_R1':
		case 'MEDIAS_2VIA_PJ_R2':
		case 'MEDIAS_PJ_DADOS_R1':
		case 'MEDIAS_PJ_DADOS_R2':
		case 'MEDIAS_PJ_FIXO_R1':
		case 'MEDIAS_PJ_FIXO_R2':
		case 'MEDIAS_PJ_FTTH':
		case 'MEDIAS_PJ_MOVEL_R1':
		case 'MEDIAS_PJ_MOVEL_R2':
		case 'MEDIAS_PJ_TV_R1':
		case 'MEDIAS_PJ_TV_R2':
		case 'MEDIAS_BLEND_PJ_R1': //##SUB-GRUPO: Inicio
		case 'MEDIAS_BLEND_PJ_R2':
		case 'MEDIAS_BLEND_PS_R1':
		case 'MEDIAS_BLEND_PS_R2':
		case 'MEDIAS_CONTUMA_PJ_R1':
		case 'MEDIAS_CONTUMA_PJ_R2':
		case 'MEDIAS_CONV_ESP_R1':
		case 'MEDIAS_CONV_ESP_R2':
		case 'MEDIAS_MADRUGA_PJ_R1':
		case 'MEDIAS_MADRUGA_PJ_R2':
		case 'MEDIAS_PJ_ST_MOV_R1':
		case 'MEDIAS_PJ_ST_MOV_R2':
		case 'ATACADO_FIXO':
		case 'ATACADO_MOVEL':
		case 'ATACADO_CORP_FIXO_R1':
		case 'ATACADO_CORP_FIXO_R2':
		case 'ATACADO_CORP_MOVEL':
		case 'MEDIAS_BLEND_NPS_R1':
		case 'MEDIAS_BLEND_NPS_R2':  //##SUB-GRUPO: Fim
			ret = 'pesquisaEmpresarialMedias'; 
			break; 
		case 'CORP_CONTINGENCIA':
		case 'CORP_FIXO_R1':
		case 'CORP_FIXO_R2':
		case 'CORP_FTTH':
		case 'CORP_MOVEL':
		case 'CORP_NID_MOVEL':
		case 'CORP_OI_TV':
		case 'CORP_NID_FIXO_R1':
		case 'CORP_NID_FIXO_R2':
		case 'CORP_PERSO_06_MOVEL':
		case 'CORP_PERSO_07_MOVEL':
		case 'CORP_PERSO_08_MOVEL':
		case 'CORP_PERSO_09_MOVEL':
		case 'CORP_PERSO_10_MOVEL':
		case 'CORP_PERSO_11_MOVEL':
		case 'CORP_PERSO_01_MOVEL':
		case 'CORP_PERSO_02_MOVEL':
		case 'CORP_PERSO_03_MOVEL':
		case 'CORP_PERSO_04_MOVEL':
		case 'CORP_PERSO_05_MOVEL':
		case 'CORP_PERSO_04_FIX_R2':
		case 'CORP_PERSO_03_FIX_R1':
		case 'CORP_PERSO_05_FIX_R2':
		case 'CORP_PERSO_04_FIX_R1':
		case 'CORP_PERSO_06_FIX_R2':
		case 'CORP_PERSO_05_FIX_R1':
		case 'CORP_PERSO_07_FIX_R2':
		case 'CORP_PERSO_06_FIX_R1':
		case 'CORP_PERSO_08_FIX_R2':
		case 'CORP_PERSO_07_FIX_R1':
		case 'CORP_PERSO_09_FIX_R2':
		case 'CORP_PERSO_01_FIX_R1':
		case 'CORP_PERSO_01_FIX_R2':
		case 'CORP_PERSO_02_FIX_R1':
		case 'CORP_PERSO_02_FIX_R2':
		case 'CORP_PERSO_03_FIX_R2':
		case 'CORP_PERSO_08_FIX_R1':
		case 'CORP_PERSO_09_FIX_R1':
		case 'CORP_PERSO_10_FIX_R1':
		case 'CORP_PERSO_10_FIX_R2':
		case 'CORP_PERSO_11_FIX_R1':
		case 'CORP_PERSO_11_FIX_R2':
		case 'CORP_EVENTOS':           //##SUB-GRUPO
		case 'CORP_FIXO_TU':
		case 'CORP_MOVEL_TU':
		case 'CORP_OI_GESTOR':
		case 'CORP_ROAMING_INTER':
		case 'CORP_BLEND_PS_R1':
		case 'CORP_BLEND_PS_R2':
		case 'CORP_BLEND_NPS_R1':
		case 'CORP_BLEND_NPS_R2':
			ret = 'pesquisaCorpOnline'; 
			break; 
		case 'SUPORTE_EBILLING':
			ret = 'pesquisaEBilling'; 
			break;			  
		case 'SAC_FIBRA':
		case 'TV_CONVERG_R2_TRANSB':
		case 'FONE_OI_FIBRA':
		case 'CONTAS_FIBRA':
		case 'CTAS_TRIPLE_PLAY_DQX':
		case 'SAC_TRIPLE_PLAY_DQX':
		case 'VOIP_FIBRA':
		case 'OI_TOTAL_FIBRA_R1':
		case 'OI_TOTAL_FIBRA_R2':
		case 'SAC_FIBRA_CDF':
		case 'OFF_CONTESTA_FIBRA':
		case 'FIBRA_OUTLIERS':
		case 'BO_TRIPLE_PLAY':
			ret = 'pesquisaOiFibra'; 
			break;
		case 'SAC_OI_TV':
		case 'SAC_OI_TV_RECH':
		case 'SAC_TV_REPETIDA':
		case 'OFF_CONT_POS_PAGO':
			ret = 'pesquisaSAC'; 
			break;
		case 'PROCON_CLIENTE_RECEP':
			ret = 'pesquisaProcon'; 
			break;
		case 'ATEND_SOLUCAO_PROCON':
		case 'ATENDIMENTO_PREPOSTO':
		case 'ATENDIMENTO_RRO':
			ret = 'pesquisaProconRRO'; 
			break;
		case 'PROCON_CLIENTE_RECEP':
			ret = 'pesquisaProconReceptivo'; 
			break;	
		case 'ATEND_OI_WI_FI':
		case 'ST_OIWIFI_PORTUGUES':
		case 'ST_OIWIFI_INGLES':
		case 'ST_OIWIFI_ESPANHOL':
		case 'CALLING_TUP_PORTUG':
		case 'CALLING_TUP_ESPANHOL':
		case 'ST_TRIPLE_PLAY_ALONE':
		case 'ST_TRIPLE_PLAY_OIT':						
			ret = 'pesquisaST'; 
			break;
		case 'BO_ANATEL_FIXA_PJ':
		case 'ANATEL_F_V_TECNICO':
		case 'ANATEL_F_V_N_TECNICO':
		case 'TLV_WEB':
		case 'BO_ANATEL_TE':
		case 'SAC_FIXA_VELOX_OFF':
		case 'FALE_CONOSCO_ANATEL':
		case 'ST_MICROINFO_R1':
		case 'ST_MICROINFO_R2':
		case 'ST_VLX_DOMICILIO_R1':
		case 'ST_VLX_DOMICILIO_R2':
		case 'BO_ANATEL_PJ':
		case 'FALE_CONOSCO_ANA1_PJ':
		case 'FALE_CONOSCO_ANATEL1':
		case 'ANATEL_FV_TECNICO':
		case 'BO_OI_TV_ANATEL':	
		case 'BO_ANATEL_MOVEL':
		case 'OUV_ANT_BKO_FIBRA':
		case 'OUV_ANT_BKO_F_BL_TV':
		case 'OUV_ANT_BKO_PRE_CTRL':
		case 'OUV_ANT_BKO_POS_OIT':
		case 'OUV_ANT_BKO_EMP':
			ret = 'pesquisaAnatel'; 
			break;	
		case 'SNIPERS_ATV':
		case 'SNIPERS_RECP_OIT_R1':
		case 'SNIPERS_RECP_OIT_R2':
		case 'SNIPERS_RECP_POS':
		case 'SNIPERS_RECP_OI_TV':
		case 'SNIPER_DISC_MOVEL':
		case 'SNIPER_DISC_TV':
		case 'SNIPER_DISC_OIT_R1':
		case 'SNIPER_DISC_OIT_R2':
		case 'OFF_DISC_NAO_TECNICO':
		case 'OFF_DISC_TECNICO':
		case 'SNIPERS_F_V_R2':
		case 'SNIPERS_OI_TV':
		case 'SNIPERS_MOVEL_POS':
		case 'SNIPERS_MOVEL_PRE':
			ret = 'pesquisaSnipers'; 
			break;	
		case 'EMP_CONTUMAZES_R1':
		case 'EMP_CONTUMAZES_R2':
			ret = 'pesquisaContumazes'; 
			break;	
		case 'SAC_OI_INTERNET':
		case 'OI_PONTOS_RECEPT':
		case 'ADP_R1':
		case 'ACP_MOVEL':
		case 'FONE_OI_VELOX':
		case 'FONE_OI_FIXO':
		case 'APF':
		case 'ADP':
		case 'SAC_OI_INTERNET_EMP':
			ret = 'pesquisaOiInternet'; 
			break;
		case 'SAC_ESPECIAL_LOGIN':
		case 'BO_SAC_ESP_MEDIAS':
			ret = 'pesquisaCsEmpresarial'; 
			break;	
		case 'CONF_AGEND_CAMP1':
		case 'CONF_AGEND_CAMP2':
		case 'CONF_AGENDAM_CAMP9':
		case 'CONF_AGEND_CAMP5':
		case 'CONF_AGENDAM_CAMP11':
		case 'CONF_AGENDAM_CAMP12':
		case 'CONF_AGENDAM_CAMP13':
		case 'CONF_AGENDAM_CAMP14':
		case 'ATEND_CO_SLDA_CAMP1':
		case 'ATEND_CO_SLDA_CAMP2':
		case 'ATEND_CO_SLDA_CAMP4':
		case 'ATEND_CO_SLDA_CAMP5':
		case 'ATEND_CO_SLDA_CAMP6':
		case 'OFF_LINE_CAMP1':
		case 'OFF_LINE_CAMP3_BOSER':
		case 'OFF_LINE_CAMP4_BOSER':
		case 'OFF_LINE_CAMP5':
		case 'OFF_LINE_CAMP6_BOSER':
		case 'CONF_AGEND_CAMP4':
			ret = 'pesquisaConfirmacaoOffline'; 
			break;	
		case 'CONTEST_OITOT_FIBRA':
		case 'CONTEST_ALONE_FIBRA':
		case 'CONT_OIT_FIBRA_SC':
		case 'CONT_ALONE_FIBRA_SC':
		case 'SAC_FIBRA_PILOT_CONF':
		case 'ST_FIBRA_PILOT_CONF':
		case 'RET_FIBRA_PILOT_CONF':
		case 'CONSULT_FIBRA_ALONE':
		case 'COBRANCA_FIBRA_ALONE':
		case 'COBRANCA_FIBRA_OIT':
		case 'CONTEST_EMP_FIB_OIT':
		case 'CONTEST_EMP_FIB_ALONE':
		case 'CONTES_EMP_CS_FB_ALO':
		case 'CONTES_EMP_CS_FB_OIT':
			ret = 'pesquisaBOContestacao'; 
			break;			
		default:
			ret = 'pesquisaSatisfacao';
		}
			return ret;
	}
	
	

function getParametros(opm, veioOPM){
	var parametrosContingencia = {
			"AMBIENTE_EAI": "P",
			"AMBIENTE_FRAMEWORK": "P",
			"NIVEL_LOG": "5",
			"HORARIO_VERAO": "S",
			"VDN_CONTINGENCIA": "77550",
			"ATIVA_MONITORIA_PATROL": "S",
			"RP_TRANSFER": "64923",
			"DDD_PESQUISA_EMPRESARIAL": "11;12;13;14;15;16;17;18;19;21;22;24;27;28;31;32;33;34;35;37;38;41;42;43;44;45;46;47;48;49;51;53;54;55;61;62;63;64;65;66;67;68;69;71;73;74;75;77;79;81;82;83;84;85;86;87;88;89;91;92;93;94;95;96;97;98;99",
			"DDD_R2": "41;42;43;44;45;46;47;48;49;51;53;54;55;61;62;63;64;65;66;67;68;69",
			"P1_ATIVA": "S",
			"P1_VAL_DE": "0",
			"P1_VAL_AT": "1",
			"P2_ATIVA": "S",
			"P2_VAL_DE": "0",
			"P2_VAL_AT": "1",
			"P2_VALOR": "2",
			"P3_ATIVA": "S",
			"P3_VAL_DE": "0",
			"P3_VAL_AT": "1",
			"P3_VALOR": "2",
			"P4_ATIVA": "S",
			"P4_VAL_DE": "0",
			"P4_VAL_AT": "1",
			"P4_VALOR": "2",
			"VALIDAR_DDD_ATIVA": "N",
			"VALIDAR_DDD_REGIAO": "T",
			"FERIADO_ATIVA": "S",
			"HOR_ATENDIMENTO_ATIVA": "S",
			"DIA_ATENDIMENTO_INICIO": "2",
			"DIA_ATENDIMENTO_FINAL": "6",
			"HOR_ATENDIMENTO_INICIO": "00:00",
			"HOR_ATENDIMENTO_FINAL": "06:00",
			"01_01": "ANATEL",
			"01_TRANSFERE_ANATEL": "N",
			"01_FER_ANATEL_ATIVA": "N",
			"01_HOR_ANATEL_ATIVA": "S",
			"01_HOR_ANATEL_INICIO": "09:00",
			"01_HOR_ANATEL_FINAL": "21:00",
			"01_DDD_ANATEL_ATIVA": "S",
			"01_DDD_ANATEL_REGIAO": "T",
			"01_DIA_ANATEL_INICIO": "1",
			"01_DIA_ANATEL_FINAL": "5",
			"01_P1_ANATEL_ATIVA": "N",
			"01_P1_ANATEL_VAL_DE": "0",
			"01_P1_ANATEL_VAL_AT": "6",
			"01_P1_ANATEL_VALOR": "2",
			"01_P2_ANATEL_ATIVA": "S",
			"01_P2_ANATEL_VAL_DE": "0",
			"01_P2_ANATEL_VAL_AT": "6",
			"01_P2_ANATEL_VALOR": "2",
			"01_P3_ANATEL_ATIVA": "N",
			"01_P3_ANATEL_VAL_DE": "0",
			"01_P3_ANATEL_VAL_AT": "6",
			"01_P3_ANATEL_VALOR": "2",
			"01_P4_ANATEL_ATIVA": "N",
			"01_P4_ANATEL_VAL_DE": "0",
			"01_P4_ANATEL_VAL_AT": "6",
			"01_P4_ANATEL_VALOR": "2",
			"02_02": "CORP ONLINE",
			"02_TRANSFERE_CORP_ONLINE": "N",
			"02_FER_CORP_ONLINE_ATIVA": "S",
			"02_HOR_CORP_ONLINE_ATIVA": "S",
			"02_HOR_CORP_ONLINE_INICIO": "08:00",
			"02_HOR_CORP_ONLINE_FINAL": "19:30",
			"02_DIA_CORP_ONLINE_INICIO": "1",
			"02_DIA_CORP_ONLINE_FINAL": "5",
			"02_P1_CORP_ONLINE_ATIVA": "S",
			"02_P1_CORP_ONLINE_VAL_DE": "0",
			"02_P1_CORP_ONLINE_VAL_AT": "6",
			"02_P1_CORP_ONLINE_VALOR": "2",
			"02_P1_CORP_ONLINE_NV_QST": "8",
			"02_P2_CORP_ONLINE_ATIVA": "S",
			"02_P2_CORP_ONLINE_VAL_DE": "0",
			"02_P2_CORP_ONLINE_VAL_AT": "6",
			"02_P2_CORP_ONLINE_VALOR": "2",
			"02_P3_CORP_ONLINE_ATIVA": "S",
			"02_P3_CORP_ONLINE_VAL_DE": "0",
			"02_P3_CORP_ONLINE_VAL_AT": "6",
			"02_P3_CORP_ONLINE_VALOR": "2",
			"02_P3_CORP_ONLINE_NV_QST1": "7",
			"02_P3_CORP_ONLINE_NV_QST2": "8",
			"02_P4_CORP_ONLINE_ATIVA": "S",
			"02_P4_CORP_ONLINE_VAL_DE": "0",
			"02_P4_CORP_ONLINE_VAL_AT": "6",
			"02_P4_CORP_ONLINE_VALOR": "2",
			"03_03": "EMPRESARIAL",
			"03_TRANSFERE_EMPRESARIAL": "N",
			"03_FER_EMPRESARIAL_ATIVA": "S",
			"03_HOR_EMPRESARIAL_ATIVA": "S",
			"03_HOR_EMPRESARIAL_INICIO": "08:00",
			"03_HOR_EMPRESARIAL_FINAL": "19:30",
			"03_DIA_EMPRESARIAL_INICIO": "1",
			"03_DIA_EMPRESARIAL_FINAL": "5",
			"03_P1_EMPRESARIAL_ATIVA": "S",
			"03_P1_EMPRESARIAL_VAL_DE": "0",
			"03_P1_EMPRESARIAL_VAL_AT": "1",
			"03_P1_EMPRESARIAL_VALOR": "2",
			"03_P2_EMPRESARIAL_ATIVA": "S",
			"03_P2_EMPRESARIAL_VAL_DE": "0",
			"03_P2_EMPRESARIAL_VAL_AT": "6",
			"03_P2_EMPRESARIAL_VALOR": "2",
			"03_P3_EMPRESARIAL_ATIVA": "S",
			"03_P3_EMPRESARIAL_VAL_DE": "0",
			"03_P3_EMPRESARIAL_VAL_AT": "6",
			"03_P3_EMPRESARIAL_VALOR": "2",
			"03_P4_EMPRESARIAL_ATIVA": "S",
			"03_P4_EMPRESARIAL_VAL_DE": "0",
			"03_P4_EMPRESARIAL_VAL_AT": "6",
			"03_P4_EMPRESARIAL_VALOR": "2",
			"04_04": "EMPRESARIAL MEDIAS",
			"04_TRANSFERE_EMP_MEDIAS": "N",
			"04_FER_EMP_MEDIAS_ATIVA": "S",
			"04_HOR_EMP_MEDIAS_ATIVA": "S",
			"04_HOR_EMP_MEDIAS_INICIO": "08:00",
			"04_HOR_EMP_MEDIAS_FINAL": "19:30",
			"04_DIA_EMP_MEDIAS_INICIO": "1",
			"04_DIA_EMP_MEDIAS_FINAL": "5",
			"04_P1_EMP_MEDIAS_ATIVA": "S",
			"04_P1_EMP_MEDIAS_VAL_DE": "0",
			"04_P1_EMP_MEDIAS_VAL_AT": "6",
			"04_P1_EMP_MEDIAS_VALOR": "2",
			"04_P1_EMP_MEDIAS_NV_QST": "8",
			"04_P2_EMP_MEDIAS_ATIVA": "S",
			"04_P2_EMP_MEDIAS_VAL_DE": "0",
			"04_P2_EMP_MEDIAS_VAL_AT": "6",
			"04_P2_EMP_MEDIAS_VALOR": "2",
			"04_P3_EMP_MEDIAS_ATIVA": "S",
			"04_P3_EMP_MEDIAS_VAL_DE": "0",
			"04_P3_EMP_MEDIAS_VAL_AT": "6",
			"04_P3_EMP_MEDIAS_VALOR": "2",
			"04_P3_EMP_MEDIAS_NV_QST1": "7",
			"04_P3_EMP_MEDIAS_NV_QST2": "8",
			"04_P4_EMP_MEDIAS_ATIVA": "S",
			"04_P4_EMP_MEDIAS_VAL_DE": "0",
			"04_P4_EMP_MEDIAS_VAL_AT": "6",
			"04_P4_EMP_MEDIAS_VALOR": "2"
	  };
	var ret = {};
	if (! jsonValido(opm)) {
		ret = parametrosContingencia;
		return ret;
	} else {
		var temp = "";
		if (! veioOPM) {
			for (var teca in parametrosContingencia) {
				if (! opm.hasOwnProperty(teca)) {
					opm[teca] = "";
				}
			}
		}
		for (var bis in opm) {
			temp = bis.substr(5);
			ret['param' + temp.toUpperCase()] = opm[bis];
		}
		return ret;
	}
}

}