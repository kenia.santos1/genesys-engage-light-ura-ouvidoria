
var eventos = {
		'1' : 'Retencao_Anunciador_01',
		'2' : 'Retencao_Anunciador_02',
		'3' : 'Retencao_Anunciador_04',
		'4' : 'Retencao_Anunciador_04',
		'5' : 'Retencao_Anunciador_04',
		'6' : 'Retencao_Anunciador_04',
		'7' : 'Retencao_Anunciador_04',
		'15' : 'Retencao_Anunciador_15'
}

function getEventoNome(numero) {
	var nomeEvento = eventos[numero];
	if (nomeEvento == undefined || nomeEvento == '') {
		nomeEvento = 'Nao_Definido_' + numero;
	}
	return nomeEvento;
}

//precisam ajustar as funcoes de acordo com entendimento / layout pontos de navegacao
function adicionaNavegacaoSimples(dadosCliente, numero) {
	if (dadosCliente != undefined && numero != undefined && numero != '') {
		dadosCliente['navegacao_simples'] += completaComZeros(numero, 4) + ';';
	}
	return dadosCliente;
}

function adicionaEventoMenuRetencao(dadosCliente, numero) {
	if (dadosCliente != undefined && numero != undefined && numero != '') {
		dadosCliente['menu_retencao'] += completaComZeros(numero, 4) + ';';
	}
	return dadosCliente;
}

function adicionaEventoMenuAbandono(dadosCliente, numero) {
	if (dadosCliente != undefined && numero != undefined && numero != '') {
		dadosCliente['menu_abandono'] += completaComZeros(numero, 4) + ';';
	}
	return dadosCliente;
}

function adicionaEventoPromptRetencao(dadosCliente, numero) {
	if (dadosCliente != undefined && numero != undefined && numero != '') {
		dadosCliente['prompt_retencao'] += completaComZeros(numero, 4) + ';';
	}
	return dadosCliente;
}

function adicionaEventoPromptAbandono(dadosCliente, numero) {
	if (dadosCliente != undefined && numero != undefined && numero != '') {
		dadosCliente['prompt_abandono'] += completaComZeros(numero, 4) + ';';
	}
	return dadosCliente;
}

function adicionaEventoAnuncioRetencao(dadosCliente, numero) {
	if (dadosCliente != undefined && numero != undefined && numero != '') {
		dadosCliente['anuncio_retencao'] += completaComZeros(numero, 4) + ';';
	}
	return dadosCliente;
}

function adicionaEventoAnuncioAbandono(dadosCliente, numero) {
	if (dadosCliente != undefined && numero != undefined && numero != '') {
		dadosCliente['anuncio_abandono'] += completaComZeros(numero, 4) + ';';
	}
	return dadosCliente;
}

function adicionaEventoTransferencia(dadosCliente, numero) {
	if (dadosCliente != undefined && numero != undefined && numero != '') {
		dadosCliente['transferencia'] += completaComZeros(numero, 4) + ';';
	}
	return dadosCliente;
}

function adicionaEventoTransacao(dadosCliente, numero) {
	if (dadosCliente != undefined && numero != undefined && numero != '') {
		dadosCliente['transacao'] += completaComZeros(numero, 4) + ';';
	}
	return dadosCliente;
}

// verificar a necessidade de uso
function setEventoFinal(dadosCliente, numero) {
	if (dadosCliente != undefined && numero != undefined && numero != '') {
		dadosCliente['finalizacao'] = completaComZeros(numero, 4);
	}
	return dadosCliente;
}
