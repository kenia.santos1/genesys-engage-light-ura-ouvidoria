
function logIVRProfile() {
	var sip = session.connection.protocol.sip;
	var headers = sip.headers;
	if (jsonValido(headers) && headers["gsw-ivr-profile-name"] != undefined) {
		return "GSW_IVR_PROFILE=" + headers["gsw-ivr-profile-name"];
	} else {
		var requesturi = sip.requesturi;
		if (jsonValido(requesturi)) {
			return "RequestUri=" + requesturi["gvp-tenant-id"];
		}
	}
	return "";
}

function getIVRProfile() {
	var profile = "";
	var sip = session.connection.protocol.sip;
	var headers = sip.headers;
	if (jsonValido(headers) && headers["gsw-ivr-profile-name"] != undefined) {
		profile = headers["gsw-ivr-profile-name"];
	} else if (jsonValido(headers) && headers["s-gsw-ivr-profile-name"] != undefined) {
		profile = headers["s-gsw-ivr-profile-name"];
	} else {
		var requesturi = sip.requesturi;
		if (jsonValido(requesturi)) {
			profile = requesturi["gvp-tenant-id"];
		}
	}
	if (profile) {
		var i = profile.indexOf(".") + 1;
		if (i > 0) {
			return profile.substring(i);
		}
	}
	return profile;
}

function logAmbiente() {
	var sip = session.connection.protocol.sip;
	var headers = sip.headers;
	if (jsonValido(headers) && headers["RequestURI"] != undefined) {
		return "RequestURI=" + headers["RequestURI"];
	} else {
		var requesturi = sip.requesturi;
		if (jsonValido(requesturi)) {
			return "voicexml=" + requesturi["voicexml"];
		}
	}
	return "";
}

function getAmbiente() {
	var uri = "";
	var sip = session.connection.protocol.sip;
	var headers = sip.headers;
	if (jsonValido(headers) && headers["RequestURI"] != undefined) {
		uri = headers["RequestURI"];
	} else {
		var requesturi = sip.requesturi;
		if (jsonValido(requesturi)) {
			uri = requesturi["voicexml"];
		}
	}
	uri = uri.toLowerCase();

	if (uri.indexOf("172.22.55.65") != -1) {
		return "d";
	} 
	return "p";
}
