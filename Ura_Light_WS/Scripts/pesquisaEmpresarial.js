function getMenuPesquisaEmpresarialPergunta1(dadosCliente){
	var ret = {};
	ret['frases'] = [];
		
	//Qual o seu grau de satisfação com o atendimento prestado? Digite uma nota de 0 a 10 considerando 
	//o 0 para muito insatisfeito e 10 para muito satisfeito.
	ret['frases'].push({ 'frase' : '2421.vox' });
	ret['promptnoinput'] = '2428.vox';
	ret['promptnomatch'] = '2428.vox';
	ret['promptagradecimento'] = '2427.vox';
	ret['maxtentativas'] = '2';
	ret['opcsvalidas'] = '012345678910';
	
	return ret;
}



function getMenuPesquisaEmpresarialPergunta2(dadosCliente){
	var ret = {};
	ret['frases'] = [];
		
	//Seu problema foi resolvido? 
	ret['frases'].push({ 'frase' : '2422.vox' });
	//Digite 1 para Sim 
	ret['frases'].push({ 'frase' : '2423.vox' });
	//e 2 para Não. 
	ret['frases'].push({ 'frase' : '2424.vox' });
	ret['promptnoinput'] = '2428.vox';
	ret['promptnomatch'] = '2428.vox';
	ret['promptagradecimento'] = '2427.vox';
	ret['maxtentativas'] = '2';
	ret['opcsvalidas'] = '12';
	
	return ret;
}

function getMenuPesquisaEmpresarialPergunta3(dadosCliente){
	var ret = {};
	ret['frases'] = [];
		
	//Para finalizar, você recomendaria a Oi para um amigo ou parente? Digite um valor entre 0 e 10, 
	//onde 0 significa nada provável e 10 muito provável recomendar. 
	ret['frases'].push({ 'frase' : '2425.vox' });
	ret['promptnoinput'] = '2428.vox';
	ret['promptnomatch'] = '2428.vox';
	ret['promptagradecimento'] = '2427.vox';
	ret['maxtentativas'] = '2';
	ret['opcsvalidas'] = '012345678910';
	
	return ret;
}

function getMenuPesquisaEmpresarialPergunta4(dadosCliente){
	var ret = {};
	ret['frases'] = [];
		
	//Gostaria de permanecer  na linha para resolver sua solicitação com o nosso time especializado?
   	ret['frases'].push({ 'frase' : '1657.vox' });
	//Digite 1 para Sim 
	ret['frases'].push({ 'frase' : '1658.vox' });
	//e 2 para Não
	ret['frases'].push({ 'frase' : '1659.vox' });
	ret['promptnoinput'] = '2428.vox';
	ret['promptnomatch'] = '2428.vox';
	ret['promptagradecimento'] = '2427.vox';
	ret['maxtentativas'] = '2';
	ret['opcsvalidas'] = '12';
	
	return ret;
}

function getTransfereEmpresarial(dadosCliente) {
		
	var TRANSFERE_EMPRESARIAL = dadosCliente['Parametros']['03_TRANSFERE_EMPRESARIAL'];
			
	if (TRANSFERE_EMPRESARIAL != undefined) {
		if (TRANSFERE_EMPRESARIAL == 'S') {
			return true;
		}
	}
	return false;
}

function getDDD(ani){
	return ani.substring(0,2);
}

function getDDDPesquisaEmpresarial(dadosCliente, ani) {
	
	var DDD_PESQUISA_EMPRESARIAL = dadosCliente['Parametros']['DDD_PESQUISA_EMPRESARIAL'].split(';');
    var ddd = ani.substring(0,2);
	
	if (DDD_PESQUISA_EMPRESARIAL != undefined) {
		if (DDD_PESQUISA_EMPRESARIAL.indexOf(ddd) >= 0) {
			return true;
		}
	}
	return false;
}

function verificaEmpresarial_SkillSubGrupo(dadosCliente){
	var ret = ''; 
	var skill = dadosCliente['TLB_SKILL'];
	
	switch(skill){			
		case 'ACP_VELOX':
		case 'BERCARIO_FIXO_RECEP':
		case 'BERCARIO_R1_FIXO':
		case 'BERCARIO_R1_VELOX':
		case 'BERCARIO_VELOX_RECP':
		case 'BO_APOIO_VEND_EMP':
		case 'BO_GESTAO_MEDIAS':
		case 'CONTINGENCIA_EMP_UNI':
		case 'CORP_ATACADO':
		case 'CSE_DADOS_R1':
		case 'CSE_FTTH_EMP':
		case 'EMP_BLEND_PJ_PS_R1':
		case 'EMP_BLEND_PJ_PS_R2':
		case 'EMP_BLEND_PJ_R1':
		case 'EMP_BLEND_PJ_R2':
		case 'EMP_CONTUMAZES_R1':
		case 'EMP_CONTUMAZES_R2':
		case 'ESCOLA_CONECTADA_R2':
		case 'ESCOLA_OIWIFI_R1':
		case 'ESCOLA_OIWIFI_R2':
		case 'ESCOLAS_CONECTADAS':
		case 'FALHA_AT_EMPRESARIAL':
		case 'FALHA_AT_ESCOLAS':
		case 'FONE_OI_EMP':
		case 'FONE_OI_FIBRA_EMP':
		case 'ILHA_MIG_ATO_UNI_R2':
		case 'ILHA_MIGR_FIXA_VELOX':
		case 'ILHA_MIGR_NAO_PROP':
		case 'MIGR_FIXA_VELOX_APP':
		case 'MIGR_FIXA_VELOX_EMP':
		case 'OFF_EMPRESARIAL':
		case 'OFF_GEST_MED_EMP':
		case 'OFF_GEST_PEQ_EMP':
		case 'OI_WIFI_PJ':
		case 'PORT_EMP_FI_R1_WCA':
		case 'PORT_EMP_MOV_R1_WCA':
		case 'POSICIONAMENTO_ESP':
		case 'POSICIONAMENTO_GER':
		case 'REP_MOV_PJ':
		case 'REP_MOV_PJ_R1':
		case 'REPAROS_R2_CORP':
		case 'RET_CTXCALLDESK':
		case 'RET_EMP':
		case 'RET_EMP_ESP_01':
		case 'RET_EMP_ESP_01_R1':
		case 'RET_EMP_ESP_02':
		case 'RET_EMP_ESP_02_R1':
		case 'RET_EMP_ESP_MED_01':
		case 'RET_EMP_ESP_MED_02':
		case 'RET_EMP_ESP_OCT':
		case 'RET_EMP_ESP_R1':
		case 'RET_EMP_ESPECIALIZADA':
		case 'RET_EMP_MOVEL':
		case 'RET_EMP_NAO_PROP':
		case 'RET_EMP_OCT':
		case 'RET_EMP_R1':
		case 'RET_EMP_R1_01':
		case 'RET_EMP_R1_02':
		case 'RET_EMP_R1_MED_01':
		case 'RET_EMP_R1_MED_02':
		case 'RET_EMP_UNI_01':
		case 'RET_EMP_UNI_02':
		case 'RET_EMP_UNI_MED_01':
		case 'RET_EMP_UNI_MED_02':
		case 'RET_FIXA_UNI':
		case 'RET_FIXO_OITOTAL':
		case 'RET_OCT_ATO':
		case 'RET_VELOX_NID':
		case 'RET_VELOX_OITOTAL':
		case 'RET_VELOX_UNI':
		case 'RETEMP_ESPECIALIZADA':
		case 'RETENCAO_EMP_CTX_R1':
		case 'RETENCAO_EMP_CTX_R2':
		case 'RETENCAO_EMP_MOVEL':
		case 'RETENCAO_EMPRESARIAL':
		case 'RETENCAO_LD':
		case 'RETENCAO_OCT_R1':
		case 'RETENCAO_OCT_R2':
		case 'S2_VIA_WEB_PJ_FIX_R1':
		case 'S2_VIA_WEB_PJ_FIX_R2':
		case 'S2_VIA_WEB_PJ_MVL_R1':
		case 'S2_VIA_WEB_PJ_MVL_R2':
		case 'SAC_CONV_PJ_ESP_R1':
		case 'SAC_CONV_PJ_ESP_R1_CC':
		case 'SAC_CONV_PJ_ESP_R2':
		case 'SAC_CONV_PJ_ESP_R2_CC':
		case 'SAC_CONV_PJ_FIXO_R1':
		case 'SAC_CONV_PJ_FIXO_R2':
		case 'SAC_CONV_PJ_MIG_FTT':
		case 'SAC_CONV_PJ_MOVEL_R1':
		case 'SAC_CONV_PJ_MOVEL_R2':
		case 'SAC_CONV_PJ_S2_VIAR1':
		case 'SAC_CONV_PJ_S2_VIAR2':
		case 'SAC_CSE_DADOS':
		case 'SAC_EMP_IRC_R1':
		case 'SAC_EMP_IRC_R2':
		case 'SAC_EMP_MADRU_GNA':
		case 'SAC_MADRUGADA_PJ_R1':
		case 'SAC_PJ_ROAMING':
		case 'SAC_PJ_ROAMING_MADRU':
		case 'SAC_R1_EMP_CONECT':
		case 'SAC_R1_EMP_NAO_CONEC':
		case 'SAC_R2_EMP_CONECT':
		case 'SAC_R2_EMP_NAO_CONEC':
		case 'SAC_TV_EMP':
		case 'ST_OITOTAL_MOVEL_EMP':
		case 'TH_RETENCAO_OCT_R1':
		case 'TRATATIVA_TRAAS':
		case 'TV_EMP_R1':
		case 'TV_EMP_R2':
		ret = 'true'
			break;
			default:
		ret = 'false';
		}
			return ret;
	}

function isForaHorarioEmpresarial(dadosCliente) {
	
	var parametro = "";
	var atendimentoAtivo =  dadosCliente['Parametros']['03_HOR_EMPRESARIAL_ATIVA'].split(';');
	var diaAtendimentoInicio =  parseInt(dadosCliente['Parametros']['03_DIA_EMPRESARIAL_INICIO'].split(';'));
	var diaAtendimentoFim =  parseInt(dadosCliente['Parametros']['03_DIA_EMPRESARIAL_FINAL'].split(';'));
	var horaAtendimentoInicio = ""+ dadosCliente['Parametros']['03_HOR_EMPRESARIAL_INICIO'].split(';');
	var horaAtendimentoFim = ""+ dadosCliente['Parametros']['03_HOR_EMPRESARIAL_FINAL'].split(';');
		
	horaAtendimentoInicio = replaceAll(horaAtendimentoInicio, ":", "");
	horaAtendimentoFim = replaceAll(horaAtendimentoFim, ":", "");
	
	parametro = ((concatenaDiasSemana(diaAtendimentoInicio, diaAtendimentoFim))+";"+horaAtendimentoInicio+","+horaAtendimentoFim);
	
	if (parametro == '') { //se o parametro nao tiver valor assume que esta fora de horario
		return true;
	}
	
	var data = new Date(); 
	var diaSemana = data.getDay();
	var temp = parametro.split(";");
	var diaSemanaHabilitado = temp[0];
	var tempHora = temp[1].split(",");
	var horaIni = parseInt(tempHora[0], 10);
	var horaFim = parseInt(tempHora[1], 10);
	var horaAtual = parseInt(formataDataAtual("hhmm"), 10);
		
	if (diaSemanaHabilitado.indexOf(diaSemana) == -1 || (horaAtual < horaIni || horaAtual > horaFim )) {
		return true;
	}	
	return false;
}

	function replaceAll(str, rep, valor) {
		var idx = str.indexOf(rep);
		if (idx > 0) {
			return replaceAll(str.substring(0,idx) + valor + str.substring(idx + rep.length), rep, valor);
		}
		return str;
	}
	
	function concatenaDiasSemana(diaAtendimentoInicio, diaAtendimentoFim) {
		var DiasSemana = "";
		
		for (var i=diaAtendimentoInicio; i <= diaAtendimentoFim; i++) {
			DiasSemana = DiasSemana + i;
		}
		return DiasSemana;
	}
	
	function verificaNotaAtendimentoEmpresarialP1(dadosCliente){
			
		var notaIni = parseInt(dadosCliente['Parametros']['03_P1_EMPRESARIAL_VAL_DE']);
		var notaFim = parseInt(dadosCliente['Parametros']['03_P1_EMPRESARIAL_VAL_AT']);
		var nota = parseInt(dadosCliente['NotaAtendPesquisaEmpresarial_P1']);
				
		if ((nota >= notaIni && nota <= notaFim )) {
			return true;
		}	
		return false;
		
	}
	

	function verifica_DDD_R2(dadosCliente,ani) {
		
		var DDD_R2 = dadosCliente['Parametros']['DDD_R2'].split(';');
		var ddd = ani.substring(0,2);
		
		if (DDD_R2 != undefined) {
			if (DDD_R2.indexOf(ddd) >= 0) {
				return true;
			}
		}
		return false;
	}



	