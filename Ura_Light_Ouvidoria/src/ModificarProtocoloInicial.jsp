<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!

public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {

    JSONObject result = new JSONObject();
    
    String clientId = additionalParams.get("client");
	String clientSecret = additionalParams.get("clientSec");
	String protocolo = additionalParams.get("protocolo");
    String xpartner = additionalParams.get("partner");     
	String urlUra = additionalParams.get("url");
	String timeoutUra = additionalParams.get("timeout");
	ModificarProtocoloInicial ModificarProtocoloInicial = new ModificarProtocoloInicial();
    result = ModificarProtocoloInicial.executeModificarProtocoloInicial(clientId, clientSecret, protocolo, xpartner, urlUra, timeoutUra);
    
    result.put ("objRetornoModProtocolo",result);
    
    return result;
    
 
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="sap.*"%>
<%@include file="../include/backend.jspf" %>