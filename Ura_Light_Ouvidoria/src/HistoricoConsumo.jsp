<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
    
    JSONObject result = new JSONObject();
    
    String clientId = additionalParams.get("clientId"); 
    String clientSecret = additionalParams.get("clientSecret");
    String urlUra = additionalParams.get("urlUra");
 	String timeoutUra = additionalParams.get("timeoutUra");
    String vertrag = additionalParams.get("vertrag");
	String data_ini = additionalParams.get("data_ini");
	String data_fim = additionalParams.get("data_fim"); 
 
       
	HistoricoConsumo historicoConsumo = new HistoricoConsumo();
    result = historicoConsumo.executeHistoricoConsumo(clientId, clientSecret, urlUra, timeoutUra, vertrag, data_ini, data_fim);
    result.put ("objRetornoHistoricoConsumo",result);
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="sap.*"%>
<%@include file="../include/backend.jspf" %>