<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
    JSONObject result = new JSONObject();
    
    String parametrosUra = additionalParams.get("parametrosRequest"); 
    String ipUra = additionalParams.get("ipGdis");
    String timeoutUra = additionalParams.get("timeout");
    String portUra = additionalParams.get("porta");
    
    SocketGdis socket = new SocketGdis();
    String Retorno = socket.enviarInformacaoGdis(parametrosUra, ipUra, timeoutUra, portUra );
    
    result.put ("retornoGdis",Retorno);
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="Socket.*"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/backend.jspf" %>