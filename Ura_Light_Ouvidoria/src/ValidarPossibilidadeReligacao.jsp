<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {

    
    JSONObject result = new JSONObject();
    
    String clientId = additionalParams.get("client");
    String clientSecret  = additionalParams.get("clientSec");
    String partner = additionalParams.get("partner"); 
    String anlage = additionalParams.get("anlage"); 
    String vkont = additionalParams.get("vkont"); 
    String limite = additionalParams.get("limite"); 
    String dias_venc = additionalParams.get("dias_venc"); 
    String dias_apos_corte = additionalParams.get("total_deb"); 
    String total_deb = additionalParams.get("partner"); 
    String protocolo  = additionalParams.get("protocolo");
    String category = additionalParams.get("category"); 
    String urlUra = additionalParams.get("url");
    String timeoutUra = additionalParams.get("timeout");
    ValidarPossibilidadeReligacao ValidarPossibilidadeReligacao = new ValidarPossibilidadeReligacao();
    result = ValidarPossibilidadeReligacao.executeValidarPossibilidadeReligacao(clientId, clientSecret, partner, anlage, vkont, limite, dias_venc, dias_apos_corte, total_deb, protocolo, category, urlUra, timeoutUra);
    
    result.put ("objRetornoPossReliga",result);
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="sap.*"%>
<%@include file="../include/backend.jspf" %>