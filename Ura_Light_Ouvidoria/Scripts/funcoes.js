
// CONSTANTES
var silencio = "silence1ms.vox";
var appName = 'Ura_Light_Ouvidoria';
// FIM CONSTANTES

function criaDados(opm) {
	var ret = {};
	
	ret['profile'] = getIVRProfile();
	ret['ambiente'] = getAmbiente();
	ret['ani'] = AppState.ANI;	
	ret['dnis'] = AppState.DNIS.substr(-4);	
	ret['startts'] = formataDataAtual("yyyyMMdd-hhmmss");
	ret['hourType'] = 'Horario normal';
	ret['app'] = appName;
	ret['vdn'] = '';
	ret['tipo_transferencia'] = '';
	ret['ani_identificado'] = 'n';
	ret['attachs'] = getAttachs();
	
	//retirar apos a implantacao do transaction no GA
//	opm = false;
	
	if(opm){
		ret['parametros'] = opm;
	} else {
		ret['parametros'] = {};
		ret['parametros']['feriados'] = "0101;2402;2502;1004;2104;0105;0709;1210;0211;1511;2512";
		ret['parametros']['horario_atendimento'] = "0000-2359";
		ret['parametros']['habilita_msg_customizada_inicio'] = "n";
		ret['parametros']['desliga_msg_customizada_inicio'] = "n";
		ret['parametros']['msg_customizada_inicio'] = "silencio.wav";
		ret['parametros']['habilita_menu_naoidentificado_opcao9'] = "n";
		ret['parametros']['habilita_menu_protocolo_opcao9'] = "n";
		ret['parametros']['habilita_menu_emergenciafaltaluz_opcao9'] = "n";
		ret['parametros']['habilita_menu_verbalizaprotocolo_opcao9'] = "n";
		ret['parametros']['dbuid'] = "light_ivr";
		ret['parametros']['dbpwd'] = "light_ivr";
	}
	
	return ret;
}

function criaObjeto(nome, valor) {
	var objeto = {};
	objeto[nome] = valor;
	return objeto;
}

function criaObjetoVazio(){
	var objeto = {};
	return objeto;
}

function adicionaDados(objeto, nome, valor) {
	objeto[nome] = valor;
	return objeto;
}

function adicionaObjetoDados(objeto, parametros, nome, valor) {
	objeto[parametros][nome] = valor;
	return objeto;
}

function getDiretorioFrases() {
	return "../Resources/Prompts/Frases/";
}

function getSilencio() {
	return "Silencio500.vox";
}

function getDDD(ani){
	return ani.substring(0,2);
}

function verificaJsonVazio(obj) {
	for (var prop in obj) {
		if (obj.hasOwnProperty(prop)) {
			return false;
		}
	}
	return true;
}

function jsonValido(obj) {
	if (obj == null || obj == undefined) return false;
	return true;
}

function completaComZeros(variavel, tamanho) {
	if (variavel == null) {
		variavel = "";
	}
	while (variavel.length < tamanho) {
		variavel = "0" + variavel;
	}
	return variavel;
}

function completaComZerosDireita(variavel, tamanho) {
	if (variavel == null) {
		variavel = "";
	}
	while (variavel.length < tamanho) {
		variavel = variavel + "0";
	}
	return variavel;
}


function jsonLog(json){
	
	return JSON.stringify(json);
	
}

function ehCPFCNPJouInstalacao(coleta) {
	var ret = '';
	if (coleta.length == 11){
		ret = 'cpf';
	}else if (coleta.length == 14){ // confirmar tamanho do numero de instalacao
		ret = 'cnpj';
	}else if (coleta.length == 10){ // confirmar tamanho do numero de instalacao
		ret = 'instalacao';
	}
	
	return ret;
}

function concatenaPerguntaNota(pergunta, nota, dados){
	var ret = '';
	if(dados['notaDigitada']){
		ret += dados['notaDigitada'] + ";" + pergunta + '=' + nota;
	}else{
		ret += pergunta + '=' + nota;
	}
	return ret; 
}
	
function validanota(notas_validas,nota){
	var ret = 'NOK';
	 if(notas_validas.indexOf(nota) != -1){
		ret = 'OK';
		    }
		return ret;
}

function armazenarResposta(dadosCliente){
	
	var uraQualidade = "URA_QUALID_RESP_" ;
	var numPergunta =  parseInt(dadosCliente['numPergunta']);
	var valor =  parseInt(dadosCliente['valor']);
	
	if (numPergunta == 4){
		var aux = dadosCliente['Pesquisa3'];
		return aux + ";"+ uraQualidade + numPergunta+":"+valor;
	}
	return uraQualidade + numPergunta+":"+valor; 
	
}

function isCPF(cpf) {
    var acumulador = 0, numerosIguais = true;
    for (var peso = 10, indice = 0; indice < 9; indice++, peso--) {
        acumulador += peso * cpf[indice];
        if (cpf[indice] != cpf[indice + 1]) {
            numerosIguais = false;
        }
    }
    if (numerosIguais) {
        return false;
    }
    var digito1;
    var resto = acumulador % 11;
    if (resto < 2) {
        digito1 = 0;
    } else {
        digito1 = 11 - resto;
    }
    if (digito1 != cpf[9]) {
        return false;
    }
    acumulador = 0;
    for (peso = 11, indice = 0; indice < 10; indice++, peso--) {
        acumulador += peso * cpf[indice];
    }
    var digito2;
    var resto = acumulador % 11;
    if (resto < 2) {
        digito2 = 0;
    } else {
        digito2 = 11 - resto;
    }
    if (digito2 != cpf[10]) {
        return false;
    }
    return true;
}

function isCNPJ(cnpj) {
    var acumulador = 0, pesos = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
    for (var indice = 0; indice < 12; indice++) {
        acumulador += pesos[indice + 1] * cnpj[indice];
    }
    var digito1;
    var resto = acumulador % 11;
    if (resto < 2) {
        digito1 = 0;
    } else {
        digito1 = 11 - resto;
    }
    if (digito1 != cnpj[12]) {
        return false;
    }
    acumulador = 0;
    for (var indice = 0; indice < 13; indice++) {
        acumulador += pesos[indice] * cnpj[indice];
    }
    var digito2;
    var resto = acumulador % 11;
    if (resto < 2) {
        digito2 = 0;
    } else {
        digito2 = 11 - resto;
    }
    if (digito2 != cnpj[13]) {
        return false;
    }
    return true;
}

function validaGrandesClientes(dados){
	
	return (dados['servicoIdentificacao']['Y_TP_CLIENTE'] == 'Z002' || dados['servicoIdentificacao']['Y_TP_CLIENTE'] == 'Z003' || dados['servicoIdentificacao']['Y_TP_CLIENTE'] == 'Z004' || dados['servicoIdentificacao']['Y_TP_CLIENTE'] == 'Z005');
	
}

function qtdeNotas(dados){
	var notas = 0;
	
	if(dados['servicoIdentificacao']['YT_NOTAS']['item']){
		if(dados['servicoIdentificacao']['YT_NOTAS']['item'][0]){
			notas = dados['servicoIdentificacao']['YT_NOTAS']['item'].length + Number(dados['servicoOrdemRessarcimento']['X_TOTAL']);
		} else {
			notas = 1 + Number(dados['servicoOrdemRessarcimento']['X_TOTAL']);
		}
	} else {
		notas = Number(dados['servicoOrdemRessarcimento']['X_TOTAL']);
	}
	
	return notas;
}

function getTipoNotaOrdemServico(dados){
	var ret = {};
	
	ret['u_navegacao'] = "ns2341";
	//ret['evento'] = "NS2341";
	ret['sequencia'] = "protocolo";
	
	__Log('#### LOG getTipoNotaOrdemServico ######## ');
	__Log('#### LOG servicoOrdemRessarcimento X_TOTAL : ' + JSON.stringify(dados['servicoOrdemRessarcimento']['X_TOTAL']));
	__Log('#### LOG servicoIdentificacao YT_NOTAS item QMART: ' + JSON.stringify(dados['servicoIdentificacao']['YT_NOTAS']['item']['QMART']));
	__Log('#### LOG servicoIdentificacao YT_NOTAS item FECOD: ' + JSON.stringify(dados['servicoIdentificacao']['YT_NOTAS']['item']['FECOD']));
	__Log('#### LOG servicoIdentificacao YT_NOTAS item STATUS: ' + JSON.stringify(dados['servicoIdentificacao']['YT_NOTAS']['item']['STATUS']));
	__Log('#### LOG servicoIdentificacao YT_NOTAS item LTRMN: ' + JSON.stringify(dados['servicoIdentificacao']['YT_NOTAS']['item']['LTRMN']));
	
	
	if(Number(dados['servicoOrdemRessarcimento']['X_TOTAL'] > 0)){ //possui ordem de ressarcimento
		//ret['evento'] = "NS2336";
		ret['sequencia'] = "transfere";
		ret['u_navegacao'] = "ns2336";
	} else if(dados['servicoIdentificacao']['YT_NOTAS']['item']['QMART'] == 'AU'){ //nota AU
		//ret['evento'] = "NS2333";
		ret['sequencia'] = "transfere";
		ret['u_navegacao'] = "ns2333";
	} else if(dados['servicoIdentificacao']['YT_NOTAS']['item']['QMART'] == 'BE' && (dados['servicoIdentificacao']['YT_NOTAS']['item']['FECOD'] == 'RC07' || dados['servicoIdentificacao']['YT_NOTAS']['item']['FECOD'] == 'RC75')){ //NOTA BE | DANO RC07 e RC75 - falta de luz
		//ret['evento'] = "NS2334";
		ret['sequencia'] = "emergencia";
		ret['u_navegacao'] = "ns2333";
	} else if(dados['servicoIdentificacao']['YT_NOTAS']['item']['QMART'] == 'BE' && !(dados['servicoIdentificacao']['YT_NOTAS']['item']['FECOD'] == 'RC07' || dados['servicoIdentificacao']['YT_NOTAS']['item']['FECOD'] == 'RC75')){ //NOTA BE | DANO diferente de RC07 e RC75
		//ret['evento'] = "NS2335";
		ret['sequencia'] = "transfere";
		ret['u_navegacao'] = "ns2335";
	} else if(dados['servicoIdentificacao']['YT_NOTAS']['item']['QMART'] == 'A2' && dados['servicoIdentificacao']['YT_NOTAS']['item']['FECOD'] == '0020'){ //NOTA A2 | DANO 0020 - conta paga nao baixada
		//ret['evento'] = "NS2337";
		ret['sequencia'] = "transfere";
		ret['u_navegacao'] = "ns2337";
	} else if(dados['servicoIdentificacao']['YT_NOTAS']['item']['QMART'] == 'B1' && dados['servicoIdentificacao']['YT_NOTAS']['item']['FECOD'] == 'RC22'){ //NOTA B1 | DANO RC22 - religacao por corte indevido
		//ret['evento'] = "NS2338";
		ret['sequencia'] = "transfere";
		ret['u_navegacao'] = "ns2338";
	} else if(dados['servicoIdentificacao']['YT_NOTAS']['item']['QMART'] == 'BT' && dados['servicoIdentificacao']['YT_NOTAS']['item']['FECOD'] == 'RC90'){ //NOTA BT | DANO RC90 - VTF horario especifico
		//ret['evento'] = "NS2339";
		ret['sequencia'] = "transfere";
		ret['u_navegacao'] = "ns2339";
	} else if(dados['servicoIdentificacao']['YT_NOTAS']['item']['QMART'] == 'AX' && (dados['servicoIdentificacao']['YT_NOTAS']['item']['STATUS'] == 'PRO' || dados['servicoIdentificacao']['YT_NOTAS']['item']['STATUS'] == 'IMP' || dados['servicoIdentificacao']['YT_NOTAS']['item']['STATUS'] == 'CANC')){ //Status fechado
		//ret['evento'] = "NS2340";
		ret['sequencia'] = "transfere";
		ret['u_navegacao'] = "ns2340";
	} else if(dados['servicoIdentificacao']['YT_NOTAS']['item']['QMART'] == 'AX'){ //Status aberto
		var dataAtual = new Date();
		var dataServico = dados['servicoIdentificacao']['YT_NOTAS']['item']['LTRMN'].split("-");
		var dataAux = new Date(dataServico[0], dataServico[1]-1, dataServico[2]);
		
		if(dataAtual.getTime() > dataAux.getTime()){ //fora do prazo
			//ret['evento'] = "NS2342";
			ret['sequencia'] = "transfere";
			ret['u_navegacao'] = "ns2342";
		}
		
	} else {
		//ret['evento'] = "";
		ret['u_navegacao'] ="";
		ret['sequencia'] = "naotratado";
	}
	
	__Log('#### LOG ret: ' + JSON.stringify(ret));
	
	
	return ret;
}

function getLimiteExcedido(dadosGdis){
	
	var ret = {};
	
	var dataServico = dadosGdis['DadosRegistroClienteOuvidoria']['dataHora'];
	                //obj_Light['DadosRegistroClienteOuvidoria']['dataHora']
	var data = dataServico.split(" ")[0];
	var hora = dataServico.split(" ")[1];
	data = data.split("-");
	hora = hora.split(":");
	var dataLimite = new Date(data[2], data[1]-1, data[0], hora[0], hora[1]);
	var dtAtual = new Date();
	dtAtual.setHours(dtAtual.getHours() - 20);
	
	ret['dataAtual'] = dtAtual.getTime();
	ret['dataLimite'] = dataLimite.getTime();
	
	return ret;
//	return dataServico.getTime() < dtAtual.getTime();
//	return true;
}

function notaFaltaDeLuz(dados){
	
	if(dados['servicoIdentificacao']['YT_NOTAS']['item']){
		var notas = dados['servicoIdentificacao']['YT_NOTAS']['item'];
		if (dados['servicoIdentificacao']['YT_NOTAS']['item'][0]) { //se existir o vetor 0, significa que existe mais de uma nota
			for(var i = 0; i < notas.length; i++){
				if (notas[i]['QMART'] == 'BE' && (notas[i]['FECOD'] == 'RC07' || notas[i]['FECOD'] == 'RC75')){
					return true;
					break;
				}
			}			
		} else {
			if (notas['QMART'] == 'BE' && (notas['FECOD'] == 'RC07' || notas['FECOD'] == 'RC75')){
				return true;
			}
			
		}
		
	}
	
	return false;
	
}

function getAttachs(){
	var ret = {};
	
	ret['u_tipoidentificacaocliente'] = "";
	ret['u_cpf_cnpj'] = "";
	ret['u_num_instalacao'] = "";
	ret['u_falta_luz'] = false;
	ret['u_qtdade_nota_aberto'] = "";
	ret['u_desliga'] = "";
	ret['u_destino'] = "OUVIDORIA";
	ret['u_id_cliente'] = "";
	ret['u_nome_cliente'] = "";
	ret['u_pn'] = "";
	ret['u_destino'] = "";
	
	//pontos para os relatorios
	ret['u_rechamada'] = "";
	ret['u_retidaAutomatizada'] = "";
	ret['u_automatizada'] = "";
	ret['u_servicos'] = "";
	ret['u_autoservico'] = "";
	ret['u_AutomatedCall'] = "";
	ret['u_motivoChamada'] = "";
	
	
	return ret;
}
