function getFrasesXXX(dados) {
	var ret = {};
	var i = 0;
	var frases = [];
	
	frases[i++] = {"frase" : ".vox"};
	frases[i++] = {"data" : formataData( "","ddMMyyyy")};
	frases[i++] = {"frase" : ".vox"};
	frases[i++] = {"valor" : trataValor("")};
	frases[i++] = {"frase" : ".vox"};
	frases[i++] = {"numero" : "3"};
	
	return frases;
}

function getFrasesProtocolo(dados) {
	var i = 0;
	var frases = [];
	
	var protocolo = dados['protocolo'];
	
	if (protocolo) {
		frases[i++] = {"frase" : "MSG011.vox"};
		frases[i++] = {"protocolo" : protocolo};
	}
	
	return frases;
}

function getFrasesFHAPrompt() {
	var i = 0;
	var frases = [];
	
	frases[i++] = {"frase" : "Tchau_AfterHours.wav"};
	
	return frases;
}

function getFrasesBoasVindas() {
	var i = 0;
	var frases = [];
	
	var date = new Date();
	
	if(date.getHours() > 0 && date.getHours() < 12){
		frases[i++] = {"frase" : "BemVindo_Manha.wav"};
	} else if(date.getHours() >= 12 && date.getHours() < 18){
		frases[i++] = {"frase" : "BemVindo_Tarde.wav"};
	} else {
		frases[i++] = {"frase" : "BemVindo_Noite.wav"};
	}
	
	return frases;
}

function getFrasesMensagemCustomizadaInicio(dados){
	
	var i = 0;
	var frases = [];
	
	frases[i++] = {"frase" : dados['parametros']['msg_customizada_inicio']};
	
	return frases;
	
}

function getFrasesAvisoOcorrencia() {
	var i = 0;
	var frases = [];
	
	frases[i++] = {"frase" : "AvisoOcorrencia.wav"};
	
	return frases;
}

function getFrasesInputIdentificacao(){
	var ret = {};
	
	ret['frases'] = [{"frase" : "Identificacao_Ini.wav"}];
	ret['frasesNoMatch'] = [{"frase" : "Identificacao_Inv.wav"}];
	ret['frasesNoInput'] = [{"frase" : "Identificacao_Sil.wav"}];
	ret['frasesIncompleto'] = [{"frase" : "Identificacao_Inc.wav"}];
	
	return ret;
}

function getFrasesTeclado() {
	var i = 0;
	var frases = [];
	
	frases[i++] = {"frase" : "LatenciaID.wav"};
	
	return frases;
}

function getFrasesTransfereUraEmergencia() {
	var i = 0;
	var frases = [];
	
	frases[i++] = {"frase" : "Transfere_Emergencia.wav"};
	
	return frases;
}

function getFrasesTransfereUraComercial() {
	var i = 0;
	var frases = [];
	
	frases[i++] = {"frase" : "Transfere_Comercial.wav"};
	
	return frases;
}

function getFrasesMaxSil() {
	var i = 0;
	var frases = [];
	
//	frases[i++] = {"frase" : "Tchau_MaxSil.wav"};
	frases[i++] = {"frase" : "Tchau_MaxRej.wav"};
	
	return frases;
}

function getFrasesMaxRej() {
	var i = 0;
	var frases = [];
	
	frases[i++] = {"frase" : "Tchau_MaxRej.wav"};
	
	return frases;
}

function getFrasesTransferencia() {
	var i = 0;
	var frases = [];
	
	frases[i++] = {"frase" : "Transferir_Generico.wav"};
	
	return frases;
}

function getFrasesInputUmaInstalacao(dados){
	var ret = {};
	var frases = [];
	var frasesNoInput = [];
	var frasesNoMatch = [];
	
	frases[0] = {"frase" : "ConfirmaImovel_Ini.wav"};
	frases[1] = {"frase" : getFrasesBairroInstalacao(dados['servicoIdentificacao']['YT_INSTALS']['item']['COD_BAIRRO'])};
	frases[2] = {"frase" : "ConfirmaImovel_B.wav"};
	frases[3] = {"frase" : "ConfirmaImovel_C.wav"};
	frasesNoMatch[0] = {"frase" : "ConfirmaImovel_Rej.wav"};
	frasesNoMatch[1] = {"frase" : getFrasesBairroInstalacao(dados['servicoIdentificacao']['YT_INSTALS']['item']['COD_BAIRRO'])};
	frasesNoMatch[2] = {"frase" : "ConfirmaImovel_B.wav"};
	frasesNoMatch[3] = {"frase" : "ConfirmaImovel_C.wav"};
	frasesNoInput[0] = {"frase" : "ConfirmaImovel_Sil.wav"};
	frasesNoInput[1] = {"frase" : getFrasesBairroInstalacao(dados['servicoIdentificacao']['YT_INSTALS']['item']['COD_BAIRRO'])};
	frasesNoInput[2] = {"frase" : "ConfirmaImovel_B.wav"};
	frasesNoInput[3] = {"frase" : "ConfirmaImovel_C.wav"};
	
	ret['frases'] = frases;
	ret['frasesNoInput'] = frasesNoInput;
	ret['frasesNoMatch'] = frasesNoMatch;
	
	return ret;
	
}

function getFrasesInputDuasInstalacoes(dados){
	var ret = {};
	
	var frases = [];
	var frasesNoInput = [];
	var frasesNoMatch = [];
	
	frases[0] = {"frase" : "DesambImovel_Ini.wav"};
	frases[1] = {"frase" : getFrasesBairroInstalacao(dados['servicoIdentificacao']['YT_INSTALS']['item'][0]['COD_BAIRRO'])};
	frases[2] = {"frase" : "DesambImovel_B.wav"};
	frases[3] = {"frase" : "DesambImovel_C.wav"};
	frases[4] = {"frase" : getFrasesBairroInstalacao(dados['servicoIdentificacao']['YT_INSTALS']['item'][1]['COD_BAIRRO'])};
	frases[5] = {"frase" : "DesambImovel_D.wav"};
	frases[6] = {"frase" : "DesambImovel_E.wav"};
	frasesNoMatch[0] = {"frase" : "DesambImovel_Rej.wav"};
	frasesNoMatch[1] = {"frase" : getFrasesBairroInstalacao(dados['servicoIdentificacao']['YT_INSTALS']['item'][0]['COD_BAIRRO'])};
	frasesNoMatch[2] = {"frase" : "DesambImovel_B.wav"};
	frasesNoMatch[3] = {"frase" : "DesambImovel_C.wav"};
	frasesNoMatch[4] = {"frase" : getFrasesBairroInstalacao(dados['servicoIdentificacao']['YT_INSTALS']['item'][1]['COD_BAIRRO'])};
	frasesNoMatch[5] = {"frase" : "DesambImovel_D.wav"};
	frasesNoMatch[6] = {"frase" : "DesambImovel_E.wav"};
	frasesNoInput[0] = {"frase" : "DesambImovel_Sil.wav"};
	frasesNoInput[1] = {"frase" : getFrasesBairroInstalacao(dados['servicoIdentificacao']['YT_INSTALS']['item'][0]['COD_BAIRRO'])};
	frasesNoInput[2] = {"frase" : "DesambImovel_B.wav"};
	frasesNoInput[3] = {"frase" : "DesambImovel_C.wav"};
	frasesNoInput[4] = {"frase" : getFrasesBairroInstalacao(dados['servicoIdentificacao']['YT_INSTALS']['item'][1]['COD_BAIRRO'])};
	frasesNoInput[5] = {"frase" : "DesambImovel_D.wav"};
	frasesNoInput[6] = {"frase" : "DesambImovel_E.wav"};
	
	ret['frases'] = frases;
	ret['frasesNoInput'] = frasesNoInput;
	ret['frasesNoMatch'] = frasesNoMatch;
	
	return ret;
	
}

function getFrasesInputVariasInstalacoes(dados){
	var ret = {};
	
	if(dados['servicoIdentificacao']['Y_QTINST'] && dados['servicoIdentificacao']['Y_QTINST'] == 0){
		ret['frases'] = [{"frase" : "InstalacaoID_SemInst_Ini.wav"}];
	} else {
		ret['frases'] = [{"frase" : "InstalacaoID_Ini.wav"}];
	}
	
	ret['frasesNoMatch'] = [{"frase" : "InstalacaoID_Rej.wav"}];
	ret['frasesNoInput'] = [{"frase" : "InstalacaoID_Sil.wav"}];
	ret['frasesIncompleto'] = [{"frase" : "InstalacaoID_Inc.wav"}];
	
	return ret;
	
}

function getFrasesBairroInstalacao(codigoBairro){
	
	var codigo = Number(codigoBairro);
	
	if (codigo == "44008") {
	    return "bairro_1_NF.wav";
	} else if (codigo == "11797") {
	    return "bairro_10_NF.wav";
	} else if (codigo == "29530") {
	    return "bairro_100_NF.wav";
	} else if (codigo == "14656") {
	    return "bairro_1000_NF.wav";
	} else if (codigo == "1686") {
	    return "bairro_1001_NF.wav";
	} else if (codigo == "3654") {
	    return "bairro_1002_NF.wav";
	} else if (codigo == "1961") {
	    return "bairro_1003_NF.wav";
	} else if (codigo == "3581") {
	    return "bairro_1004_NF.wav";
	} else if (codigo == "43737") {
	    return "bairro_1005_NF.wav";
	} else if (codigo == "2453") {
	    return "bairro_1006_NF.wav";
	} else if (codigo == "5924") {
	    return "bairro_1007_NF.wav";
	} else if (codigo == "44091") {
	    return "bairro_1008_NF.wav";
	} else if (codigo == "2488") {
	    return "bairro_1009_NF.wav";
	} else if (codigo == "3221" || codigo == "4014" || codigo == "42293") {
	    return "bairro_101_NF.wav";
	} else if (codigo == "44326") {
	    return "bairro_1010_NF.wav";
	} else if (codigo == "1546") {
	    return "bairro_1011_NF.wav";
	} else if (codigo == "41777") {
	    return "bairro_1012_NF.wav";
	} else if (codigo == "14559") {
	    return "bairro_1013_NF.wav";
	} else if (codigo == "13901") {
	    return "bairro_1014_NF.wav";
	} else if (codigo == "698") {
	    return "bairro_1015_NF.wav";
	} else if (codigo == "13838") {
	    return "bairro_1016_NF.wav";
	} else if (codigo == "337") {
	    return "bairro_1017_NF.wav";
	} else if (codigo == "345") {
	    return "bairro_1018_NF.wav";
	} else if (codigo == "40703" || codigo == "43354") {
	    return "bairro_1019_NF.wav";
	} else if (codigo == "3841") {
	    return "bairro_102_NF.wav";
	} else if (codigo == "41947") {
	    return "bairro_1020_NF.wav";
	} else if (codigo == "40045") {
	    return "bairro_1021_NF.wav";
	} else if (codigo == "906") {
	    return "bairro_1022_NF.wav";
	} else if (codigo == "11665") {
	    return "bairro_1023_NF.wav";
	} else if (codigo == "2992") {
	    return "bairro_1024_NF.wav";
	} else if (codigo == "3697" || codigo == "7005") {
	    return "bairro_1025_NF.wav";
	} else if (codigo == "44032") {
	    return "bairro_1026_NF.wav";
	} else if (codigo == "868") {
	    return "bairro_1027_NF.wav";
	} else if (codigo == "9300") {
	    return "bairro_1028_NF.wav";
	} else if (codigo == "833") {
	    return "bairro_1029_NF.wav";
	} else if (codigo == "4600" || codigo == "4782" || codigo == "45373") {
	    return "bairro_103_NF.wav";
	} else if (codigo == "43001") {
	    return "bairro_1030_NF.wav";
	} else if (codigo == "41661" || codigo == "7552") {
	    return "bairro_1031_NF.wav";
	} else if (codigo == "11592") {
	    return "bairro_1032_NF.wav";
	} else if (codigo == "3778") {
	    return "bairro_1033_NF.wav";
	} else if (codigo == "40436") {
	    return "bairro_1034_NF.wav";
	} else if (codigo == "44016") {
	    return "bairro_1035_NF.wav";
	} else if (codigo == "2836") {
	    return "bairro_1036_NF.wav";
	} else if (codigo == "42757") {
	    return "bairro_1037_NF.wav";
	} else if (codigo == "42641") {
	    return "bairro_1038_NF.wav";
	} else if (codigo == "8737") {
	    return "bairro_1039_NF.wav";
	} else if (codigo == "42897") {
	    return "bairro_104_NF.wav";
	} else if (codigo == "2364") {
	    return "bairro_1040_NF.wav";
	} else if (codigo == "42102") {
	    return "bairro_1041_NF.wav";
	} else if (codigo == "44563") {
	    return "bairro_1042_NF.wav";
	} else if (codigo == "4022") {
	    return "bairro_1043_NF.wav";
	} else if (codigo == "41033") {
	    return "bairro_1044_NF.wav";
	} else if (codigo == "8001") {
	    return "bairro_1045_NF.wav";
	} else if (codigo == "41726" || codigo == "44113") {
	    return "bairro_1046_NF.wav";
	} else if (codigo == "2381") {
	    return "bairro_1047_NF.wav";
	} else if (codigo == "43176") {
	    return "bairro_1048_NF.wav";
	} else if (codigo == "27" || codigo == "3760") {
	    return "bairro_1049_NF.wav";
	} else if (codigo == "8508") {
	    return "bairro_105_NF.wav";
	} else if (codigo == "19062") {
	    return "bairro_1050_NF.wav";
	} else if (codigo == "41645") {
	    return "bairro_1051_NF.wav";
	} else if (codigo == "9245") {
	    return "bairro_1052_NF.wav";
	} else if (codigo == "41106") {
	    return "bairro_1053_NF.wav";
	} else if (codigo == "43842") {
	    return "bairro_1054_NF.wav";
	} else if (codigo == "8320") {
	    return "bairro_1055_NF.wav";
	} else if (codigo == "3743") {
	    return "bairro_1056_NF.wav";
	} else if (codigo == "1724") {
	    return "bairro_1057_NF.wav";
	} else if (codigo == "8265") {
	    return "bairro_1058_NF.wav";
	} else if (codigo == "43877") {
	    return "bairro_1059_NF.wav";
	} else if (codigo == "14303" || codigo == "43567") {
	    return "bairro_106_NF.wav";
	} else if (codigo == "43052") {
	    return "bairro_1060_NF.wav";
	} else if (codigo == "1180" || codigo == "41335" || codigo == "8818") {
	    return "bairro_1061_NF.wav";
	} else if (codigo == "43761" || codigo == "30635") {
	    return "bairro_1062_NF.wav";
	} else if (codigo == "44598") {
	    return "bairro_1063_NF.wav";
	} else if (codigo == "43362") {
	    return "bairro_1064_NF.wav";
	} else if (codigo == "8079" || codigo == "45926") {
	    return "bairro_1065_NF.wav";
	} else if (codigo == "2291") {
	    return "bairro_1066_NF.wav";
	} else if (codigo == "43044") {
	    return "bairro_1067_NF.wav";
	} else if (codigo == "41637" || codigo == "50201") {
	    return "bairro_1068_NF.wav";
	} else if (codigo == "2542") {
	    return "bairro_1069_NF.wav";
	} else if (codigo == "1244") {
	    return "bairro_107_NF.wav";
	} else if (codigo == "2631") {
	    return "bairro_1070_NF.wav";
	} else if (codigo == "11967") {
	    return "bairro_1071_NF.wav";
	} else if (codigo == "1821") {
	    return "bairro_1072_NF.wav";
	} else if (codigo == "43443" || codigo == "44431") {
	    return "bairro_1073_NF.wav";
	} else if (codigo == "2968") {
	    return "bairro_1074_NF.wav";
	} else if (codigo == "41823") {
	    return "bairro_1075_NF.wav";
	} else if (codigo == "44504") {
	    return "bairro_1076_NF.wav";
	} else if (codigo == "4812") {
	    return "bairro_1077_NF.wav";
	} else if (codigo == "5371") {
	    return "bairro_1078_NF.wav";
	} else if (codigo == "4731") {
	    return "bairro_1079_NF.wav";
	} else if (codigo == "43869") {
	    return "bairro_108_NF.wav";
	} else if (codigo == "10855") {
	    return "bairro_1080_NF.wav";
	} else if (codigo == "43273") {
	    return "bairro_1081_NF.wav";
	} else if (codigo == "2976") {
	    return "bairro_1082_NF.wav";
	} else if (codigo == "11452") {
	    return "bairro_1083_NF.wav";
	} else if (codigo == "3433") {
	    return "bairro_1084_NF.wav";
	} else if (codigo == "40126") {
	    return "bairro_1085_NF.wav";
	} else if (codigo == "43648") {
	    return "bairro_1086_NF.wav";
	} else if (codigo == "40509") {
	    return "bairro_1087_NF.wav";
	} else if (codigo == "931") {
	    return "bairro_1088_NF.wav";
	} else if (codigo == "43974") {
	    return "bairro_1089_NF.wav";
	} else if (codigo == "604") {
	    return "bairro_109_NF.wav";
	} else if (codigo == "7021") {
	    return "bairro_1090_NF.wav";
	} else if (codigo == "40789") {
	    return "bairro_1091_NF.wav";
	} else if (codigo == "8290") {
	    return "bairro_1093_NF.wav";
	} else if (codigo == "42099") {
	    return "bairro_1094_NF.wav";
	} else if (codigo == "1881") {
	    return "bairro_1095_NF.wav";
	} else if (codigo == "8095") {
	    return "bairro_1096_NF.wav";
	} else if (codigo == "761") {
	    return "bairro_1097_NF.wav";
	} else if (codigo == "41149") {
	    return "bairro_1098_NF.wav";
	} else if (codigo == "40959") {
	    return "bairro_1099_NF.wav";
	} else if (codigo == "7994") {
	    return "bairro_11_NF.wav";
	} else if (codigo == "41076") {
	    return "bairro_110_NF.wav";
	} else if (codigo == "41122") {
	    return "bairro_1100_NF.wav";
	} else if (codigo == "6165") {
	    return "bairro_1101_NF.wav";
	} else if (codigo == "11002") {
	    return "bairro_1102_NF.wav";
	} else if (codigo == "14125") {
	    return "bairro_1103_NF.wav";
	} else if (codigo == "43672") {
	    return "bairro_1104_NF.wav";
	} else if (codigo == "8761") {
	    return "bairro_1105_NF.wav";
	} else if (codigo == "1830") {
	    return "bairro_1106_NF.wav";
	} else if (codigo == "43966") {
	    return "bairro_1107_NF.wav";
	} else if (codigo == "2771") {
	    return "bairro_1108_NF.wav";
	} else if (codigo == "1490" || codigo == "50531") {
	    return "bairro_1109_NF.wav";
	} else if (codigo == "4901") {
	    return "bairro_111_NF.wav";
	} else if (codigo == "42234" || codigo == "42323") {
	    return "bairro_1110_NF.wav";
	} else if (codigo == "11975") {
	    return "bairro_1112_NF.wav";
	} else if (codigo == "5029") {
	    return "bairro_1113_NF.wav";
	} else if (codigo == "3638") {
	    return "bairro_1114_NF.wav";
	} else if (codigo == "6815") {
	    return "bairro_1115_NF.wav";
	} else if (codigo == "42528") {
	    return "bairro_1116_NF.wav";
	} else if (codigo == "7455") {
	    return "bairro_1117_NF.wav";
	} else if (codigo == "42536" || codigo == "3786") {
	    return "bairro_1118_NF.wav";
	} else if (codigo == "4413") {
	    return "bairro_1119_NF.wav";
	} else if (codigo == "124") {
	    return "bairro_112_NF.wav";
	} else if (codigo == "42129") {
	    return "bairro_1120_NF.wav";
	} else if (codigo == "2178") {
	    return "bairro_1121_NF.wav";
	} else if (codigo == "1538") {
	    return "bairro_1122_NF.wav";
	} else if (codigo == "1309") {
	    return "bairro_1124_NF.wav";
	} else if (codigo == "8371") {
	    return "bairro_1126_NF.wav";
	} else if (codigo == "388") {
	    return "bairro_1127_NF.wav";
	} else if (codigo == "14265") {
	    return "bairro_1128_NF.wav";
	} else if (codigo == "7625") {
	    return "bairro_1129_NF.wav";
	} else if (codigo == "6009") {
	    return "bairro_113_NF.wav";
	} else if (codigo == "7676") {
	    return "bairro_1130_NF.wav";
	} else if (codigo == "9661") {
	    return "bairro_1131_NF.wav";
	} else if (codigo == "10707") {
	    return "bairro_1132_NF.wav";
	} else if (codigo == "1996") {
	    return "bairro_1133_NF.wav";
	} else if (codigo == "4430") {
	    return "bairro_1134_NF.wav";
	} else if (codigo == "9482") {
	    return "bairro_1135_NF.wav";
	} else if (codigo == "44911") {
	    return "bairro_1136_NF.wav";
	} else if (codigo == "44768") {
	    return "bairro_1137_NF.wav";
	} else if (codigo == "45535") {
	    return "bairro_1138_NF.wav";
	} else if (codigo == "44776") {
	    return "bairro_1139_NF.wav";
	} else if (codigo == "1953") {
	    return "bairro_114_NF.wav";
	} else if (codigo == "45128") {
	    return "bairro_1140_NF.wav";
	} else if (codigo == "44733") {
	    return "bairro_1141_NF.wav";
	} else if (codigo == "45004") {
	    return "bairro_1142_NF.wav";
	} else if (codigo == "45721") {
	    return "bairro_1143_NF.wav";
	} else if (codigo == "45764") {
	    return "bairro_1144_NF.wav";
	} else if (codigo == "45144") {
	    return "bairro_1145_NF.wav";
	} else if (codigo == "45527") {
	    return "bairro_1146_NF.wav";
	} else if (codigo == "44652") {
	    return "bairro_1147_NF.wav";
	} else if (codigo == "45519") {
	    return "bairro_1148_NF.wav";
	} else if (codigo == "45713") {
	    return "bairro_1149_NF.wav";
	} else if (codigo == "6718") {
	    return "bairro_115_NF.wav";
	} else if (codigo == "45209") {
	    return "bairro_1150_NF.wav";
	} else if (codigo == "45667") {
	    return "bairro_1151_NF.wav";
	} else if (codigo == "44857") {
	    return "bairro_1152_NF.wav";
	} else if (codigo == "45195") {
	    return "bairro_1153_NF.wav";
	} else if (codigo == "45748" || codigo == "45837") {
	    return "bairro_1154_NF.wav";
	} else if (codigo == "45039") {
	    return "bairro_1155_NF.wav";
	} else if (codigo == "45136") {
	    return "bairro_1156_NF.wav";
	} else if (codigo == "45756") {
	    return "bairro_1157_NF.wav";
	} else if (codigo == "45055") {
	    return "bairro_1158_NF.wav";
	} else if (codigo == "45586") {
	    return "bairro_1159_NF.wav";
	} else if (codigo == "11991") {
	    return "bairro_116_NF.wav";
	} else if (codigo == "42676") {
	    return "bairro_1160_NF.wav";
	} else if (codigo == "44962") {
	    return "bairro_1161_NF.wav";
	} else if (codigo == "45829") {
	    return "bairro_1162_NF.wav";
	} else if (codigo == "44806") {
	    return "bairro_1163_NF.wav";
	} else if (codigo == "4294") {
	    return "bairro_1164_NF.wav";
	} else if (codigo == "44903") {
	    return "bairro_1165_NF.wav";
	} else if (codigo == "44822") {
	    return "bairro_1166_NF.wav";
	} else if (codigo == "45454") {
	    return "bairro_1167_NF.wav";
	} else if (codigo == "45357") {
	    return "bairro_1168_NF.wav";
	} else if (codigo == "45799") {
	    return "bairro_1169_NF.wav";
	} else if (codigo == "6700") {
	    return "bairro_117_NF.wav";
	} else if (codigo == "44644") {
	    return "bairro_1170_NF.wav";
	} else if (codigo == "45624") {
	    return "bairro_1171_NF.wav";
	} else if (codigo == "45578") {
	    return "bairro_1172_NF.wav";
	} else if (codigo == "45802") {
	    return "bairro_1173_NF.wav";
	} else if (codigo == "45659") {
	    return "bairro_1174_NF.wav";
	} else if (codigo == "45071") {
	    return "bairro_1175_NF.wav";
	} else if (codigo == "44849") {
	    return "bairro_1176_NF.wav";
	} else if (codigo == "44628") {
	    return "bairro_1177_NF.wav";
	} else if (codigo == "45608") {
	    return "bairro_1178_NF.wav";
	} else if (codigo == "45047") {
	    return "bairro_1179_NF.wav";
	} else if (codigo == "3280") {
	    return "bairro_118_NF.wav";
	} else if (codigo == "44679") {
	    return "bairro_1180_NF.wav";
	} else if (codigo == "45497") {
	    return "bairro_1181_NF.wav";
	} else if (codigo == "44717") {
	    return "bairro_1182_NF.wav";
	} else if (codigo == "44946") {
	    return "bairro_1183_NF.wav";
	} else if (codigo == "44881") {
	    return "bairro_1184_NF.wav";
	} else if (codigo == "45551") {
	    return "bairro_1185_NF.wav";
	} else if (codigo == "45446") {
	    return "bairro_1186_NF.wav";
	} else if (codigo == "45462") {
	    return "bairro_1187_NF.wav";
	} else if (codigo == "44873") {
	    return "bairro_1188_NF.wav";
	} else if (codigo == "44709") {
	    return "bairro_1189_NF.wav";
	} else if (codigo == "817") {
	    return "bairro_119_NF.wav";
	} else if (codigo == "45489") {
	    return "bairro_1190_NF.wav";
	} else if (codigo == "45403") {
	    return "bairro_1191_NF.wav";
	} else if (codigo == "44792") {
	    return "bairro_1192_NF.wav";
	} else if (codigo == "45683") {
	    return "bairro_1193_NF.wav";
	} else if (codigo == "44695") {
	    return "bairro_1194_NF.wav";
	} else if (codigo == "45675") {
	    return "bairro_1195_NF.wav";
	} else if (codigo == "43591") {
	    return "bairro_1196_NF.wav";
	} else if (codigo == "41955") {
	    return "bairro_1197_NF.wav";
	} else if (codigo == "44687") {
	    return "bairro_1198_NF.wav";
	} else if (codigo == "45152") {
	    return "bairro_1199_NF.wav";
	} else if (codigo == "12475") {
	    return "bairro_12_NF.wav";
	} else if (codigo == "1465") {
	    return "bairro_120_NF.wav";
	} else if (codigo == "45543") {
	    return "bairro_1200_NF.wav";
	} else if (codigo == "45012") {
	    return "bairro_1201_NF.wav";
	} else if (codigo == "44938") {
	    return "bairro_1202_NF.wav";
	} else if (codigo == "44865") {
	    return "bairro_1203_NF.wav";
	} else if (codigo == "44989") {
	    return "bairro_1204_NF.wav";
	} else if (codigo == "45098") {
	    return "bairro_1205_NF.wav";
	} else if (codigo == "45616") {
	    return "bairro_1206_NF.wav";
	} else if (codigo == "44997") {
	    return "bairro_1207_NF.wav";
	} else if (codigo == "45594") {
	    return "bairro_1208_NF.wav";
	} else if (codigo == "45365") {
	    return "bairro_1209_NF.wav";
	} else if (codigo == "10651") {
	    return "bairro_121_NF.wav";
	} else if (codigo == "45233") {
	    return "bairro_1210_NF.wav";
	} else if (codigo == "45985") {
	    return "bairro_1211_NF.wav";
	} else if (codigo == "46051") {
	    return "bairro_1212_NF.wav";
	} else if (codigo == "43818") {
	    return "bairro_1213_NF.wav";
	} else if (codigo == "44067") {
	    return "bairro_1217_NF.wav";
	} else if (codigo == "45993") {
	    return "bairro_1218_NF.wav";
	} else if (codigo == "50032") {
	    return "bairro_1219_NF.wav";
	} else if (codigo == "14346") {
	    return "bairro_122_NF.wav";
	} else if (codigo == "50131") {
	    return "bairro_1220_NF.wav";
	} else if (codigo == "45918") {
	    return "bairro_1221_NF.wav";
	} else if (codigo == "46256") {
	    return "bairro_1222_NF.wav";
	} else if (codigo == "50241") {
	    return "bairro_1223_NF.wav";
	} else if (codigo == "3557") {
	    return "bairro_1224_NF.wav";
	} else if (codigo == "46167" || codigo == "45187") {
	    return "bairro_1227_NF.wav";
	} else if (codigo == "4235") {
	    return "bairro_1228_NF.wav";
	} else if (codigo == "4332") {
	    return "bairro_1229_NF.wav";
	} else if (codigo == "14737" || codigo == "40819") {
	    return "bairro_123_NF.wav";
	} else if (codigo == "6491") {
	    return "bairro_1233_NF.wav";
	} else if (codigo == "7218") {
	    return "bairro_1235_NF.wav";
	} else if (codigo == "43605") {
	    return "bairro_1238_NF.wav";
	} else if (codigo == "11801") {
	    return "bairro_124_NF.wav";
	} else if (codigo == "50012") {
	    return "bairro_1241_NF.wav";
	} else if (codigo == "50411") {
	    return "bairro_1242_NF.wav";
	} else if (codigo == "50431") {
	    return "bairro_1243_NF.wav";
	} else if (codigo == "50601") {
	    return "bairro_1244_NF.wav";
	} else if (codigo == "6769") {
	    return "bairro_1245_NF.wav";
	} else if (codigo == "11703") {
	    return "bairro_1247_NF.wav";
	} else if (codigo == "13862 " || codigo == "5576") {
	    return "bairro_1248_NF.wav";
	} else if (codigo == "42285" || codigo == "5916") {
	    return "bairro_1249_NF.wav";
	} else if (codigo == "7048" || codigo == "9113" || codigo == "42307") {
	    return "bairro_125_NF.wav";
	} else if (codigo == "44725") {
	    return "bairro_1250_NF.wav";
	} else if (codigo == "6033") {
	    return "bairro_1255_NF.wav";
	} else if (codigo == "6238") {
	    return "bairro_1256_NF.wav";
	} else if (codigo == "14184") {
	    return "bairro_1259_NF.wav";
	} else if (codigo == "2844") {
	    return "bairro_126_NF.wav";
	} else if (codigo == "45969") {
	    return "bairro_1263_NF.wav";
	} else if (codigo == "46426" || codigo == "46396") {
	    return "bairro_1264_NF.wav";
	} else if (codigo == "46507") {
	    return "bairro_1265_NF.wav";
	} else if (codigo == "50031") {
	    return "bairro_1266_NF.wav";
	} else if (codigo == "50081") {
	    return "bairro_1267_NF.wav";
	} else if (codigo == "9148") {
	    return "bairro_127_NF.wav";
	} else if (codigo == "50421") {
	    return "bairro_1270_NF.wav";
	} else if (codigo == "50441") {
	    return "bairro_1271_NF.wav";
	} else if (codigo == "45853") {
	    return "bairro_1272_NF.wav";
	} else if (codigo == "3123" || codigo == "13846") {
	    return "bairro_1273_NF.wav";
	} else if (codigo == "13056") {
	    return "bairro_1277_NF.wav";
	} else if (codigo == "40193") {
	    return "bairro_1279_NF.wav";
	} else if (codigo == "1155") {
	    return "bairro_128_NF.wav";
	} else if (codigo == "40908" || codigo == "4723") {
	    return "bairro_1280_NF.wav";
	} else if (codigo == "41475") {
	    return "bairro_1281_NF.wav";
	} else if (codigo == "43257" || codigo == "11100" || codigo == "30821") {
	    return "bairro_1285_NF.wav";
	} else if (codigo == "45438") {
	    return "bairro_1287_NF.wav";
	} else if (codigo == "45934") {
	    return "bairro_1288_NF.wav";
	} else if (codigo == "46493") {
	    return "bairro_1289_NF.wav";
	} else if (codigo == "8052" || codigo == "12033") {
	    return "bairro_129_NF.wav";
	} else if (codigo == "50061") {
	    return "bairro_1290_NF.wav";
	} else if (codigo == "50141") {
	    return "bairro_1291_NF.wav";
	} else if (codigo == "50151") {
	    return "bairro_1292_NF.wav";
	} else if (codigo == "50451") {
	    return "bairro_1293_NF.wav";
	} else if (codigo == "50571") {
	    return "bairro_1294_NF.wav";
	} else if (codigo == "41238") {
	    return "bairro_1299_NF.wav";
	} else if (codigo == "9351") {
	    return "bairro_13_NF.wav";
	} else if (codigo == "12157") {
	    return "bairro_130_NF.wav";
	} else if (codigo == "485") {
	    return "bairro_131_NF.wav";
	} else if (codigo == "46175") {
	    return "bairro_1310_NF.wav";
	} else if (codigo == "46248" || codigo == "14231") {
	    return "bairro_1311_NF.wav";
	} else if (codigo == "46388") {
	    return "bairro_1312_NF.wav";
	} else if (codigo == "50072") {
	    return "bairro_1313_NF.wav";
	} else if (codigo == "50101") {
	    return "bairro_1314_NF.wav";
	} else if (codigo == "50191") {
	    return "bairro_1315_NF.wav";
	} else if (codigo == "50211") {
	    return "bairro_1316_NF.wav";
	} else if (codigo == "50381") {
	    return "bairro_1317_NF.wav";
	} else if (codigo == "14427") {
	    return "bairro_1318_NF.wav";
	} else if (codigo == "50001") {
	    return "bairro_1319_NF.wav";
	} else if (codigo == "13048") {
	    return "bairro_132_NF.wav";
	} else if (codigo == "14435 " || codigo == "5363 " || codigo == "8842 " || codigo == "50232") {
	    return "bairro_1324_NF.wav";
	} else if (codigo == "40274") {
	    return "bairro_1325_NF.wav";
	} else if (codigo == "45977") {
	    return "bairro_1328_NF.wav";
	} else if (codigo == "50003") {
	    return "bairro_1329_NF.wav";
	} else if (codigo == "990") {
	    return "bairro_133_NF.wav";
	} else if (codigo == "50301") {
	    return "bairro_1330_NF.wav";
	} else if (codigo == "14028") {
	    return "bairro_1334_NF.wav";
	} else if (codigo == "41246" || codigo == "42358") {
	    return "bairro_1336_NF.wav";
	} else if (codigo == "50231") {
	    return "bairro_1339_NF.wav";
	} else if (codigo == "11819") {
	    return "bairro_134_NF.wav";
	} else if (codigo == "42919") {
	    return "bairro_1344_NF.wav";
	} else if (codigo == "45942") {
	    return "bairro_1346_NF.wav";
	} else if (codigo == "46019") {
	    return "bairro_1347_NF.wav";
	} else if (codigo == "46027") {
	    return "bairro_1348_NF.wav";
	} else if (codigo == "46159") {
	    return "bairro_1349_NF.wav";
	} else if (codigo == "3191") {
	    return "bairro_135_NF.wav";
	} else if (codigo == "46299") {
	    return "bairro_1350_NF.wav";
	} else if (codigo == "46345") {
	    return "bairro_1351_NF.wav";
	} else if (codigo == "46418") {
	    return "bairro_1352_NF.wav";
	} else if (codigo == "46477") {
	    return "bairro_1354_NF.wav";
	} else if (codigo == "50002") {
	    return "bairro_1355_NF.wav";
	} else if (codigo == "50051") {
	    return "bairro_1357_NF.wav";
	} else if (codigo == "50242") {
	    return "bairro_1358_NF.wav";
	} else if (codigo == "50281") {
	    return "bairro_1359_NF.wav";
	} else if (codigo == "2267") {
	    return "bairro_136_NF.wav";
	} else if (codigo == "50311") {
	    return "bairro_1360_NF.wav";
	} else if (codigo == "50321") {
	    return "bairro_1361_NF.wav";
	} else if (codigo == "50501") {
	    return "bairro_1362_NF.wav";
	} else if (codigo == "50561") {
	    return "bairro_1363_NF.wav";
	} else if (codigo == "1562" || codigo == "7412") {
	    return "bairro_1364_NF.wav";
	} else if (codigo == "45217") {
	    return "bairro_1367_NF.wav";
	} else if (codigo == "45225") {
	    return "bairro_1368_NF.wav";
	} else if (codigo == "45241") {
	    return "bairro_1369_NF.wav";
	} else if (codigo == "434") {
	    return "bairro_137_NF.wav";
	} else if (codigo == "45268") {
	    return "bairro_1370_NF.wav";
	} else if (codigo == "45284") {
	    return "bairro_1372_NF.wav";
	} else if (codigo == "45292") {
	    return "bairro_1373_NF.wav";
	} else if (codigo == "45306") {
	    return "bairro_1374_NF.wav";
	} else if (codigo == "45322") {
	    return "bairro_1376_NF.wav";
	} else if (codigo == "50261") {
	    return "bairro_1377_NF.wav";
	} else if (codigo == "50591") {
	    return "bairro_1378_NF.wav";
	} else if (codigo == "1929" || codigo == "5070") {
	    return "bairro_138_NF.wav";
	} else if (codigo == "4197") {
	    return "bairro_1381_NF.wav";
	} else if (codigo == "12327") {
	    return "bairro_1387_NF.wav";
	} else if (codigo == "5789") {
	    return "bairro_139_NF.wav";
	} else if (codigo == "42196") {
	    return "bairro_1391_NF.wav";
	} else if (codigo == "45861") {
	    return "bairro_1395_NF.wav";
	} else if (codigo == "46191") {
	    return "bairro_1396_NF.wav";
	} else if (codigo == "50161") {
	    return "bairro_1398_NF.wav";
	} else if (codigo == "50192") {
	    return "bairro_1399_NF.wav";
	} else if (codigo == "531") {
	    return "bairro_14_NF.wav";
	} else if (codigo == "1201") {
	    return "bairro_140_NF.wav";
	} else if (codigo == "50481") {
	    return "bairro_1400_NF.wav";
	} else if (codigo == "3174") {
	    return "bairro_1401_NF.wav";
	} else if (codigo == "5223") {
	    return "bairro_1402_NF.wav";
	} else if (codigo == "43087") {
	    return "bairro_1408_NF.wav";
	} else if (codigo == "10871") {
	    return "bairro_141_NF.wav";
	} else if (codigo == "46442") {
	    return "bairro_1411_NF.wav";
	} else if (codigo == "46485") {
	    return "bairro_1412_NF.wav";
	} else if (codigo == "4448") {
	    return "bairro_1417_NF.wav";
	} else if (codigo == "50221") {
	    return "bairro_1418_NF.wav";
	} else if (codigo == "50541") {
	    return "bairro_1419_NF.wav";
	} else if (codigo == "1317") {
	    return "bairro_142_NF.wav";
	} else if (codigo == "4553") {
	    return "bairro_1420_NF.wav";
	} else if (codigo == "6670") {
	    return "bairro_1423_NF.wav";
	} else if (codigo == "45888") {
	    return "bairro_1424_NF.wav";
	} else if (codigo == "46361") {
	    return "bairro_1426_NF.wav";
	} else if (codigo == "50071") {
	    return "bairro_1427_NF.wav";
	} else if (codigo == "50111") {
	    return "bairro_1428_NF.wav";
	} else if (codigo == "50251") {
	    return "bairro_1429_NF.wav";
	} else if (codigo == "4171") {
	    return "bairro_143_NF.wav";
	} else if (codigo == "50262") {
	    return "bairro_1430_NF.wav";
	} else if (codigo == "50361") {
	    return "bairro_1431_NF.wav";
	} else if (codigo == "10723") {
	    return "bairro_1433_NF.wav";
	} else if (codigo == "14371") {
	    return "bairro_1434_NF.wav";
	} else if (codigo == "46086") {
	    return "bairro_1436_NF.wav";
	} else if (codigo == "50291") {
	    return "bairro_1437_NF.wav";
	} else if (codigo == "50351") {
	    return "bairro_1438_NF.wav";
	} else if (codigo == "9407") {
	    return "bairro_144_NF.wav";
	} else if (codigo == "45845") {
	    return "bairro_1440_NF.wav";
	} else if (codigo == "45896") {
	    return "bairro_1443_NF.wav";
	} else if (codigo == "46116") {
	    return "bairro_1444_NF.wav";
	} else if (codigo == "46337") {
	    return "bairro_1445_NF.wav";
	} else if (codigo == "46469") {
	    return "bairro_1446_NF.wav";
	} else if (codigo == "50371") {
	    return "bairro_1448_NF.wav";
	} else if (codigo == "42412") {
	    return "bairro_145_NF.wav";
	} else if (codigo == "14419") {
	    return "bairro_1450_NF.wav";
	} else if (codigo == "40177") {
	    return "bairro_1456_NF.wav";
	} else if (codigo == "1040") {
	    return "bairro_146_NF.wav";
	} else if (codigo == "46043") {
	    return "bairro_1466_NF.wav";
	} else if (codigo == "46078") {
	    return "bairro_1467_NF.wav";
	} else if (codigo == "46094") {
	    return "bairro_1468_NF.wav";
	} else if (codigo == "46108") {
	    return "bairro_1469_NF.wav";
	} else if (codigo == "3255" || codigo == "42021") {
	    return "bairro_147_NF.wav";
	} else if (codigo == "46124") {
	    return "bairro_1470_NF.wav";
	} else if (codigo == "46221") {
	    return "bairro_1472_NF.wav";
	} else if (codigo == "46302") {
	    return "bairro_1473_NF.wav";
	} else if (codigo == "50341") {
	    return "bairro_1474_NF.wav";
	} else if (codigo == "40061") {
	    return "bairro_1476_NF.wav";
	} else if (codigo == "5568") {
	    return "bairro_148_NF.wav";
	} else if (codigo == "46264") {
	    return "bairro_1480_NF.wav";
	} else if (codigo == "50271") {
	    return "bairro_1481_NF.wav";
	} else if (codigo == "50331") {
	    return "bairro_1482_NF.wav";
	} else if (codigo == "50401") {
	    return "bairro_1483_NF.wav";
	} else if (codigo == "50461") {
	    return "bairro_1484_NF.wav";
	} else if (codigo == "50511") {
	    return "bairro_1485_NF.wav";
	} else if (codigo == "50521") {
	    return "bairro_1486_NF.wav";
	} else if (codigo == "50581") {
	    return "bairro_1487_NF.wav";
	} else if (codigo == "46272") {
	    return "bairro_1489_NF.wav";
	} else if (codigo == "1341") {
	    return "bairro_149_NF.wav";
	} else if (codigo == "42013") {
	    return "bairro_1493_NF.wav";
	} else if (codigo == "50041") {
	    return "bairro_1496_NF.wav";
	} else if (codigo == "2445") {
	    return "bairro_1497_NF.wav";
	} else if (codigo == "36200") {
	    return "bairro_1498_NF.wav";
	} else if (codigo == "7897") {
	    return "bairro_15_NF.wav";
	} else if (codigo == "12092") {
	    return "bairro_150_NF.wav";
	} else if (codigo == "2691") {
	    return "bairro_1500_NF.wav";
	} else if (codigo == "18333") {
	    return "bairro_1501_NF.wav";
	} else if (codigo == "2071") {
	    return "bairro_1502_NF.wav";
	} else if (codigo == "41203") {
	    return "bairro_1507_NF.wav";
	} else if (codigo == "50551") {
	    return "bairro_1508_NF.wav";
	} else if (codigo == "40673") {
	    return "bairro_1509_NF.wav";
	} else if (codigo == "5487") {
	    return "bairro_151_NF.wav";
	} else if (codigo == "45705") {
	    return "bairro_1513_NF.wav";
	} else if (codigo == "46035") {
	    return "bairro_1514_NF.wav";
	} else if (codigo == "46329") {
	    return "bairro_1515_NF.wav";
	} else if (codigo == "50011") {
	    return "bairro_1517_NF.wav";
	} else if (codigo == "50091") {
	    return "bairro_1518_NF.wav";
	} else if (codigo == "50121") {
	    return "bairro_1519_NF.wav";
	} else if (codigo == "5509") {
	    return "bairro_152_NF.wav";
	} else if (codigo == "50222") {
	    return "bairro_1520_NF.wav";
	} else if (codigo == "50552") {
	    return "bairro_1524_NF.wav";
	} else if (codigo == "50171") {
	    return "bairro_1525_NF.wav";
	} else if (codigo == "40827") {
	    return "bairro_1526_NF.wav";
	} else if (codigo == "41866") {
	    return "bairro_1527_NF.wav";
	} else if (codigo == "50021" || codigo == "46183" || codigo == "8150") {
	    return "bairro_1529_NF.wav";
	} else if (codigo == "44172") {
	    return "bairro_153_NF.wav";
	} else if (codigo == "13854") {
	    return "bairro_154_NF.wav";
	} else if (codigo == "3298") {
	    return "bairro_155_NF.wav";
	} else if (codigo == "3794" || codigo == "14605") {
	    return "bairro_156_NF.wav";
	} else if (codigo == "6408") {
	    return "bairro_157_NF.wav";
	} else if (codigo == "2313") {
	    return "bairro_158_NF.wav";
	} else if (codigo == "4260") {
	    return "bairro_159_NF.wav";
	} else if (codigo == "14176") {
	    return "bairro_16_NF.wav";
	} else if (codigo == "42145") {
	    return "bairro_160_NF.wav";
	} else if (codigo == "11274") {
	    return "bairro_162_NF.wav";
	} else if (codigo == "3115" || codigo == "9831") {
	    return "bairro_163_NF.wav";
	} else if (codigo == "11070") {
	    return "bairro_164_NF.wav";
	} else if (codigo == "3808") {
	    return "bairro_165_NF.wav";
	} else if (codigo == "3131") {
	    return "bairro_166_NF.wav";
	} else if (codigo == "10448") {
	    return "bairro_167_NF.wav";
	} else if (codigo == "3361") {
	    return "bairro_168_NF.wav";
	} else if (codigo == "892") {
	    return "bairro_169_NF.wav";
	} else if (codigo == "14648") {
	    return "bairro_17_NF.wav";
	} else if (codigo == "14508") {
	    return "bairro_170_NF.wav";
	} else if (codigo == "10782") {
	    return "bairro_171_NF.wav";
	} else if (codigo == "167") {
	    return "bairro_172_NF.wav";
	} else if (codigo == "221") {
	    return "bairro_173_NF.wav";
	} else if (codigo == "12050") {
	    return "bairro_174_NF.wav";
	} else if (codigo == "795") {
	    return "bairro_175_NF.wav";
	} else if (codigo == "7161") {
	    return "bairro_176_NF.wav";
	} else if (codigo == "701") {
	    return "bairro_177_NF.wav";
	} else if (codigo == "3719") {
	    return "bairro_178_NF.wav";
	} else if (codigo == "9709") {
	    return "bairro_179_NF.wav";
	} else if (codigo == "2356") {
	    return "bairro_18_NF.wav";
	} else if (codigo == "10511") {
	    return "bairro_180_NF.wav";
	} else if (codigo == "2615") {
	    return "bairro_181_NF.wav";
	} else if (codigo == "1911") {
	    return "bairro_182_NF.wav";
	} else if (codigo == "1449") {
	    return "bairro_183_NF.wav";
	} else if (codigo == "3051") {
	    return "bairro_184_NF.wav";
	} else if (codigo == "3913") {
	    return "bairro_185_NF.wav";
	} else if (codigo == "9903") {
	    return "bairro_186_NF.wav";
	} else if (codigo == "9849") {
	    return "bairro_187_NF.wav";
	} else if (codigo == "1163") {
	    return "bairro_188_NF.wav";
	} else if (codigo == "1198") {
	    return "bairro_189_NF.wav";
	} else if (codigo == "5240" || codigo == "12980") {
	    return "bairro_19_NF.wav";
	} else if (codigo == "9601") {
	    return "bairro_190_NF.wav";
	} else if (codigo == "4529") {
	    return "bairro_191_NF.wav";
	} else if (codigo == "2895") {
	    return "bairro_192_NF.wav";
	} else if (codigo == "4979") {
	    return "bairro_193_NF.wav";
	} else if (codigo == "10375") {
	    return "bairro_194_NF.wav";
	} else if (codigo == "11878") {
	    return "bairro_195_NF.wav";
	} else if (codigo == "9652") {
	    return "bairro_196_NF.wav";
	} else if (codigo == "12912") {
	    return "bairro_197_NF.wav";
	} else if (codigo == "2828") {
	    return "bairro_198_NF.wav";
	} else if (codigo == "6734") {
	    return "bairro_199_NF.wav";
	} else if (codigo == "43664") {
	    return "bairro_2_NF.wav";
	} else if (codigo == "3891") {
	    return "bairro_20_NF.wav";
	} else if (codigo == "9571") {
	    return "bairro_200_NF.wav";
	} else if (codigo == "191") {
	    return "bairro_201_NF.wav";
	} else if (codigo == "11088") {
	    return "bairro_202_NF.wav";
	} else if (codigo == "9610") {
	    return "bairro_203_NF.wav";
	} else if (codigo == "2470") {
	    return "bairro_204_NF.wav";
	} else if (codigo == "9679") {
	    return "bairro_205_NF.wav";
	} else if (codigo == "11436") {
	    return "bairro_206_NF.wav";
	} else if (codigo == "6301") {
	    return "bairro_207_NF.wav";
	} else if (codigo == "9580") {
	    return "bairro_208_NF.wav";
	} else if (codigo == "11355") {
	    return "bairro_209_NF.wav";
	} else if (codigo == "2518") {
	    return "bairro_21_NF.wav";
	} else if (codigo == "2666") {
	    return "bairro_210_NF.wav";
	} else if (codigo == "4201") {
	    return "bairro_211_NF.wav";
	} else if (codigo == "40681" || codigo == "5193") {
	    return "bairro_212_NF.wav";
	} else if (codigo == "1643") {
	    return "bairro_214_NF.wav";
	} else if (codigo == "6505") {
	    return "bairro_215_NF.wav";
	} else if (codigo == "9083") {
	    return "bairro_216_NF.wav";
	} else if (codigo == "9636") {
	    return "bairro_217_NF.wav";
	} else if (codigo == "353") {
	    return "bairro_218_NF.wav";
	} else if (codigo == "248") {
	    return "bairro_219_NF.wav";
	} else if (codigo == "44369") {
	    return "bairro_22_NF.wav";
	} else if (codigo == "26204") {
	    return "bairro_220_NF.wav";
	} else if (codigo == "3735") {
	    return "bairro_221_NF.wav";
	} else if (codigo == "41688") {
	    return "bairro_222_NF.wav";
	} else if (codigo == "11649") {
	    return "bairro_223_NF.wav";
	} else if (codigo == "1031") {
	    return "bairro_224_NF.wav";
	} else if (codigo == "558") {
	    return "bairro_225_NF.wav";
	} else if (codigo == "3000") {
	    return "bairro_226_NF.wav";
	} else if (codigo == "8567") {
	    return "bairro_227_NF.wav";
	} else if (codigo == "914") {
	    return "bairro_228_NF.wav";
	} else if (codigo == "7081") {
	    return "bairro_229_NF.wav";
	} else if (codigo == "4642" || codigo == "45632") {
	    return "bairro_23_NF.wav";
	} else if (codigo == "43931") {
	    return "bairro_230_NF.wav";
	} else if (codigo == "1872" || codigo == "50181") {
	    return "bairro_231_NF.wav";
	} else if (codigo == "26158") {
	    return "bairro_232_NF.wav";
	} else if (codigo == "44253") {
	    return "bairro_233_NF.wav";
	} else if (codigo == "3409") {
	    return "bairro_234_NF.wav";
	} else if (codigo == "8346") {
	    return "bairro_235_NF.wav";
	} else if (codigo == "12173") {
	    return "bairro_236_NF.wav";
	} else if (codigo == "94") {
	    return "bairro_237_NF.wav";
	} else if (codigo == "14281") {
	    return "bairro_238_NF.wav";
	} else if (codigo == "779") {
	    return "bairro_239_NF.wav";
	} else if (codigo == "5266") {
	    return "bairro_24_NF.wav";
	} else if (codigo == "42188") {
	    return "bairro_240_NF.wav";
	} else if (codigo == "8036") {
	    return "bairro_241_NF.wav";
	} else if (codigo == "42218") {
	    return "bairro_242_NF.wav";
	} else if (codigo == "11312") {
	    return "bairro_243_NF.wav";
	} else if (codigo == "159") {
	    return "bairro_244_NF.wav";
	} else if (codigo == "41807") {
	    return "bairro_245_NF.wav";
	} else if (codigo == "1376") {
	    return "bairro_246_NF.wav";
	} else if (codigo == "418") {
	    return "bairro_247_NF.wav";
	} else if (codigo == "3981") {
	    return "bairro_248_NF.wav";
	} else if (codigo == "12289") {
	    return "bairro_249_NF.wav";
	} else if (codigo == "655") {
	    return "bairro_25_NF.wav";
	} else if (codigo == "1708") {
	    return "bairro_250_NF.wav";
	} else if (codigo == "12807") {
	    return "bairro_251_NF.wav";
	} else if (codigo == "44466") {
	    return "bairro_252_NF.wav";
	} else if (codigo == "44458" || codigo == "40096") {
	    return "bairro_253_NF.wav";
	} else if (codigo == "42269") {
	    return "bairro_254_NF.wav";
	} else if (codigo == "361") {
	    return "bairro_256_NF.wav";
	} else if (codigo == "2372") {
	    return "bairro_257_NF.wav";
	} else if (codigo == "6912") {
	    return "bairro_258_NF.wav";
	} else if (codigo == "1236") {
	    return "bairro_259_NF.wav";
	} else if (codigo == "4995") {
	    return "bairro_26_NF.wav";
	} else if (codigo == "41491") {
	    return "bairro_260_NF.wav";
	} else if (codigo == "42951") {
	    return "bairro_261_NF.wav";
	} else if (codigo == "40894") {
	    return "bairro_262_NF.wav";
	} else if (codigo == "540") {
	    return "bairro_263_NF.wav";
	} else if (codigo == "11410") {
	    return "bairro_264_NF.wav";
	} else if (codigo == "477") {
	    return "bairro_265_NF.wav";
	} else if (codigo == "42846") {
	    return "bairro_266_NF.wav";
	} else if (codigo == "40266") {
	    return "bairro_267_NF.wav";
	} else if (codigo == "43311") {
	    return "bairro_268_NF.wav";
	} else if (codigo == "3271" || codigo == "5401") {
	    return "bairro_269_NF.wav";
	} else if (codigo == "4561") {
	    return "bairro_27_NF.wav";
	} else if (codigo == "41327") {
	    return "bairro_270_NF.wav";
	} else if (codigo == "1945") {
	    return "bairro_271_NF.wav";
	} else if (codigo == "#N" || codigo == "D") {
	    return "bairro_272_NF.wav";
	} else if (codigo == "43508") {
	    return "bairro_273_NF.wav";
	} else if (codigo == "11240") {
	    return "bairro_274_NF.wav";
	} else if (codigo == "40258") {
	    return "bairro_275_NF.wav";
	} else if (codigo == "1970") {
	    return "bairro_276_NF.wav";
	} else if (codigo == "2399") {
	    return "bairro_277_NF.wav";
	} else if (codigo == "1121") {
	    return "bairro_278_NF.wav";
	} else if (codigo == "744") {
	    return "bairro_279_NF.wav";
	} else if (codigo == "43478") {
	    return "bairro_28_NF.wav";
	} else if (codigo == "2437") {
	    return "bairro_280_NF.wav";
	} else if (codigo == "574") {
	    return "bairro_281_NF.wav";
	} else if (codigo == "1091") {
	    return "bairro_282_NF.wav";
	} else if (codigo == "12106") {
	    return "bairro_283_NF.wav";
	} else if (codigo == "2861") {
	    return "bairro_284_NF.wav";
	} else if (codigo == "11185") {
	    return "bairro_285_NF.wav";
	} else if (codigo == "2305") {
	    return "bairro_286_NF.wav";
	} else if (codigo == "671") {
	    return "bairro_287_NF.wav";
	} else if (codigo == "1511") {
	    return "bairro_288_NF.wav";
	} else if (codigo == "370") {
	    return "bairro_289_NF.wav";
	} else if (codigo == "8273" || codigo == "45411") {
	    return "bairro_29_NF.wav";
	} else if (codigo == "12025") {
	    return "bairro_290_NF.wav";
	} else if (codigo == "7200") {
	    return "bairro_291_NF.wav";
	} else if (codigo == "264") {
	    return "bairro_292_NF.wav";
	} else if (codigo == "7188") {
	    return "bairro_293_NF.wav";
	} else if (codigo == "5452") {
	    return "bairro_294_NF.wav";
	} else if (codigo == "3450") {
	    return "bairro_295_NF.wav";
	} else if (codigo == "14061") {
	    return "bairro_296_NF.wav";
	} else if (codigo == "14141") {
	    return "bairro_297_NF.wav";
	} else if (codigo == "13994") {
	    return "bairro_298_NF.wav";
	} else if (codigo == "40754") {
	    return "bairro_299_NF.wav";
	} else if (codigo == "44385") {
	    return "bairro_3_NF.wav";
	} else if (codigo == "6521") {
	    return "bairro_30_NF.wav";
	} else if (codigo == "4162" || codigo == "50053") {
	    return "bairro_300_NF.wav";
	} else if (codigo == "42005") {
	    return "bairro_301_NF.wav";
	} else if (codigo == "40592") {
	    return "bairro_302_NF.wav";
	} else if (codigo == "9865") {
	    return "bairro_303_NF.wav";
	} else if (codigo == "6866") {
	    return "bairro_304_NF.wav";
	} else if (codigo == "9881") {
	    return "bairro_305_NF.wav";
	} else if (codigo == "2780") {
	    return "bairro_306_NF.wav";
	} else if (codigo == "6726") {
	    return "bairro_307_NF.wav";
	} else if (codigo == "2348") {
	    return "bairro_308_NF.wav";
	} else if (codigo == "132") {
	    return "bairro_309_NF.wav";
	} else if (codigo == "13111") {
	    return "bairro_31_NF.wav";
	} else if (codigo == "13081") {
	    return "bairro_310_NF.wav";
	} else if (codigo == "14095") {
	    return "bairro_311_NF.wav";
	} else if (codigo == "2763") {
	    return "bairro_312_NF.wav";
	} else if (codigo == "43125") {
	    return "bairro_313_NF.wav";
	} else if (codigo == "7030") {
	    return "bairro_314_NF.wav";
	} else if (codigo == "12041") {
	    return "bairro_315_NF.wav";
	} else if (codigo == "40517") {
	    return "bairro_316_NF.wav";
	} else if (codigo == "1023") {
	    return "bairro_317_NF.wav";
	} else if (codigo == "9440") {
	    return "bairro_318_NF.wav";
	} else if (codigo == "42633") {
	    return "bairro_319_NF.wav";
	} else if (codigo == "3671") {
	    return "bairro_32_NF.wav";
	} else if (codigo == "6742") {
	    return "bairro_320_NF.wav";
	} else if (codigo == "1805") {
	    return "bairro_321_NF.wav";
	} else if (codigo == "14699") {
	    return "bairro_322_NF.wav";
	} else if (codigo == "2950") {
	    return "bairro_323_NF.wav";
	} else if (codigo == "973") {
	    return "bairro_324_NF.wav";
	} else if (codigo == "13919") {
	    return "bairro_325_NF.wav";
	} else if (codigo == "9211") {
	    return "bairro_326_NF.wav";
	} else if (codigo == "9334") {
	    return "bairro_327_NF.wav";
	} else if (codigo == "43249") {
	    return "bairro_328_NF.wav";
	} else if (codigo == "5118") {
	    return "bairro_329_NF.wav";
	} else if (codigo == "272" || codigo == "44202") {
	    return "bairro_33_NF.wav";
	} else if (codigo == "60") {
	    return "bairro_330_NF.wav";
	} else if (codigo == "12904") {
	    return "bairro_331_NF.wav";
	} else if (codigo == "42838") {
	    return "bairro_332_NF.wav";
	} else if (codigo == "12939") {
	    return "bairro_333_NF.wav";
	} else if (codigo == "175") {
	    return "bairro_334_NF.wav";
	} else if (codigo == "40649") {
	    return "bairro_335_NF.wav";
	} else if (codigo == "1422") {
	    return "bairro_336_NF.wav";
	} else if (codigo == "965") {
	    return "bairro_337_NF.wav";
	} else if (codigo == "11151") {
	    return "bairro_338_NF.wav";
	} else if (codigo == "281" || codigo == "3832") {
	    return "bairro_339_NF.wav";
	} else if (codigo == "4715" || codigo == "44318" || codigo == "43753") {
	    return "bairro_340_NF.wav";
	} else if (codigo == "1813") {
	    return "bairro_341_NF.wav";
	} else if (codigo == "3301" || codigo == "44539") {
	    return "bairro_342_NF.wav";
	} else if (codigo == "43389" || codigo == "45381") {
	    return "bairro_343_NF.wav";
	} else if (codigo == "43524") {
	    return "bairro_344_NF.wav";
	} else if (codigo == "11789") {
	    return "bairro_345_NF.wav";
	} else if (codigo == "8435") {
	    return "bairro_346_NF.wav";
	} else if (codigo == "41939") {
	    return "bairro_347_NF.wav";
	} else if (codigo == "9261") {
	    return "bairro_348_NF.wav";
	} else if (codigo == "469") {
	    return "bairro_349_NF.wav";
	} else if (codigo == "5983") {
	    return "bairro_35_NF.wav";
	} else if (codigo == "42366") {
	    return "bairro_350_NF.wav";
	} else if (codigo == "44377") {
	    return "bairro_351_NF.wav";
	} else if (codigo == "43923") {
	    return "bairro_352_NF.wav";
	} else if (codigo == "9326") {
	    return "bairro_353_NF.wav";
	} else if (codigo == "3662") {
	    return "bairro_354_NF.wav";
	} else if (codigo == "7064") {
	    return "bairro_355_NF.wav";
	} else if (codigo == "396") {
	    return "bairro_356_NF.wav";
	} else if (codigo == "6441") {
	    return "bairro_357_NF.wav";
	} else if (codigo == "2241") {
	    return "bairro_358_NF.wav";
	} else if (codigo == "13951") {
	    return "bairro_359_NF.wav";
	} else if (codigo == "43575") {
	    return "bairro_36_NF.wav";
	} else if (codigo == "42706") {
	    return "bairro_360_NF.wav";
	} else if (codigo == "582") {
	    return "bairro_361_NF.wav";
	} else if (codigo == "41696") {
	    return "bairro_362_NF.wav";
	} else if (codigo == "9342") {
	    return "bairro_363_NF.wav";
	} else if (codigo == "9415") {
	    return "bairro_364_NF.wav";
	} else if (codigo == "11959") {
	    return "bairro_365_NF.wav";
	} else if (codigo == "40347") {
	    return "bairro_366_NF.wav";
	} else if (codigo == "44415") {
	    return "bairro_367_NF.wav";
	} else if (codigo == "43036") {
	    return "bairro_368_NF.wav";
	} else if (codigo == "1457") {
	    return "bairro_369_NF.wav";
	} else if (codigo == "13455" || codigo == "42056") {
	    return "bairro_37_NF.wav";
	} else if (codigo == "14885") {
	    return "bairro_370_NF.wav";
	} else if (codigo == "13986") {
	    return "bairro_371_NF.wav";
	} else if (codigo == "41793") {
	    return "bairro_372_NF.wav";
	} else if (codigo == "12696") {
	    return "bairro_373_NF.wav";
	} else if (codigo == "13960") {
	    return "bairro_374_NF.wav";
	} else if (codigo == "5622" || codigo == "44407" || codigo == "42455") {
	    return "bairro_375_NF.wav";
	} else if (codigo == "14389") {
	    return "bairro_376_NF.wav";
	} else if (codigo == "3930") {
	    return "bairro_377_NF.wav";
	} else if (codigo == "5614") {
	    return "bairro_378_NF.wav";
	} else if (codigo == "13811") {
	    return "bairro_379_NF.wav";
	} else if (codigo == "34304") {
	    return "bairro_38_NF.wav";
	} else if (codigo == "591") {
	    return "bairro_380_NF.wav";
	} else if (codigo == "1368") {
	    return "bairro_381_NF.wav";
	} else if (codigo == "14478") {
	    return "bairro_382_NF.wav";
	} else if (codigo == "43") {
	    return "bairro_383_NF.wav";
	} else if (codigo == "13161") {
	    return "bairro_384_NF.wav";
	} else if (codigo == "11983" || codigo == "42315") {
	    return "bairro_385_NF.wav";
	} else if (codigo == "566") {
	    return "bairro_386_NF.wav";
	} else if (codigo == "10472") {
	    return "bairro_387_NF.wav";
	} else if (codigo == "6840") {
	    return "bairro_388_NF.wav";
	} else if (codigo == "9393") {
	    return "bairro_389_NF.wav";
	} else if (codigo == "2186") {
	    return "bairro_39_NF.wav";
	} else if (codigo == "1571") {
	    return "bairro_390_NF.wav";
	} else if (codigo == "13889") {
	    return "bairro_391_NF.wav";
	} else if (codigo == "7196") {
	    return "bairro_392_NF.wav";
	} else if (codigo == "43338") {
	    return "bairro_393_NF.wav";
	} else if (codigo == "2755") {
	    return "bairro_394_NF.wav";
	} else if (codigo == "8575") {
	    return "bairro_395_NF.wav";
	} else if (codigo == "10154") {
	    return "bairro_396_NF.wav";
	} else if (codigo == "14206") {
	    return "bairro_397_NF.wav";
	} else if (codigo == "442") {
	    return "bairro_398_NF.wav";
	} else if (codigo == "5321") {
	    return "bairro_399_NF.wav";
	} else if (codigo == "1848") {
	    return "bairro_4_NF.wav";
	} else if (codigo == "230") {
	    return "bairro_40_NF.wav";
	} else if (codigo == "14729") {
	    return "bairro_400_NF.wav";
	} else if (codigo == "11428") {
	    return "bairro_401_NF.wav";
	} else if (codigo == "41289") {
	    return "bairro_402_NF.wav";
	} else if (codigo == "9890") {
	    return "bairro_403_NF.wav";
	} else if (codigo == "10561") {
	    return "bairro_404_NF.wav";
	} else if (codigo == "40444") {
	    return "bairro_405_NF.wav";
	} else if (codigo == "41343") {
	    return "bairro_406_NF.wav";
	} else if (codigo == "40606") {
	    return "bairro_407_NF.wav";
	} else if (codigo == "41769") {
	    return "bairro_408_NF.wav";
	} else if (codigo == "44601") {
	    return "bairro_409_NF.wav";
	} else if (codigo == "515") {
	    return "bairro_41_NF.wav";
	} else if (codigo == "41599") {
	    return "bairro_410_NF.wav";
	} else if (codigo == "42781") {
	    return "bairro_411_NF.wav";
	} else if (codigo == "42153") {
	    return "bairro_412_NF.wav";
	} else if (codigo == "5410") {
	    return "bairro_413_NF.wav";
	} else if (codigo == "5428") {
	    return "bairro_414_NF.wav";
	} else if (codigo == "41165" || codigo == "43281") {
	    return "bairro_415_NF.wav";
	} else if (codigo == "40665") {
	    return "bairro_416_NF.wav";
	} else if (codigo == "3263") {
	    return "bairro_417_NF.wav";
	} else if (codigo == "41041") {
	    return "bairro_418_NF.wav";
	} else if (codigo == "43028") {
	    return "bairro_419_NF.wav";
	} else if (codigo == "35") {
	    return "bairro_42_NF.wav";
	} else if (codigo == "41114") {
	    return "bairro_420_NF.wav";
	} else if (codigo == "42889") {
	    return "bairro_421_NF.wav";
	} else if (codigo == "43826") {
	    return "bairro_422_NF.wav";
	} else if (codigo == "14249") {
	    return "bairro_423_NF.wav";
	} else if (codigo == "10715") {
	    return "bairro_424_NF.wav";
	} else if (codigo == "42501") {
	    return "bairro_425_NF.wav";
	} else if (codigo == "5495") {
	    return "bairro_426_NF.wav";
	} else if (codigo == "4961") {
	    return "bairro_427_NF.wav";
	} else if (codigo == "11380") {
	    return "bairro_428_NF.wav";
	} else if (codigo == "3573") {
	    return "bairro_429_NF.wav";
	} else if (codigo == "7382") {
	    return "bairro_43_NF.wav";
	} else if (codigo == "841") {
	    return "bairro_430_NF.wav";
	} else if (codigo == "15318") {
	    return "bairro_431_NF.wav";
	} else if (codigo == "11061") {
	    return "bairro_432_NF.wav";
	} else if (codigo == "10421") {
	    return "bairro_433_NF.wav";
	} else if (codigo == "43222") {
	    return "bairro_434_NF.wav";
	} else if (codigo == "7650") {
	    return "bairro_435_NF.wav";
	} else if (codigo == "6068") {
	    return "bairro_436_NF.wav";
	} else if (codigo == "43613") {
	    return "bairro_437_NF.wav";
	} else if (codigo == "9237") {
	    return "bairro_438_NF.wav";
	} else if (codigo == "40215") {
	    return "bairro_439_NF.wav";
	} else if (codigo == "40487") {
	    return "bairro_44_NF.wav";
	} else if (codigo == "4120") {
	    return "bairro_440_NF.wav";
	} else if (codigo == "9423") {
	    return "bairro_441_NF.wav";
	} else if (codigo == "5533") {
	    return "bairro_442_NF.wav";
	} else if (codigo == "11479") {
	    return "bairro_443_NF.wav";
	} else if (codigo == "14214") {
	    return "bairro_444_NF.wav";
	} else if (codigo == "2259") {
	    return "bairro_445_NF.wav";
	} else if (codigo == "6262") {
	    return "bairro_446_NF.wav";
	} else if (codigo == "41378") {
	    return "bairro_447_NF.wav";
	} else if (codigo == "41068") {
	    return "bairro_448_NF.wav";
	} else if (codigo == "1104") {
	    return "bairro_449_NF.wav";
	} else if (codigo == "9318") {
	    return "bairro_45_NF.wav";
	} else if (codigo == "11517") {
	    return "bairro_450_NF.wav";
	} else if (codigo == "40339") {
	    return "bairro_451_NF.wav";
	} else if (codigo == "10961") {
	    return "bairro_452_NF.wav";
	} else if (codigo == "1228") {
	    return "bairro_453_NF.wav";
	} else if (codigo == "10669") {
	    return "bairro_454_NF.wav";
	} else if (codigo == "14869") {
	    return "bairro_455_NF.wav";
	} else if (codigo == "42609") {
	    return "bairro_456_NF.wav";
	} else if (codigo == "6998") {
	    return "bairro_457_NF.wav";
	} else if (codigo == "11584") {
	    return "bairro_458_NF.wav";
	} else if (codigo == "9938") {
	    return "bairro_459_NF.wav";
	} else if (codigo == "8541") {
	    return "bairro_46_NF.wav";
	} else if (codigo == "2941") {
	    return "bairro_460_NF.wav";
	} else if (codigo == "44288") {
	    return "bairro_461_NF.wav";
	} else if (codigo == "9971") {
	    return "bairro_462_NF.wav";
	} else if (codigo == "4839") {
	    return "bairro_463_NF.wav";
	} else if (codigo == "1899" || codigo == "41254") {
	    return "bairro_464_NF.wav";
	} else if (codigo == "14800") {
	    return "bairro_466_NF.wav";
	} else if (codigo == "40185") {
	    return "bairro_467_NF.wav";
	} else if (codigo == "6254") {
	    return "bairro_468_NF.wav";
	} else if (codigo == "14117") {
	    return "bairro_47_NF.wav";
	} else if (codigo == "40991") {
	    return "bairro_470_NF.wav";
	} else if (codigo == "42048") {
	    return "bairro_471_NF.wav";
	} else if (codigo == "40711") {
	    return "bairro_472_NF.wav";
	} else if (codigo == "2623" || codigo == "45349") {
	    return "bairro_473_NF.wav";
	} else if (codigo == "3948") {
	    return "bairro_474_NF.wav";
	} else if (codigo == "10219") {
	    return "bairro_475_NF.wav";
	} else if (codigo == "4626") {
	    return "bairro_476_NF.wav";
	} else if (codigo == "2526") {
	    return "bairro_477_NF.wav";
	} else if (codigo == "2721") {
	    return "bairro_478_NF.wav";
	} else if (codigo == "3964") {
	    return "bairro_479_NF.wav";
	} else if (codigo == "9750") {
	    return "bairro_48_NF.wav";
	} else if (codigo == "5479") {
	    return "bairro_480_NF.wav";
	} else if (codigo == "42226") {
	    return "bairro_481_NF.wav";
	} else if (codigo == "42854") {
	    return "bairro_482_NF.wav";
	} else if (codigo == "3336") {
	    return "bairro_483_NF.wav";
	} else if (codigo == "13871") {
	    return "bairro_484_NF.wav";
	} else if (codigo == "41882") {
	    return "bairro_485_NF.wav";
	} else if (codigo == "14524") {
	    return "bairro_486_NF.wav";
	} else if (codigo == "14681") {
	    return "bairro_487_NF.wav";
	} else if (codigo == "43435") {
	    return "bairro_488_NF.wav";
	} else if (codigo == "6246") {
	    return "bairro_489_NF.wav";
	} else if (codigo == "4227" || codigo == "9822") {
	    return "bairro_49_NF.wav";
	} else if (codigo == "9288") {
	    return "bairro_490_NF.wav";
	} else if (codigo == "40118") {
	    return "bairro_491_NF.wav";
	} else if (codigo == "2216") {
	    return "bairro_492_NF.wav";
	} else if (codigo == "12238") {
	    return "bairro_493_NF.wav";
	} else if (codigo == "4324") {
	    return "bairro_494_NF.wav";
	} else if (codigo == "40398") {
	    return "bairro_495_NF.wav";
	} else if (codigo == "14761") {
	    return "bairro_496_NF.wav";
	} else if (codigo == "4952") {
	    return "bairro_497_NF.wav";
	} else if (codigo == "9377") {
	    return "bairro_498_NF.wav";
	} else if (codigo == "7293") {
	    return "bairro_499_NF.wav";
	} else if (codigo == "30767") {
	    return "bairro_5_NF.wav";
	} else if (codigo == "44423") {
	    return "bairro_50_NF.wav";
	} else if (codigo == "4146") {
	    return "bairro_500_NF.wav";
	} else if (codigo == "14338") {
	    return "bairro_501_NF.wav";
	} else if (codigo == "6360") {
	    return "bairro_502_NF.wav";
	} else if (codigo == "40525") {
	    return "bairro_503_NF.wav";
	} else if (codigo == "43184") {
	    return "bairro_504_NF.wav";
	} else if (codigo == "42463") {
	    return "bairro_505_NF.wav";
	} else if (codigo == "11827") {
	    return "bairro_506_NF.wav";
	} else if (codigo == "11835") {
	    return "bairro_507_NF.wav";
	} else if (codigo == "183") {
	    return "bairro_508_NF.wav";
	} else if (codigo == "12297") {
	    return "bairro_509_NF.wav";
	} else if (codigo == "42722") {
	    return "bairro_51_NF.wav";
	} else if (codigo == "2887") {
	    return "bairro_510_NF.wav";
	} else if (codigo == "3476" || codigo == "5878") {
	    return "bairro_511_NF.wav";
	} else if (codigo == "78") {
	    return "bairro_512_NF.wav";
	} else if (codigo == "4111") {
	    return "bairro_513_NF.wav";
	} else if (codigo == "12769") {
	    return "bairro_514_NF.wav";
	} else if (codigo == "40916") {
	    return "bairro_515_NF.wav";
	} else if (codigo == "6084") {
	    return "bairro_516_NF.wav";
	} else if (codigo == "141") {
	    return "bairro_517_NF.wav";
	} else if (codigo == "44083") {
	    return "bairro_518_NF.wav";
	} else if (codigo == "51") {
	    return "bairro_519_NF.wav";
	} else if (codigo == "8087") {
	    return "bairro_52_NF.wav";
	} else if (codigo == "86") {
	    return "bairro_520_NF.wav";
	} else if (codigo == "6688") {
	    return "bairro_521_NF.wav";
	} else if (codigo == "14541") {
	    return "bairro_522_NF.wav";
	} else if (codigo == "12271") {
	    return "bairro_523_NF.wav";
	} else if (codigo == "9768") {
	    return "bairro_524_NF.wav";
	} else if (codigo == "9385") {
	    return "bairro_525_NF.wav";
	} else if (codigo == "44059") {
	    return "bairro_526_NF.wav";
	} else if (codigo == "2461") {
	    return "bairro_527_NF.wav";
	} else if (codigo == "4634") {
	    return "bairro_528_NF.wav";
	} else if (codigo == "4090") {
	    return "bairro_529_NF.wav";
	} else if (codigo == "42692") {
	    return "bairro_53_NF.wav";
	} else if (codigo == "12009") {
	    return "bairro_530_NF.wav";
	} else if (codigo == "12165") {
	    return "bairro_531_NF.wav";
	} else if (codigo == "40495") {
	    return "bairro_532_NF.wav";
	} else if (codigo == "3689") {
	    return "bairro_533_NF.wav";
	} else if (codigo == "922") {
	    return "bairro_534_NF.wav";
	} else if (codigo == "1732") {
	    return "bairro_535_NF.wav";
	} else if (codigo == "1015") {
	    return "bairro_536_NF.wav";
	} else if (codigo == "16411") {
	    return "bairro_537_NF.wav";
	} else if (codigo == "12068") {
	    return "bairro_538_NF.wav";
	} else if (codigo == "736") {
	    return "bairro_539_NF.wav";
	} else if (codigo == "4863") {
	    return "bairro_54_NF.wav";
	} else if (codigo == "4057") {
	    return "bairro_540_NF.wav";
	} else if (codigo == "9229") {
	    return "bairro_541_NF.wav";
	} else if (codigo == "40614") {
	    return "bairro_542_NF.wav";
	} else if (codigo == "9784") {
	    return "bairro_543_NF.wav";
	} else if (codigo == "299") {
	    return "bairro_544_NF.wav";
	} else if (codigo == "2852") {
	    return "bairro_545_NF.wav";
	} else if (codigo == "44156") {
	    return "bairro_546_NF.wav";
	} else if (codigo == "3484") {
	    return "bairro_547_NF.wav";
	} else if (codigo == "40797") {
	    return "bairro_548_NF.wav";
	} else if (codigo == "523") {
	    return "bairro_549_NF.wav";
	} else if (codigo == "4537") {
	    return "bairro_55_NF.wav";
	} else if (codigo == "43729") {
	    return "bairro_550_NF.wav";
	} else if (codigo == "41386") {
	    return "bairro_551_NF.wav";
	} else if (codigo == "8176") {
	    return "bairro_552_NF.wav";
	} else if (codigo == "4987") {
	    return "bairro_553_NF.wav";
	} else if (codigo == "2739") {
	    return "bairro_554_NF.wav";
	} else if (codigo == "1627") {
	    return "bairro_555_NF.wav";
	} else if (codigo == "1597") {
	    return "bairro_556_NF.wav";
	} else if (codigo == "400") {
	    return "bairro_557_NF.wav";
	} else if (codigo == "6599") {
	    return "bairro_558_NF.wav";
	} else if (codigo == "1007") {
	    return "bairro_559_NF.wav";
	} else if (codigo == "3239") {
	    return "bairro_56_NF.wav";
	} else if (codigo == "43559") {
	    return "bairro_560_NF.wav";
	} else if (codigo == "4588") {
	    return "bairro_561_NF.wav";
	} else if (codigo == "3921") {
	    return "bairro_562_NF.wav";
	} else if (codigo == "2593") {
	    return "bairro_563_NF.wav";
	} else if (codigo == "4065") {
	    return "bairro_564_NF.wav";
	} else if (codigo == "14052") {
	    return "bairro_565_NF.wav";
	} else if (codigo == "43834") {
	    return "bairro_566_NF.wav";
	} else if (codigo == "6319") {
	    return "bairro_567_NF.wav";
	} else if (codigo == "9521") {
	    return "bairro_568_NF.wav";
	} else if (codigo == "3506" || codigo == "1589") {
	    return "bairro_569_NF.wav";
	} else if (codigo == "2551") {
	    return "bairro_57_NF.wav";
	} else if (codigo == "2321" || codigo == "3565" || codigo == "6157") {
	    return "bairro_570_NF.wav";
	} else if (codigo == "7846") {
	    return "bairro_571_NF.wav";
	} else if (codigo == "9059") {
	    return "bairro_572_NF.wav";
	} else if (codigo == "4740") {
	    return "bairro_573_NF.wav";
	} else if (codigo == "1082") {
	    return "bairro_575_NF.wav";
	} else if (codigo == "1210") {
	    return "bairro_576_NF.wav";
	} else if (codigo == "42072") {
	    return "bairro_577_NF.wav";
	} else if (codigo == "43621") {
	    return "bairro_578_NF.wav";
	} else if (codigo == "40584") {
	    return "bairro_579_NF.wav";
	} else if (codigo == "40657") {
	    return "bairro_58_NF.wav";
	} else if (codigo == "41181") {
	    return "bairro_580_NF.wav";
	} else if (codigo == "43346") {
	    return "bairro_581_NF.wav";
	} else if (codigo == "42617") {
	    return "bairro_582_NF.wav";
	} else if (codigo == "3212") {
	    return "bairro_583_NF.wav";
	} else if (codigo == "3310") {
	    return "bairro_584_NF.wav";
	} else if (codigo == "3875") {
	    return "bairro_585_NF.wav";
	} else if (codigo == "14290") {
	    return "bairro_586_NF.wav";
	} else if (codigo == "4421") {
	    return "bairro_587_NF.wav";
	} else if (codigo == "5941") {
	    return "bairro_588_NF.wav";
	} else if (codigo == "14192") {
	    return "bairro_589_NF.wav";
	} else if (codigo == "2097" || codigo == "10391") {
	    return "bairro_59_NF.wav";
	} else if (codigo == "4774") {
	    return "bairro_590_NF.wav";
	} else if (codigo == "3107") {
	    return "bairro_591_NF.wav";
	} else if (codigo == "32611" || codigo == "43451") {
	    return "bairro_592_NF.wav";
	} else if (codigo == "14672" || codigo == "42803") {
	    return "bairro_593_NF.wav";
	} else if (codigo == "6173") {
	    return "bairro_594_NF.wav";
	} else if (codigo == "40355") {
	    return "bairro_595_NF.wav";
	} else if (codigo == "11282") {
	    return "bairro_596_NF.wav";
	} else if (codigo == "2577") {
	    return "bairro_597_NF.wav";
	} else if (codigo == "16900") {
	    return "bairro_598_NF.wav";
	} else if (codigo == "9130") {
	    return "bairro_599_NF.wav";
	} else if (codigo == "4405") {
	    return "bairro_6_NF.wav";
	} else if (codigo == "43079") {
	    return "bairro_60_NF.wav";
	} else if (codigo == "11657") {
	    return "bairro_600_NF.wav";
	} else if (codigo == "1520") {
	    return "bairro_602_NF.wav";
	} else if (codigo == "40975") {
	    return "bairro_603_NF.wav";
	} else if (codigo == "43265") {
	    return "bairro_604_NF.wav";
	} else if (codigo == "876") {
	    return "bairro_606_NF.wav";
	} else if (codigo == "41009" || codigo == "3514") {
	    return "bairro_607_NF.wav";
	} else if (codigo == "4502" || codigo == "42765") {
	    return "bairro_608_NF.wav";
	} else if (codigo == "5011") {
	    return "bairro_609_NF.wav";
	} else if (codigo == "43303") {
	    return "bairro_61_NF.wav";
	} else if (codigo == "6947") {
	    return "bairro_611_NF.wav";
	} else if (codigo == "2402" || codigo == "3859") {
	    return "bairro_612_NF.wav";
	} else if (codigo == "40576") {
	    return "bairro_613_NF.wav";
	} else if (codigo == "14150") {
	    return "bairro_614_NF.wav";
	} else if (codigo == "5550") {
	    return "bairro_615_NF.wav";
	} else if (codigo == "663") {
	    return "bairro_616_NF.wav";
	} else if (codigo == "9105") {
	    return "bairro_617_NF.wav";
	} else if (codigo == "116") {
	    return "bairro_618_NF.wav";
	} else if (codigo == "6432") {
	    return "bairro_619_NF.wav";
	} else if (codigo == "8249") {
	    return "bairro_62_NF.wav";
	} else if (codigo == "957") {
	    return "bairro_620_NF.wav";
	} else if (codigo == "42862") {
	    return "bairro_621_NF.wav";
	} else if (codigo == "2275" || codigo == "42579") {
	    return "bairro_622_NF.wav";
	} else if (codigo == "11843") {
	    return "bairro_623_NF.wav";
	} else if (codigo == "9873") {
	    return "bairro_624_NF.wav";
	} else if (codigo == "4138") {
	    return "bairro_625_NF.wav";
	} else if (codigo == "14788") {
	    return "bairro_626_NF.wav";
	} else if (codigo == "4936") {
	    return "bairro_627_NF.wav";
	} else if (codigo == "1384") {
	    return "bairro_628_NF.wav";
	} else if (codigo == "4804") {
	    return "bairro_629_NF.wav";
	} else if (codigo == "5274") {
	    return "bairro_63_NF.wav";
	} else if (codigo == "1406") {
	    return "bairro_630_NF.wav";
	} else if (codigo == "42382") {
	    return "bairro_631_NF.wav";
	} else if (codigo == "1759") {
	    return "bairro_632_NF.wav";
	} else if (codigo == "1147") {
	    return "bairro_633_NF.wav";
	} else if (codigo == "11673") {
	    return "bairro_634_NF.wav";
	} else if (codigo == "2038") {
	    return "bairro_635_NF.wav";
	} else if (codigo == "4898") {
	    return "bairro_636_NF.wav";
	} else if (codigo == "3531") {
	    return "bairro_637_NF.wav";
	} else if (codigo == "42935") {
	    return "bairro_638_NF.wav";
	} else if (codigo == "10251" || codigo == "46213") {
	    return "bairro_639_NF.wav";
	} else if (codigo == "13935") {
	    return "bairro_64_NF.wav";
	} else if (codigo == "9504") {
	    return "bairro_640_NF.wav";
	} else if (codigo == "809") {
	    return "bairro_641_NF.wav";
	} else if (codigo == "3883") {
	    return "bairro_642_NF.wav";
	} else if (codigo == "4243" || codigo == "42471" || codigo == "42811" || codigo == "4391" || codigo == "41025") {
	    return "bairro_643_NF.wav";
	} else if (codigo == "12190") {
	    return "bairro_644_NF.wav";
	} else if (codigo == "41092") {
	    return "bairro_645_NF.wav";
	} else if (codigo == "44024") {
	    return "bairro_646_NF.wav";
	} else if (codigo == "44474") {
	    return "bairro_647_NF.wav";
	} else if (codigo == "44571") {
	    return "bairro_648_NF.wav";
	} else if (codigo == "43656") {
	    return "bairro_649_NF.wav";
	} else if (codigo == "2801") {
	    return "bairro_65_NF.wav";
	} else if (codigo == "2232") {
	    return "bairro_650_NF.wav";
	} else if (codigo == "43907") {
	    return "bairro_651_NF.wav";
	} else if (codigo == "40452") {
	    return "bairro_652_NF.wav";
	} else if (codigo == "14273") {
	    return "bairro_653_NF.wav";
	} else if (codigo == "44334") {
	    return "bairro_654_NF.wav";
	} else if (codigo == "43532") {
	    return "bairro_655_NF.wav";
	} else if (codigo == "41017") {
	    return "bairro_656_NF.wav";
	} else if (codigo == "42668") {
	    return "bairro_657_NF.wav";
	} else if (codigo == "43397") {
	    return "bairro_658_NF.wav";
	} else if (codigo == "44547") {
	    return "bairro_659_NF.wav";
	} else if (codigo == "43516") {
	    return "bairro_66_NF.wav";
	} else if (codigo == "6785") {
	    return "bairro_660_NF.wav";
	} else if (codigo == "11932") {
	    return "bairro_661_NF.wav";
	} else if (codigo == "6416") {
	    return "bairro_662_NF.wav";
	} else if (codigo == "9024") {
	    return "bairro_663_NF.wav";
	} else if (codigo == "451") {
	    return "bairro_664_NF.wav";
	} else if (codigo == "4456") {
	    return "bairro_665_NF.wav";
	} else if (codigo == "256") {
	    return "bairro_666_NF.wav";
	} else if (codigo == "42064") {
	    return "bairro_667_NF.wav";
	} else if (codigo == "3956") {
	    return "bairro_668_NF.wav";
	} else if (codigo == "1279") {
	    return "bairro_669_NF.wav";
	} else if (codigo == "8206") {
	    return "bairro_67_NF.wav";
	} else if (codigo == "9369") {
	    return "bairro_670_NF.wav";
	} else if (codigo == "3611") {
	    return "bairro_671_NF.wav";
	} else if (codigo == "14168") {
	    return "bairro_672_NF.wav";
	} else if (codigo == "12211") {
	    return "bairro_673_NF.wav";
	} else if (codigo == "42595") {
	    return "bairro_674_NF.wav";
	} else if (codigo == "710") {
	    return "bairro_675_NF.wav";
	} else if (codigo == "680") {
	    return "bairro_676_NF.wav";
	} else if (codigo == "11223") {
	    return "bairro_677_NF.wav";
	} else if (codigo == "7072") {
	    return "bairro_678_NF.wav";
	} else if (codigo == "43109") {
	    return "bairro_679_NF.wav";
	} else if (codigo == "1112") {
	    return "bairro_68_NF.wav";
	} else if (codigo == "1333") {
	    return "bairro_680_NF.wav";
	} else if (codigo == "2585") {
	    return "bairro_681_NF.wav";
	} else if (codigo == "9962") {
	    return "bairro_682_NF.wav";
	} else if (codigo == "825") {
	    return "bairro_683_NF.wav";
	} else if (codigo == "10197") {
	    return "bairro_684_NF.wav";
	} else if (codigo == "639") {
	    return "bairro_685_NF.wav";
	} else if (codigo == "11886") {
	    return "bairro_686_NF.wav";
	} else if (codigo == "42994") {
	    return "bairro_687_NF.wav";
	} else if (codigo == "12114") {
	    return "bairro_688_NF.wav";
	} else if (codigo == "7501") {
	    return "bairro_689_NF.wav";
	} else if (codigo == "44105") {
	    return "bairro_69_NF.wav";
	} else if (codigo == "14087") {
	    return "bairro_690_NF.wav";
	} else if (codigo == "1503") {
	    return "bairro_691_NF.wav";
	} else if (codigo == "8389") {
	    return "bairro_692_NF.wav";
	} else if (codigo == "9474") {
	    return "bairro_693_NF.wav";
	} else if (codigo == "6602") {
	    return "bairro_694_NF.wav";
	} else if (codigo == "43117") {
	    return "bairro_695_NF.wav";
	} else if (codigo == "4651") {
	    return "bairro_696_NF.wav";
	} else if (codigo == "12122") {
	    return "bairro_697_NF.wav";
	} else if (codigo == "6211") {
	    return "bairro_698_NF.wav";
	} else if (codigo == "8478") {
	    return "bairro_699_NF.wav";
	} else if (codigo == "2208") {
	    return "bairro_7_NF.wav";
	} else if (codigo == "3603" || codigo == "44555" || codigo == "38261" || codigo == "12823" || codigo == "6980") {
	    return "bairro_70_NF.wav";
	} else if (codigo == "3352" || codigo == "7480") {
	    return "bairro_700_NF.wav";
	} else if (codigo == "42447") {
	    return "bairro_701_NF.wav";
	} else if (codigo == "44075") {
	    return "bairro_702_NF.wav";
	} else if (codigo == "4481" || codigo == "6220") {
	    return "bairro_703_NF.wav";
	} else if (codigo == "3824") {
	    return "bairro_704_NF.wav";
	} else if (codigo == "5851" || codigo == "34312") {
	    return "bairro_705_NF.wav";
	} else if (codigo == "3727" || codigo == "6874" || codigo == "46205") {
	    return "bairro_706_NF.wav";
	} else if (codigo == "42439") {
	    return "bairro_707_NF.wav";
	} else if (codigo == "5304") {
	    return "bairro_708_NF.wav";
	} else if (codigo == "3158") {
	    return "bairro_709_NF.wav";
	} else if (codigo == "6475" || codigo == "11401" || codigo == "14613" || codigo == "14494") {
	    return "bairro_71_NF.wav";
	} else if (codigo == "41815") {
	    return "bairro_710_NF.wav";
	} else if (codigo == "44245") {
	    return "bairro_711_NF.wav";
	} else if (codigo == "12131") {
	    return "bairro_712_NF.wav";
	} else if (codigo == "9431") {
	    return "bairro_713_NF.wav";
	} else if (codigo == "2224") {
	    return "bairro_714_NF.wav";
	} else if (codigo == "26174") {
	    return "bairro_715_NF.wav";
	} else if (codigo == "44121") {
	    return "bairro_716_NF.wav";
	} else if (codigo == "1066") {
	    return "bairro_717_NF.wav";
	} else if (codigo == "3034") {
	    return "bairro_718_NF.wav";
	} else if (codigo == "9270") {
	    return "bairro_719_NF.wav";
	} else if (codigo == "981") {
	    return "bairro_72_NF.wav";
	} else if (codigo == "3042") {
	    return "bairro_720_NF.wav";
	} else if (codigo == "40037") {
	    return "bairro_721_NF.wav";
	} else if (codigo == "1864" || codigo == "44636") {
	    return "bairro_722_NF.wav";
	} else if (codigo == "2682") {
	    return "bairro_723_NF.wav";
	} else if (codigo == "40169") {
	    return "bairro_724_NF.wav";
	} else if (codigo == "41904") {
	    return "bairro_725_NF.wav";
	} else if (codigo == "2674" || codigo == "40223") {
	    return "bairro_726_NF.wav";
	} else if (codigo == "40851") {
	    return "bairro_727_NF.wav";
	} else if (codigo == "2607" || codigo == "46353" || codigo == "42943") {
	    return "bairro_728_NF.wav";
	} else if (codigo == "11371") {
	    return "bairro_729_NF.wav";
	} else if (codigo == "4286") {
	    return "bairro_73_NF.wav";
	} else if (codigo == "11215") {
	    return "bairro_730_NF.wav";
	} else if (codigo == "10880") {
	    return "bairro_731_NF.wav";
	} else if (codigo == "41785") {
	    return "bairro_732_NF.wav";
	} else if (codigo == "40207") {
	    return "bairro_733_NF.wav";
	} else if (codigo == "5444") {
	    return "bairro_735_NF.wav";
	} else if (codigo == "40746") {
	    return "bairro_736_NF.wav";
	} else if (codigo == "2747") {
	    return "bairro_737_NF.wav";
	} else if (codigo == "6921") {
	    return "bairro_738_NF.wav";
	} else if (codigo == "42137") {
	    return "bairro_739_NF.wav";
	} else if (codigo == "1392") {
	    return "bairro_74_NF.wav";
	} else if (codigo == "13480") {
	    return "bairro_740_NF.wav";
	} else if (codigo == "40088") {
	    return "bairro_741_NF.wav";
	} else if (codigo == "1937") {
	    return "bairro_742_NF.wav";
	} else if (codigo == "2496") {
	    return "bairro_743_NF.wav";
	} else if (codigo == "41319") {
	    return "bairro_744_NF.wav";
	} else if (codigo == "10201") {
	    return "bairro_745_NF.wav";
	} else if (codigo == "40541") {
	    return "bairro_746_NF.wav";
	} else if (codigo == "42927") {
	    return "bairro_747_NF.wav";
	} else if (codigo == "2569") {
	    return "bairro_748_NF.wav";
	} else if (codigo == "1635") {
	    return "bairro_749_NF.wav";
	} else if (codigo == "11011") {
	    return "bairro_75_NF.wav";
	} else if (codigo == "41173") {
	    return "bairro_750_NF.wav";
	} else if (codigo == "4359") {
	    return "bairro_751_NF.wav";
	} else if (codigo == "6637") {
	    return "bairro_752_NF.wav";
	} else if (codigo == "14451") {
	    return "bairro_753_NF.wav";
	} else if (codigo == "41653") {
	    return "bairro_754_NF.wav";
	} else if (codigo == "40134") {
	    return "bairro_755_NF.wav";
	} else if (codigo == "10987") {
	    return "bairro_756_NF.wav";
	} else if (codigo == "2500") {
	    return "bairro_757_NF.wav";
	} else if (codigo == "42242") {
	    return "bairro_758_NF.wav";
	} else if (codigo == "10944") {
	    return "bairro_759_NF.wav";
	} else if (codigo == "14036") {
	    return "bairro_76_NF.wav";
	} else if (codigo == "11606") {
	    return "bairro_760_NF.wav";
	} else if (codigo == "2283" || codigo == "50022") {
	    return "bairro_761_NF.wav";
	} else if (codigo == "41467") {
	    return "bairro_762_NF.wav";
	} else if (codigo == "1902") {
	    return "bairro_763_NF.wav";
	} else if (codigo == "11444") {
	    return "bairro_764_NF.wav";
	} else if (codigo == "6041") {
	    return "bairro_765_NF.wav";
	} else if (codigo == "41874") {
	    return "bairro_766_NF.wav";
	} else if (codigo == "2534") {
	    return "bairro_767_NF.wav";
	} else if (codigo == "40363") {
	    return "bairro_768_NF.wav";
	} else if (codigo == "42544") {
	    return "bairro_769_NF.wav";
	} else if (codigo == "43982") {
	    return "bairro_77_NF.wav";
	} else if (codigo == "3018") {
	    return "bairro_770_NF.wav";
	} else if (codigo == "9466") {
	    return "bairro_771_NF.wav";
	} else if (codigo == "42714") {
	    return "bairro_772_NF.wav";
	} else if (codigo == "787") {
	    return "bairro_773_NF.wav";
	} else if (codigo == "11525") {
	    return "bairro_774_NF.wav";
	} else if (codigo == "1767") {
	    return "bairro_775_NF.wav";
	} else if (codigo == "2909") {
	    return "bairro_776_NF.wav";
	} else if (codigo == "3026") {
	    return "bairro_777_NF.wav";
	} else if (codigo == "2925") {
	    return "bairro_778_NF.wav";
	} else if (codigo == "40932") {
	    return "bairro_779_NF.wav";
	} else if (codigo == "9539") {
	    return "bairro_78_NF.wav";
	} else if (codigo == "13897" || codigo == "45276") {
	    return "bairro_780_NF.wav";
	} else if (codigo == "4677") {
	    return "bairro_781_NF.wav";
	} else if (codigo == "12351") {
	    return "bairro_782_NF.wav";
	} else if (codigo == "9911") {
	    return "bairro_783_NF.wav";
	} else if (codigo == "14079") {
	    return "bairro_784_NF.wav";
	} else if (codigo == "850") {
	    return "bairro_785_NF.wav";
	} else if (codigo == "5649") {
	    return "bairro_786_NF.wav";
	} else if (codigo == "6696" || codigo == "40568") {
	    return "bairro_787_NF.wav";
	} else if (codigo == "3085") {
	    return "bairro_788_NF.wav";
	} else if (codigo == "11291") {
	    return "bairro_789_NF.wav";
	} else if (codigo == "41483") {
	    return "bairro_79_NF.wav";
	} else if (codigo == "493") {
	    return "bairro_790_NF.wav";
	} else if (codigo == "1171") {
	    return "bairro_791_NF.wav";
	} else if (codigo == "43486") {
	    return "bairro_792_NF.wav";
	} else if (codigo == "8028") {
	    return "bairro_793_NF.wav";
	} else if (codigo == "5461") {
	    return "bairro_794_NF.wav";
	} else if (codigo == "5002" || codigo == "41262") {
	    return "bairro_795_NF.wav";
	} else if (codigo == "647") {
	    return "bairro_796_NF.wav";
	} else if (codigo == "3166" || codigo == "12688") {
	    return "bairro_797_NF.wav";
	} else if (codigo == "40533") {
	    return "bairro_798_NF.wav";
	} else if (codigo == "3182") {
	    return "bairro_799_NF.wav";
	} else if (codigo == "5827") {
	    return "bairro_8_NF.wav";
	} else if (codigo == "1775") {
	    return "bairro_80_NF.wav";
	} else if (codigo == "8648") {
	    return "bairro_800_NF.wav";
	} else if (codigo == "9253") {
	    return "bairro_801_NF.wav";
	} else if (codigo == "1741") {
	    return "bairro_803_NF.wav";
	} else if (codigo == "43915") {
	    return "bairro_804_NF.wav";
	} else if (codigo == "41718") {
	    return "bairro_805_NF.wav";
	} else if (codigo == "4855") {
	    return "bairro_806_NF.wav";
	} else if (codigo == "4511") {
	    return "bairro_807_NF.wav";
	} else if (codigo == "42161") {
	    return "bairro_808_NF.wav";
	} else if (codigo == "14401") {
	    return "bairro_809_NF.wav";
	} else if (codigo == "3395") {
	    return "bairro_81_NF.wav";
	} else if (codigo == "3328") {
	    return "bairro_810_NF.wav";
	} else if (codigo == "43168") {
	    return "bairro_811_NF.wav";
	} else if (codigo == "9717" || codigo == "44296") {
	    return "bairro_812_NF.wav";
	} else if (codigo == "43192") {
	    return "bairro_813_NF.wav";
	} else if (codigo == "42684") {
	    return "bairro_814_NF.wav";
	} else if (codigo == "42331" || codigo == "45772") {
	    return "bairro_815_NF.wav";
	} else if (codigo == "329") {
	    return "bairro_816_NF.wav";
	} else if (codigo == "14109") {
	    return "bairro_817_NF.wav";
	} else if (codigo == "9458") {
	    return "bairro_818_NF.wav";
	} else if (codigo == "14354") {
	    return "bairro_819_NF.wav";
	} else if (codigo == "5339") {
	    return "bairro_82_NF.wav";
	} else if (codigo == "40428") {
	    return "bairro_820_NF.wav";
	} else if (codigo == "213") {
	    return "bairro_821_NF.wav";
	} else if (codigo == "9041" || codigo == "9075" || codigo == "43788") {
	    return "bairro_822_NF.wav";
	} else if (codigo == "5720") {
	    return "bairro_823_NF.wav";
	} else if (codigo == "1139") {
	    return "bairro_824_NF.wav";
	} else if (codigo == "1261") {
	    return "bairro_825_NF.wav";
	} else if (codigo == "14257") {
	    return "bairro_826_NF.wav";
	} else if (codigo == "41963") {
	    return "bairro_827_NF.wav";
	} else if (codigo == "44164") {
	    return "bairro_828_NF.wav";
	} else if (codigo == "40738") {
	    return "bairro_829_NF.wav";
	} else if (codigo == "41297") {
	    return "bairro_83_NF.wav";
	} else if (codigo == "42498") {
	    return "bairro_830_NF.wav";
	} else if (codigo == "11851") {
	    return "bairro_831_NF.wav";
	} else if (codigo == "6751") {
	    return "bairro_832_NF.wav";
	} else if (codigo == "10341") {
	    return "bairro_833_NF.wav";
	} else if (codigo == "4499") {
	    return "bairro_834_NF.wav";
	} else if (codigo == "7544") {
	    return "bairro_835_NF.wav";
	} else if (codigo == "12882") {
	    return "bairro_836_NF.wav";
	} else if (codigo == "14575") {
	    return "bairro_837_NF.wav";
	} else if (codigo == "302" || codigo == "40983" || codigo == "6149" || codigo == "3425") {
	    return "bairro_838_NF.wav";
	} else if (codigo == "884" || codigo == "1619") {
	    return "bairro_839_NF.wav";
	} else if (codigo == "4596" || codigo == "42986") {
	    return "bairro_84_NF.wav";
	} else if (codigo == "10766" || codigo == "11908") {
	    return "bairro_840_NF.wav";
	} else if (codigo == "6131") {
	    return "bairro_841_NF.wav";
	} else if (codigo == "4316") {
	    return "bairro_842_NF.wav";
	} else if (codigo == "13803") {
	    return "bairro_843_NF.wav";
	} else if (codigo == "2704" || codigo == "3441" || codigo == "6530" || codigo == "1694" || codigo == "43427" || codigo == "50491" || codigo == "5517") {
	    return "bairro_844_NF.wav";
	} else if (codigo == "13927") {
	    return "bairro_845_NF.wav";
	} else if (codigo == "8524") {
	    return "bairro_846_NF.wav";
	} else if (codigo == "4219" || codigo == "50391" || codigo == "11096") {
	    return "bairro_847_NF.wav";
	} else if (codigo == "3999") {
	    return "bairro_848_NF.wav";
	} else if (codigo == "6424" || codigo == "50052" || codigo == "44954") {
	    return "bairro_849_NF.wav";
	} else if (codigo == "40886") {
	    return "bairro_85_NF.wav";
	} else if (codigo == "9296") {
	    return "bairro_850_NF.wav";
	} else if (codigo == "42587") {
	    return "bairro_851_NF.wav";
	} else if (codigo == "14320") {
	    return "bairro_852_NF.wav";
	} else if (codigo == "14567") {
	    return "bairro_853_NF.wav";
	} else if (codigo == "6114") {
	    return "bairro_854_NF.wav";
	} else if (codigo == "7129") {
	    return "bairro_855_NF.wav";
	} else if (codigo == "311") {
	    return "bairro_856_NF.wav";
	} else if (codigo == "13790") {
	    return "bairro_857_NF.wav";
	} else if (codigo == "40231") {
	    return "bairro_858_NF.wav";
	} else if (codigo == "10791") {
	    return "bairro_859_NF.wav";
	} else if (codigo == "14796") {
	    return "bairro_86_NF.wav";
	} else if (codigo == "14010") {
	    return "bairro_860_NF.wav";
	} else if (codigo == "5967") {
	    return "bairro_861_NF.wav";
	} else if (codigo == "44229") {
	    return "bairro_862_NF.wav";
	} else if (codigo == "43796") {
	    return "bairro_863_NF.wav";
	} else if (codigo == "4758") {
	    return "bairro_864_NF.wav";
	} else if (codigo == "7986" || codigo == "40282") {
	    return "bairro_865_NF.wav";
	} else if (codigo == "1414" || codigo == "6459" || codigo == "40924") {
	    return "bairro_866_NF.wav";
	} else if (codigo == "43583") {
	    return "bairro_867_NF.wav";
	} else if (codigo == "4766" || codigo == "3549" || codigo == "5151" || codigo == "4928") {
	    return "bairro_868_NF.wav";
	} else if (codigo == "12084") {
	    return "bairro_87_NF.wav";
	} else if (codigo == "44342" || codigo == "46434") {
	    return "bairro_870_NF.wav";
	} else if (codigo == "5975") {
	    return "bairro_871_NF.wav";
	} else if (codigo == "10847") {
	    return "bairro_872_NF.wav";
	} else if (codigo == "3867" || codigo == "11321" || codigo == "14001" || codigo == "12335" || codigo == "43141") {
	    return "bairro_873_NF.wav";
	} else if (codigo == "3751" || codigo == "44482" || codigo == "12017" || codigo == "45314" || codigo == "42404" || codigo == "40878") {
	    return "bairro_874_NF.wav";
	} else if (codigo == "2798" || codigo == "7056") {
	    return "bairro_875_NF.wav";
	} else if (codigo == "9733" || codigo == "9491" || codigo == "12858" || codigo == "43702") {
	    return "bairro_877_NF.wav";
	} else if (codigo == "5525") {
	    return "bairro_878_NF.wav";
	} else if (codigo == "5860") {
	    return "bairro_879_NF.wav";
	} else if (codigo == "43885") {
	    return "bairro_88_NF.wav";
	} else if (codigo == "14133") {
	    return "bairro_880_NF.wav";
	} else if (codigo == "1325") {
	    return "bairro_881_NF.wav";
	} else if (codigo == "3204" || codigo == "9202") {
	    return "bairro_882_NF.wav";
	} else if (codigo == "41858") {
	    return "bairro_883_NF.wav";
	} else if (codigo == "43699") {
	    return "bairro_884_NF.wav";
	} else if (codigo == "2658" || codigo == "14311" || codigo == "14745") {
	    return "bairro_885_NF.wav";
	} else if (codigo == "10502") {
	    return "bairro_886_NF.wav";
	} else if (codigo == "1651") {
	    return "bairro_887_NF.wav";
	} else if (codigo == "3379" || codigo == "9598") {
	    return "bairro_888_NF.wav";
	} else if (codigo == "5347") {
	    return "bairro_889_NF.wav";
	} else if (codigo == "43419") {
	    return "bairro_89_NF.wav";
	} else if (codigo == "43893") {
	    return "bairro_891_NF.wav";
	} else if (codigo == "43494" || codigo == "45179") {
	    return "bairro_892_NF.wav";
	} else if (codigo == "7722") {
	    return "bairro_893_NF.wav";
	} else if (codigo == "3492" || codigo == "14044") {
	    return "bairro_894_NF.wav";
	} else if (codigo == "2101" || codigo == "2712") {
	    return "bairro_895_NF.wav";
	} else if (codigo == "6122" || codigo == "43095" || codigo == "40967") {
	    return "bairro_896_NF.wav";
	} else if (codigo == "18325" || codigo == "41157") {
	    return "bairro_897_NF.wav";
	} else if (codigo == "10839") {
	    return "bairro_899_NF.wav";
	} else if (codigo == "44237") {
	    return "bairro_9_NF.wav";
	} else if (codigo == "426") {
	    return "bairro_90_NF.wav";
	} else if (codigo == "4693" || codigo == "41211") {
	    return "bairro_900_NF.wav";
	} else if (codigo == "6076" || codigo == "45101" || codigo == "50004") {
	    return "bairro_901_NF.wav";
	} else if (codigo == "4006" || codigo == "6181" || codigo == "42773") {
	    return "bairro_903_NF.wav";
	} else if (codigo == "17485") {
	    return "bairro_904_NF.wav";
	} else if (codigo == "2429" || codigo == "45691") {
	    return "bairro_905_NF.wav";
	} else if (codigo == "5436") {
	    return "bairro_906_NF.wav";
	} else if (codigo == "14397" || codigo == "43745") {
	    return "bairro_907_NF.wav";
	} else if (codigo == "14222" || codigo == "40479") {
	    return "bairro_908_NF.wav";
	} else if (codigo == "4791" || codigo == "42978") {
	    return "bairro_909_NF.wav";
	} else if (codigo == "42374") {
	    return "bairro_91_NF.wav";
	} else if (codigo == "41734") {
	    return "bairro_910_NF.wav";
	} else if (codigo == "44261") {
	    return "bairro_911_NF.wav";
	} else if (codigo == "7820") {
	    return "bairro_912_NF.wav";
	} else if (codigo == "13633") {
	    return "bairro_913_NF.wav";
	} else if (codigo == "5711") {
	    return "bairro_914_NF.wav";
	} else if (codigo == "11860") {
	    return "bairro_915_NF.wav";
	} else if (codigo == "7358" || codigo == "50223") {
	    return "bairro_916_NF.wav";
	} else if (codigo == "205") {
	    return "bairro_917_NF.wav";
	} else if (codigo == "9857") {
	    return "bairro_918_NF.wav";
	} else if (codigo == "12149" || codigo == "8354") {
	    return "bairro_919_NF.wav";
	} else if (codigo == "4278") {
	    return "bairro_92_NF.wav";
	} else if (codigo == "1783") {
	    return "bairro_921_NF.wav";
	} else if (codigo == "1431") {
	    return "bairro_922_NF.wav";
	} else if (codigo == "1473") {
	    return "bairro_923_NF.wav";
	} else if (codigo == "12891") {
	    return "bairro_924_NF.wav";
	} else if (codigo == "40371") {
	    return "bairro_925_NF.wav";
	} else if (codigo == "3905") {
	    return "bairro_926_NF.wav";
	} else if (codigo == "6963" || codigo == "50471") {
	    return "bairro_927_NF.wav";
	} else if (codigo == "7633") {
	    return "bairro_928_NF.wav";
	} else if (codigo == "14702") {
	    return "bairro_929_NF.wav";
	} else if (codigo == "44512") {
	    return "bairro_93_NF.wav";
	} else if (codigo == "40312") {
	    return "bairro_930_NF.wav";
	} else if (codigo == "18406") {
	    return "bairro_931_NF.wav";
	} else if (codigo == "41831") {
	    return "bairro_932_NF.wav";
	} else if (codigo == "43206") {
	    return "bairro_933_NF.wav";
	} else if (codigo == "8231") {
	    return "bairro_934_NF.wav";
	} else if (codigo == "7391") {
	    return "bairro_935_NF.wav";
	} else if (codigo == "7463") {
	    return "bairro_936_NF.wav";
	} else if (codigo == "40029") {
	    return "bairro_937_NF.wav";
	} else if (codigo == "4464") {
	    return "bairro_938_NF.wav";
	} else if (codigo == "41084") {
	    return "bairro_939_NF.wav";
	} else if (codigo == "2984" || codigo == "5291" || codigo == "44148" || codigo == " 43133" || codigo == "4049" || codigo == "50412" || codigo == "40304" || codigo == "3140") {
	    return "bairro_94_NF.wav";
	} else if (codigo == "8061") {
	    return "bairro_940_NF.wav";
	} else if (codigo == "40622") {
	    return "bairro_941_NF.wav";
	} else if (codigo == "41742") {
	    return "bairro_942_NF.wav";
	} else if (codigo == "5738") {
	    return "bairro_943_NF.wav";
	} else if (codigo == "6386") {
	    return "bairro_944_NF.wav";
	} else if (codigo == "8982") {
	    return "bairro_945_NF.wav";
	} else if (codigo == "12203") {
	    return "bairro_947_NF.wav";
	} else if (codigo == "4031") {
	    return "bairro_948_NF.wav";
	} else if (codigo == "13072") {
	    return "bairro_949_NF.wav";
	} else if (codigo == "7706" || codigo == "40762") {
	    return "bairro_95_NF.wav";
	} else if (codigo == "1554") {
	    return "bairro_950_NF.wav";
	} else if (codigo == "4375") {
	    return "bairro_951_NF.wav";
	} else if (codigo == "14630") {
	    return "bairro_952_NF.wav";
	} else if (codigo == "728") {
	    return "bairro_953_NF.wav";
	} else if (codigo == "6483") {
	    return "bairro_954_NF.wav";
	} else if (codigo == "41971") {
	    return "bairro_955_NF.wav";
	} else if (codigo == "41351") {
	    return "bairro_956_NF.wav";
	} else if (codigo == "4570") {
	    return "bairro_957_NF.wav";
	} else if (codigo == "5312") {
	    return "bairro_958_NF.wav";
	} else if (codigo == "42552" || codigo == "6904" || codigo == "1074") {
	    return "bairro_959_NF.wav";
	} else if (codigo == "5886") {
	    return "bairro_96_NF.wav";
	} else if (codigo == "2194") {
	    return "bairro_960_NF.wav";
	} else if (codigo == "14532") {
	    return "bairro_961_NF.wav";
	} else if (codigo == "5258") {
	    return "bairro_962_NF.wav";
	} else if (codigo == "13820" || codigo == "44814" || codigo == "45063") {
	    return "bairro_963_NF.wav";
	} else if (codigo == "40835") {
	    return "bairro_964_NF.wav";
	} else if (codigo == "5037") {
	    return "bairro_965_NF.wav";
	} else if (codigo == "2640") {
	    return "bairro_966_NF.wav";
	} else if (codigo == "4685") {
	    return "bairro_967_NF.wav";
	} else if (codigo == "9563") {
	    return "bairro_968_NF.wav";
	} else if (codigo == "6611") {
	    return "bairro_969_NF.wav";
	} else if (codigo == "612") {
	    return "bairro_97_NF.wav";
	} else if (codigo == "1295") {
	    return "bairro_970_NF.wav";
	} else if (codigo == "1287") {
	    return "bairro_971_NF.wav";
	} else if (codigo == "9512") {
	    return "bairro_972_NF.wav";
	} else if (codigo == "1678") {
	    return "bairro_973_NF.wav";
	} else if (codigo == "2879") {
	    return "bairro_974_NF.wav";
	} else if (codigo == "19") {
	    return "bairro_975_NF.wav";
	} else if (codigo == "3077" || codigo == "3093") {
	    return "bairro_976_NF.wav";
	} else if (codigo == "42749") {
	    return "bairro_977_NF.wav";
	} else if (codigo == "4367") {
	    return "bairro_978_NF.wav";
	} else if (codigo == "12866") {
	    return "bairro_979_NF.wav";
	} else if (codigo == "4189") {
	    return "bairro_98_NF.wav";
	} else if (codigo == "507" || codigo == "44741") {
	    return "bairro_980_NF.wav";
	} else if (codigo == "752") {
	    return "bairro_981_NF.wav";
	} else if (codigo == "2411") {
	    return "bairro_982_NF.wav";
	} else if (codigo == "7277") {
	    return "bairro_983_NF.wav";
	} else if (codigo == "4251") {
	    return "bairro_984_NF.wav";
	} else if (codigo == "40053" || codigo == "46132") {
	    return "bairro_985_NF.wav";
	} else if (codigo == "40142") {
	    return "bairro_986_NF.wav";
	} else if (codigo == "9725") {
	    return "bairro_987_NF.wav";
	} else if (codigo == "6882") {
	    return "bairro_988_NF.wav";
	} else if (codigo == "40843") {
	    return "bairro_989_NF.wav";
	} else if (codigo == "43214") {
	    return "bairro_990_NF.wav";
	} else if (codigo == "3816") {
	    return "bairro_991_NF.wav";
	} else if (codigo == "6572") {
	    return "bairro_992_NF.wav";
	} else if (codigo == "44393") {
	    return "bairro_993_NF.wav";
	} else if (codigo == "4871") {
	    return "bairro_994_NF.wav";
	} else if (codigo == "3417") {
	    return "bairro_995_NF.wav";
	} else if (codigo == "1252") {
	    return "bairro_996_NF.wav";
	} else if (codigo == "42625") {
	    return "bairro_997_NF.wav";
	} else if (codigo == "1481") {
	    return "bairro_998_NF.wav";
	} else if (codigo == "108") {
	    return "bairro_999_NF.wav";
	}
	
	return "";
	
}

function getPromptData(data){
	data = data.split("-");
	return "DiaMes_F_" + data[2] + "" + data[1] + ".wav";
}

function getFrasesRegistroReclamacao() {
	var i = 0;
	var frases = [];
	
	frases[i++] = {"frase" : "LatenciaProtocolo.wav"};
	
	return frases;
}

function getFrasesRepeteProtocolo(dados) {
	var i = 0;
	var frases = [];
	
	frases[i++] = {"frase" : "VocalizaProtocolo_Rpt.wav"};
	frases[i++] = {"protocolo": dados['servicoProtocoloInicial']['Y_PROTOCOLO']};
	frases[i++] = {"frase" : "VocalizaProtocolo_RptC.wav"};
	
	return frases;
}

