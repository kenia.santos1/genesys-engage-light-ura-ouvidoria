
var eventos = {
		
		"EventoX":"Descricao Evento",
}

function getEventoNome(numero) {
	var ret = {};
	var relatorio = [];
	var nomeEvento = "";
		var string = numero.toString();
		relatorio = string.split(";");
		if (relatorio.length > 1) {
			if (relatorio[1] == undefined || relatorio[1] == '') {
				nomeEvento = 'Nao_Definido_' + numero;
			}else{
				nomeEvento = relatorio[1].toString();
			}
		}else{
			nomeEvento = eventos[numero];
			if (nomeEvento == undefined || nomeEvento == '') { 
				nomeEvento = 'Nao_Definido_' + relatorio[0]; 
			} 
		}
	ret['numeroEvento'] = relatorio[0];
	ret['nomeEvento'] = (relatorio[0] + '.' + removerAcentos(nomeEvento)).substring(0, 64);
	return ret;
}

function adicionaEventoNavegacao(dados, numero) {
	if (!dados['eventosNavegacao']) {
		dados['eventosNavegacao'] = numero;
	} else {
		dados['eventosNavegacao'] += ";" + numero;
	}
	return dados;
}

var map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" };
function removerAcentos(s) { return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a }).replace(" - ", "-").replace(" – ", "-").replace(" – ", "-").replace(/\s/g, "_") };
