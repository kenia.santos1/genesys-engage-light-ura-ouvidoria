function getFrasesMenuNaoIdentificado(dados) {
	var ret = {};
	var i = 0;

	ret['nomeMenu'] = "MenuNaoIdentificado";
	ret["opcoesValidas"] = "";
	ret["maxTentativas"] = 1;
	ret["frases"] = [];
	ret["fraseNoMatch"] = [];
	ret["fraseNoMatch"][0] = { "frase": "MenuNaoID_Rej.wav" };
	ret["fraseNoInput"] = [];
	ret["fraseNoInput"][0] = { "frase": "MenuNaoID_Sil.wav" };
	ret['fraseDesconexao'] = [];
	ret['fraseDesconexao'][0] = "";
	ret['statOpcoes'] = "getEventoMenuNaoIdentificado";
	ret['statInvalida'] = "";

	ret["frases"][0] = { "frase": "MenuNaoID_Ini.wav" };
	ret["opcoesValidas"] = "1234";
	
	if(dados['parametros']['habilita_menu_naoidentificado_opcao9'] == 's'){
		ret["fraseNoMatch"][1] = { "frase": "MenuNaoID_9oculto.wav" };
		ret["fraseNoInput"][1] = { "frase": "MenuNaoID_9oculto.wav" };
		ret["frases"][1] = { "frase": "MenuNaoID_9oculto.wav" };
		ret["opcoesValidas"] = "12349";
	}

	return ret;
}

function getEventoMenuNaoIdentificado(opcao) {
	var evento = "";
	opcao = parseInt(opcao, 10);

	switch (opcao) {
		case 1:
			evento = "TF0001";
			break;
		case 2:
			evento = "TF0002";
			break;
		case 3:
			evento = "NS2318";
			break;
		case 4:
			evento = "NS2355";
			break;
		case 9:
			evento = "NS2356";
			break;
	}
	return evento;
}

function getFrasesMenuFaltaDeEnergia(dados) {
	var ret = {};
	var i = 0;

	ret['nomeMenu'] = "MenuFaltaDeEnergia";
	ret["opcoesValidas"] = "";
	ret["maxTentativas"] = 3;
	ret["frases"] = [];
	ret["fraseNoMatch"] = [];
	ret["fraseNoMatch"][0] = { "frase": "ChuteFaltaLuz_Rej.wav" };
	ret["fraseNoInput"] = [];
	ret["fraseNoInput"][0] = { "frase": "ChuteFaltaLuz_Sil.wav" };
	ret['fraseDesconexao'] = [];
	ret['fraseDesconexao'][0] = "";
	ret['statOpcoes'] = "getEventoMenuFaltaDeEnergia";
	ret['statInvalida'] = "";

	ret["frases"][0] = { "frase": "ChuteFaltaLuz_Ini.wav" };
	ret["opcoesValidas"] += "12";
	
	return ret;
}

function getEventoMenuFaltaDeEnergia(opcao) {
	var evento = "";
	opcao = parseInt(opcao, 10);

	switch (opcao) {
		case 1:
			evento = "";
			break;
		case 2:
			evento = "";
			break;
		
	}
	return evento;
}

function getFrasesMenuRecAnalise(dados){
	var ret = {};
	var i = 0;

	ret['nomeMenu'] = "MenuRecAnalise";
	ret["opcoesValidas"] = "";
	ret["maxTentativas"] = 3;
	ret["frases"] = [];
	ret["fraseNoMatch"] = [];
	ret["fraseNoMatch"][0] = { "frase": "MenuRecAnalise_Rej.wav" };
	ret["fraseNoInput"] = [];
	ret["fraseNoInput"][0] = { "frase": "MenuRecAnalise_Sil.wav" };
	ret["fraseNoInput"][1] = { "frase": getPromptData(dados['servicoIdentificacao']['YT_NOTAS']['item']['LTRMN'])};
	ret["fraseNoInput"][2] = { "frase": "MenuRecAnalise_B.wav" };
	ret['fraseDesconexao'] = [];
	ret['fraseDesconexao'][0] = "";
	ret['statOpcoes'] = "getEventoMenuRecAnalise";
	ret['statInvalida'] = "";

	ret["frases"][0] = { "frase": "MenuRecAnalise_Ini.wav" };
	ret["frases"][1] = { "frase": getPromptData(dados['servicoIdentificacao']['YT_NOTAS']['item']['LTRMN'])};
	ret["frases"][2] = { "frase": "MenuRecAnalise_B.wav" };
	ret["opcoesValidas"] = "1234";
	
	if(dados['parametros']['habilita_menu_protocolo_opcao9'] == 's'){
		ret["fraseNoMatch"][3] = { "frase": "MenuRecAnalise_9oculto.wav" };
		ret["fraseNoInput"][3] = { "frase": "MenuRecAnalise_9oculto.wav" };
		ret["frases"][3] = { "frase": "MenuRecAnalise_9oculto.wav" };
		ret["opcoesValidas"] = "12349";
	}
	
	return ret;
}

function getEventoMenuRecAnalise(opcao) {
	var evento = "";
	opcao = parseInt(opcao, 10);

	switch (opcao) {
	case 1:
		evento = "TF0003";
		break;
	case 2:
		evento = "TF0004";
		break;
	case 3:
		evento = "NS2329";
		break;
	case 4:
		evento = "NS2364";
		break;
	case 9:
		evento = "NS2365";
		break;	
	
}
	return evento;
}

function getFrasesMenuProtocoloSemReclamacao(dados){
	var ret = {};
	var i = 0;
	var parametros = dados['parametros'];

	ret['nomeMenu'] = "MenuProtocoloSemReclamacao";
	ret["opcoesValidas"] = "";
	ret["maxTentativas"] = 3;
	ret["frases"] = [];
	ret["fraseNoMatch"] = [];
	ret["fraseNoMatch"][0] = { "frase": "MenuSemProtocol_Rej.wav" };
	ret["fraseNoInput"] = [];
	ret["fraseNoInput"][0] = { "frase": "MenuSemProtocol_Sil.wav" };
	ret['fraseDesconexao'] = [];
	ret['fraseDesconexao'][0] = "";
	ret['statOpcoes'] = "getEventoMenuProtocoloSemReclamacao";
	ret['statInvalida'] = "";

	ret["frases"][0] = { "frase": "MenuSemProtocol_Ini.wav" };
	ret["opcoesValidas"] = "1234";
	
	if(dados['parametros']['habilita_menu_protocolo_opcao9'] == 's'){
		ret["fraseNoMatch"][1] = { "frase": "MenuSemProtocol_9oculto.wav" };
		ret["fraseNoInput"][1] = { "frase": "MenuSemProtocol_9oculto.wav" };
		ret["frases"][1] = { "frase": "MenuSemProtocol_9oculto.wav" };
		ret["opcoesValidas"] = "12349";
	}
	
	return ret;
}

function getEventoMenuProtocoloSemReclamacao(opcao) {
	var evento = "";
	opcao = parseInt(opcao, 10);

	switch (opcao) {
		case 1:
			evento = "TF0003";
			break;
		case 2:
			evento = "TF0004";
			break;
		case 3:
			evento = "NS2329";
			break;
		case 4:
			evento = "NS2364";
			break;
		case 9:
			evento = "NS2365";
			break;	
		
	}
	return evento;
}

function getFrasesMenuEmergenciaFaltaLuz(dados) {
	var ret = {};
	var i = 0;
	var parametros = dados['parametros'];

	ret['nomeMenu'] = "MenuEmergenciaFaltaLuz";
	ret["opcoesValidas"] = "";
	ret["maxTentativas"] = 3;
	ret["frases"] = [];
	ret["fraseNoMatch"] = [];
	ret["fraseNoMatch"][0] = { "frase": "MenuFaltaLuz_Rej.wav" };
	ret["fraseNoInput"] = [];
	ret["fraseNoInput"][0] = { "frase": "MenuFaltaLuz_Sil.wav" };
	ret['fraseDesconexao'] = [];
	ret['fraseDesconexao'][0] = "";
	ret['statOpcoes'] = "getEventoMenuEmergenciaFaltaLuz";
	ret['statInvalida'] = "";

	ret["frases"][0] = { "frase": "MenuFaltaLuz_Ini.wav" };
	ret["opcoesValidas"] = "123";
	
	if(dados['parametros']['habilita_menu_emergenciafaltaluz_opcao9'] == 's'){
		ret["fraseNoMatch"][1] = { "frase": "MenuFaltaLuz_9oculto.wav" };
		ret["fraseNoInput"][1] = { "frase": "MenuFaltaLuz_9oculto.wav" };
		ret["frases"][1] = { "frase": "MenuFaltaLuz_9oculto.wav" };
		ret["opcoesValidas"] = "1239";
	}
	
	return ret;
}

function getEventoMenuEmergenciaFaltaLuz(opcao) {
	var evento = "";
	opcao = parseInt(opcao, 10);

	switch (opcao) {
		case 1:
			evento = "AA2003";
			break;
		case 2:
			evento = "NS2360";
			break;
		case 3:
			evento = "NS2361";
			break;
		case 9:
			evento = "NS2362";
			break;
		
	}
	return evento;
}

function getFrasesMenuVerbalizaProtocolo(dados) {
	var ret = {};
	var i = 0;

	ret['nomeMenu'] = "MenuVerbalizaProtocolo";
	ret["opcoesValidas"] = "";
	ret["maxTentativas"] = 3;
	ret["frases"] = [];
	ret["fraseNoMatch"] = [];
	ret["fraseNoMatch"][0] = { "frase": "VocalizaProtocolo_Rej.wav" };
	ret["fraseNoInput"] = [];
	ret["fraseNoInput"][0] = { "frase": "VocalizaProtocolo_Sil.wav" };
	ret['fraseDesconexao'] = [];
	ret['fraseDesconexao'][0] = "";
	ret['statOpcoes'] = "getEventoMenuVerbalizaProtocolo";
	ret['statInvalida'] = "";

	ret["frases"][0] = { "frase": "VocalizaProtocolo_Ini.wav" };
	ret["frases"][1] = { "protocolo": dados['servicoProtocoloInicial']['Y_PROTOCOLO'] };
	ret["frases"][2] = { "frase": "VocalizaProtocolo_IniB.wav" };
	ret["frases"][3] = { "protocolo": dados['servicoProtocoloInicial']['Y_PROTOCOLO'] };
	ret["frases"][4] = { "frase": "VocalizaProtocolo_C.wav" };
	ret["opcoesValidas"] = "1";
	
	if(dados['parametros']['habilita_menu_verbalizaprotocolo_opcao9'] == 's'){
		ret["fraseNoMatch"][1] = { "frase": "VocalizaProtocolo_9oculto.wav" };
		ret["fraseNoInput"][1] = { "frase": "VocalizaProtocolo_9oculto.wav" };
		ret["frases"][5] = { "frase": "VocalizaProtocolo_9oculto.wav" };
		ret["opcoesValidas"] = "19";
	}
	
	return ret;
}

function getEventoMenuVerbalizaProtocolo(opcao) {
	var evento = "";
	opcao = parseInt(opcao, 10);

	switch (opcao) {
		case 1:
			evento = "";
			break;
		case 9:
			evento = "NS2359";
			break;
		
	}
	return evento;
}
